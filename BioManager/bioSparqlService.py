# -*- python -*-
#
#  This file is part of bioSparqlServices ces
#
##############################################################################

from SPARQLWrapper import *
import requests

__all__ = ["BioSparqlServicesError","BioSparqlService","Orphadata","MeshEndpoint","OntobeeEndpoint","EbiEndpoint",
"UniprotEndpoint","OmimEndpoint","LinkedLifeDataEndpoint","NextprotEndpoint","DisgenetEndpoint","PdbEndpoint"]



class BioSparqlServicesError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class BioSparqlService(object) :



    def __init__(self,name,endpoint,buildinPrefixes=""):
        self.basicPrefixes = {
        'dcterms':'http://purl.org/dc/terms/',
        'rdf':'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
        'rdfs':'http://www.w3.org/2000/01/rdf-schema#',
        'owl':'http://www.w3.org/2002/07/owl#',
        'skos':'http://www.w3.org/2004/02/skos/core#'}

        self.responseCodes = {
        200: 'OK',
        201: 'Created',
        400: 'Bad Request. There is a problem with your input',
        404: 'Not found. The resource you requests does not exist',
        405: 'Method not allowed',
        406: 'Not Acceptable. Usually headers issue',
        410: 'Gone. The resource you requested was removed.',
        415: 'Unsupported Media Type',
        500: 'Internal server error. Most likely a temporary problem',
        503: 'Service not available. The server is being updated, try again later'} 
        self._name = name
        self._endpoint = endpoint
        self._buildinPrefixes = buildinPrefixes

#    def _get_uri(self):
#	return self._uri
#
#    def _set_uri(self,url):
#	self._uri = uri
#    uri = property(_get_url, _set_url, doc="URL of this service")

    def get_name(self):
        return self._name

    def set_name(self,value):
        self._name = value
    name = property(get_name,set_name, doc="Name of this service")

    def get_basicPrefixes(self):
        return self.basicPrefixes

    def get_responseCodes(self):
        return self.responseCodes
   
    def set_endpoint(self,value):
        self._endpoint=value

    def get_endpoint(self):
        return self._endpoint
    endpoint = property(get_endpoint, doc="endpoint of this service")

    def viewBuildinPrefixes(self):
        print(self._buildinPrefixes)

    def execQuery(self,query,returnFormat=JSON):
        print("execQuery-------------------------")
        sparql = SPARQLWrapper(self._endpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(returnFormat)
        return sparql.query().convert()

class Orphadata(BioSparqlService) :
    """ Orphadata service 
    """

    def __init__(self) :
        self._buildinPrefixes="""
    PREFIX HOOM:<http://www.semanticweb.org/ontology/HOOM>
    PREFIX ORDO:<http://www.orpha.net/ORDO/>
    PREFIX Orphanet_:<http://www.orpha.net/ORDO/Orphanet_#>
    PREFIX bif:<bif:>
    PREFIX dawgt:<http://www.w3.org/2001/sw/DataAccess/tests/test-dawg#>
    PREFIX dbpedia:<http://dbpedia.org/resource/>
    PREFIX dbpprop:<http://dbpedia.org/property/>
    PREFIX dc:<http://purl.org/dc/elements/1.1/>
    PREFIX ebi:<http://www.ebi.ac.uk/efo/>
    PREFIX fn:<http://www.w3.org/2005/xpath-functions/#>
    PREFIX foaf:<http://xmlns.com/foaf/0.1/>
    PREFIX geo:<http://www.w3.org/2003/01/geo/wgs84_pos#>
    PREFIX go:<http://purl.org/obo/owl/GO#>
    PREFIX ldp:<http://www.w3.org/ns/ldp#>
    PREFIX math:<http://www.w3.org/2000/10/swap/math#>
    PREFIX mesh:<http://purl.org/commons/record/mesh/>
    PREFIX mf:<http://www.w3.org/2001/sw/DataAccess/tests/test-manifest#>
    PREFIX nci:<http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#>
    PREFIX obo:<http://www.geneontology.org/formats/oboInOwl#>
    PREFIX ogc:<http://www.opengis.net/>
    PREFIX ogcgml:<http://www.opengis.net/ont/gml#>
    PREFIX ogcgs:<http://www.opengis.net/ont/geosparql#>
    PREFIX ogcgsf:<http://www.opengis.net/def/function/geosparql/>
    PREFIX ogcgsr:<http://www.opengis.net/def/rule/geosparql/>
    PREFIX ogcsf:<http://www.opengis.net/ont/sf#>
    PREFIX owl:<http://www.w3.org/2002/07/owl#>
    PREFIX product:<http://www.buy.com/rss/module/productV2/>
    PREFIX protseq:<http://purl.org/science/protein/bysequence/>
    PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfa:<http://www.w3.org/ns/rdfa#>
    PREFIX rdfdf:<http://www.openlinksw.com/virtrdf-data-formats#>
    PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
    PREFIX sc:<http://purl.org/science/owl/sciencecommons/>
    PREFIX scovo:<http://purl.org/NET/scovo#>
    PREFIX sd:<http://www.w3.org/ns/sparql-service-description#>
    PREFIX sioc:<http://rdfs.org/sioc/ns#>
    PREFIX skos:<http://www.w3.org/2004/02/skos/core#>
    PREFIX sql:<sql:>
    PREFIX vcard:<http://www.w3.org/2001/vcard-rdf/3.0#>
    PREFIX vcard2006:<http://www.w3.org/2006/vcard/ns#>
    PREFIX virtcxml:<http://www.openlinksw.com/schemas/virtcxml#>
    PREFIX virtrdf:<http://www.openlinksw.com/schemas/virtrdf#>
    PREFIX void:<http://rdfs.org/ns/void#>
    PREFIX xf:<http://www.w3.org/2004/07/xpath-functions>
    PREFIX xml:<http://www.w3.org/XML/1998/namespace>
    PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
    PREFIX xsl10:<http://www.w3.org/XSL/Transform/1.0>
    PREFIX xsl1999:<http://www.w3.org/1999/XSL/Transform>
    PREFIX xslwd:<http://www.w3.org/TR/WD-xsl>
    PREFIX yago:<http://dbpedia.org/class/yago/>
    """
        super().__init__("Orphadata","https://www.orpha.net/sparql",self._buildinPrefixes)  

    
class MeshEndpoint(BioSparqlService) :
    """ Mesh Sparql Endpoint
    """
    
    def __init__(self) :
        self._buildinPrefixes="""
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX meshv: <http://id.nlm.nih.gov/mesh/vocab#>
PREFIX mesh: <http://id.nlm.nih.gov/mesh/>
PREFIX mesh2019: <http://id.nlm.nih.gov/mesh/2019/>
PREFIX mesh2018: <http://id.nlm.nih.gov/mesh/2018/>
PREFIX mesh2017: <http://id.nlm.nih.gov/mesh/2017/>
"""

        super().__init__("MeshEndpoint","http://id.nlm.nih.gov/mesh/sparql", self._buildinPrefixes)



class OntobeeEndpoint(BioSparqlService) :
    """ Ontobee  Sparql Endpoint
    """
    
    def __init__(self) :
        self._buildinPrefixes="""
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xhtml: <http://www.w3.org/1999/xhtml>
PREFIX wot: <http://xmlns.com/wot/0.1/>
PREFIX vs: <http://www.w3.org/2003/06/sw-vocab-status/ns#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX sioct: <http://rdfs.org/sioc/types#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
PREFIX rss: <http://purl.org/rss/1.0/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX dataview: <http://www.w3.org/2003/g/data-view#>
PREFIX cc: <http://web.resource.org/cc/>
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
prefix owl: <http://www.w3.org/2002/07/owl#> 
"""

        super().__init__("OntobeeEndpoint","http://sparql.hegroup.org/sparql/", self._buildinPrefixes)


class EbiEndpoint(BioSparqlService) :
    """ EbiEndpoint  Sparql Endpoint
    """
    
    def __init__(self) :
        self._buildinPrefixes="""
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX dbpedia2: <http://dbpedia.org/property/>
PREFIX dbpedia: <http://dbpedia.org/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX sbmlrdf: <http://identifiers.org/biomodels.vocabulary#>
PREFIX biosd-terms: <http://rdf.ebi.ac.uk/terms/biosd/>
PREFIX pav: <http://purl.org/pav/2.0/>
PREFIX atlas: <http://rdf.ebi.ac.uk/terms/atlas/>
PREFIX cco: <http://rdf.ebi.ac.uk/terms/chembl#>
PREFIX obo: <http://purl.obolibrary.org/obo/>
PREFIX sio: <http://semanticscience.org/resource/>
PREFIX ensembltranscript: <http://rdf.ebi.ac.uk/resource/ensembl.transcript/>
PREFIX faldo: <http://biohackathon.org/resource/faldo#>
PREFIX biopax3: <http://www.biopax.org/release/biopax-level3.owl#>
PREFIX atlasterms: <http://rdf.ebi.ac.uk/terms/expressionatlas/>
PREFIX efo: <http://www.ebi.ac.uk/efo/>
PREFIX core: <http://purl.uniprot.org/core/>
"""

        super().__init__("EbiEndpoint","https://www.ebi.ac.uk/rdf/services/sparql", self._buildinPrefixes)


class UniprotEndpoint(BioSparqlService):
    """Interface to the `UniProt <http://www.uniprot.org>`_ service
    """

    def __init__(self):
        
        self.buildinPrefixes="""      
PREFIX up:<http://purl.uniprot.org/core/> 
PREFIX keywords:<http://purl.uniprot.org/keywords/> 
PREFIX uniprotkb:<http://purl.uniprot.org/uniprot/> 
PREFIX taxon:<http://purl.uniprot.org/taxonomy/> 
PREFIX ec:<http://purl.uniprot.org/enzyme/> 
PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> 
PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 
PREFIX owl:<http://www.w3.org/2002/07/owl#> 
PREFIX bibo:<http://purl.org/ontology/bibo/> 
PREFIX dc:<http://purl.org/dc/terms/> 
PREFIX xsd:<http://www.w3.org/2001/XMLSchema#> 
PREFIX faldo:<http://biohackathon.org/resource/faldo#>         
"""

        super().__init__("UniprotEndpoint","https://sparql.uniprot.org/",self.buildinPrefixes)	    

class LinkedLifeDataEndpoint(BioSparqlService):
    """ interface to LinkedLifeDataEndpoint
    """

    def __init__(self) : 
        self.buildinPrefixes="""
PREFIX dc-term: <http://purl.org/dc/terms/>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX biopax3: <http://www.biopax.org/release/biopax-level3.owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX skos-xl: <http://www.w3.org/2008/05/skos-xl#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX umls-semnetwork: <http://linkedlifedata.com/resource/semanticnetwork/id/>
PREFIX umls-label: <http://linkedlifedata.com/resource/umls/label/>
PREFIX umls-concept: <http://linkedlifedata.com/resource/umls/id/>
PREFIX umls: <http://linkedlifedata.com/resource/umls/>
        """
        super().__init__("LinkedLifeDataEndpoint","http://linkedlifedata.com/sparql",self.buildinPrefixes)


class OmimEndpoint(BioSparqlService):
    """ Bio2rdf endpoint for omim
    """

    def __init__(self):
        self.builinPrefixes="""
        """
        super().__ini__("OmimEndpoint","http://omim.bio2rdf.org/sparql",self.buildinPrefixes)


class NextprotEndpoint(BioSparqlService):
    """ Bio2rdf endpoint for omim
    """

    def __init__(self):
        self.builinPrefixes="""
        PREFIX : <http://nextprot.org/rdf#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX up: <http://purl.uniprot.org/core/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX entry: <http://nextprot.org/rdf/entry/>
PREFIX isoform: <http://nextprot.org/rdf/isoform/>
PREFIX annotation: <http://nextprot.org/rdf/annotation/>
PREFIX evidence: <http://nextprot.org/rdf/evidence/>
PREFIX xref: <http://nextprot.org/rdf/xref/>
PREFIX publication: <http://nextprot.org/rdf/publication/>
PREFIX identifier: <http://nextprot.org/rdf/identifier/>
PREFIX cv: <http://nextprot.org/rdf/terminology/>
PREFIX gene: <http://nextprot.org/rdf/gene/>
PREFIX source: <http://nextprot.org/rdf/source/>
PREFIX db: <http://nextprot.org/rdf/db/>
PREFIX context: <http://nextprot.org/rdf/context/>
PREFIX interaction: <http://nextprot.org/rdf/interaction/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX uniprot: <http://purl.uniprot.org/uniprot/>
PREFIX unipage: <http://www.uniprot.org/uniprot/>
PREFIX proteoform: <http://nextprot.org/rdf/proteoform/>
PREFIX chebi: <http://purl.obolibrary.org/obo/>
PREFIX drugbankdrugs: <http://wifo5-04.informatik.uni-mannheim.de/drugbank/resource/drugs/>
        """
        super().__ini__("NextprotEndpoint","https://api.nextprot.org/sparql",self.buildinPrefixes)



class DisgenetEndpoint(BioSparqlService):
    """ interface to LinkedLifeDataEndpoint
    """

    def __init__(self) : 
        self.buildinPrefixes="""
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX void: <http://rdfs.org/ns/void#>
PREFIX sio: <http://semanticscience.org/resource/>
PREFIX ncit: <http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#>
PREFIX up: <http://purl.uniprot.org/core/>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dctypes: <http://purl.org/dc/dcmitype/>
PREFIX wi: <http://http://purl.org/ontology/wi/core#>
PREFIX eco: <http://http://purl.obolibrary.org/obo/eco.owl#>
PREFIX prov: <http://http://http://www.w3.org/ns/prov#>
PREFIX pav: <http://http://http://purl.org/pav/>
PREFIX obo: <http://purl.obolibrary.org/obo/> 
        """
        super().__init__("DisgenetEndpoint","http://rdf.disgenet.org/sparql/",self.buildinPrefixes)



class PdbEndpoint(BioSparqlService):
    """ interface to LinkedLifeDataEndpoint
    """

    def __init__(self) : 
        self.buildinPrefixes="""
        """
        super().__init__("PdbEndpoint","https://integbio.jp/rdf/sparql",self.buildinPrefixes)