from rdflib.namespace import *
#from pyramid.threadlocal import
#import configparser
from pyramid.config import Configurator
from pyramid.threadlocal import get_current_registry


registry = get_current_registry()
settings = registry.settings
if settings == None :
    import configparser
    config = configparser.ConfigParser()
    config.read('../../development.ini')
    settings=config['app:main']

AMQUERY = settings['repo_amyp_query']
AMUPDATE = settings['repo_amyp_update']
UNIQUERY = settings['repo_uniprot_query']
UNIUPDATE = settings['repo_uniprot_update']
GOQUERY = settings['repo_go_query']
MONDOQUERY = settings['repo_mondo_query']
OMIMQUERY = settings['repo_omim_query']
HPOQUERY=settings['repo_hpo_query']

STR_AMKB = settings['amkb']
STR_AMMODEL = settings['ammodel']
STR_AMTHES = settings['amthes']
STR_AMBASE = settings['ambase']
STR_AMNAME = settings['amname']

STR_UNIKB = "http://purl.uniprot.org/uniprot/"
STR_UNICORE = "http://purl.uniprot.org/core/"
STR_UNINAME = "Uniprot"
STR_NEXTKB = "http://nextprot.org/rdf/entry/"
STR_ENS = "http://rdf.ebi.ac.uk/resource/ensembl/"
STR_ENSNAME = "Ensembl"
STR_OBO = "http://purl.obolibrary.org/obo/"

AMKB = Namespace(STR_AMKB)
AMMODEL = Namespace(STR_AMMODEL)
AMTHES = Namespace(STR_AMTHES)
UNIKB = Namespace(STR_UNIKB)
UNICORE = Namespace(STR_UNICORE)
NEXTKB = Namespace(STR_NEXTKB)

if __name__ == '__main__':
    print(AMKB)    
