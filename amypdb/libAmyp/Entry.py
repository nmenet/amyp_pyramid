from rdflib import URIRef, Literal,Graph
from SPARQLWrapper import SPARQLWrapper,N3,JSON
import requests
from rdflib.plugins.stores import sparqlstore
from rdflib.namespace import SKOS,RDF,RDFS,OWL,DC,XSD
from amypdb.libAmyp.AmyConf import *
from amypdb.libAmyp.Thesaurus import *
from BioManager.bioSparqlService import *
from BioManager.bioEntity import *
from Bio.SeqUtils.ProtParam import ProteinAnalysis
import json
from operator import itemgetter
from amypdb.libAmyp.utils import *

class Entry(object) :

    def __init__(self,identifier,uriBase,queryEndpoint,updateEndpoint):
        self._id = identifier
        self._stUri = uriBase + self._id
        self._uri = URIRef(self._stUri)
        self._stUriBase = uriBase
        self._queryEndpoint = queryEndpoint
        self._updateEndpoint = updateEndpoint

    def get_stUriBase(self):
        return self._stUriBase

    def _set_stUriBase(self,value):
        self._stUriBase = value

    uriBase = property(get_stUriBase, doc="Entry UriBase")

    def get_queryEndpoint(self):
        return self._queryEndpoint

    def _set_queryEndpoint(self,value):
        self._queryEndpoint = value
        
    queryEndpoint = property(get_queryEndpoint, doc="Entry queryEndpoint")

    def get_updateEndpoint(self):
        return self._updateEndpoint

    def _set_updateEndpoint(self,value):
        self._updateEndpoint = value
        
    updateEndpoint = property(get_updateEndpoint, doc="Entry updateEndpoint")


    def get_id(self):
        return self._id

    def _set_id(self,value):
        self._id = value
        self._set_stUri(self.get_stUriBase+self._id)

    id = property(get_id, doc="Entry identifier")


    def get_stUri(self):
        return self._stUri

    def _set_stUri(self,value):
        self._stUri = value
        self._set_uri(self._stUri)

    stUri = property(get_stUri, doc="Entry string uri")

    def get_uri(self):
        return self._uri

    def _set_uri(self,value):
        self._uri = URIRef(value)

    uri = property(get_uri, doc="Entry Uri")


    def getEntityGraph(self) :
        #print("get entity graph " + self._uri + " with : " + self._stUriBase)
        sparql = SPARQLWrapper(self._queryEndpoint)
        queryDesc = "DESCRIBE <{s}>".format(s=self._stUri)
        queryMore = "CONSTRUCT {{ ?s2 ?p2 ?o2 }} WHERE {{ <{s}> ?p  ?s2 . ?s2 ?p2 ?o2 }}".format(s=self._stUri)
        sparql.setQuery(queryDesc)
        sparql.setReturnFormat(N3)
        results = sparql.query().convert()
        sparql.setQuery(queryMore)
        rg = Graph()
        try : 
            results2 = sparql.query().convert()
        except : 
            print("The graph was not retrieved")
        else : 
            rg.parse(data=results, format="n3")
            rg.parse(data=results2, format="n3")
        finally : 
            return rg


    def getDescribeGraph(self) :
        #print("get entity graph " + self._uri + " with : " + self._stUriBase)
        sparql = SPARQLWrapper(self._queryEndpoint)
        queryDesc = "DESCRIBE <{s}>".format(s=self._stUri)
        sparql.setQuery(queryDesc)
        sparql.setReturnFormat(N3)
        results = sparql.query().convert()
        rg = Graph()
        rg.parse(data=results, format="n3")
        return rg


    def getIdentityGraph(self) :
        #print("get entity graph " + self._uri + " with : " + self._stUriBase)
        sparql = SPARQLWrapper(self._queryEndpoint)
        query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  PREFIX model: <http://purl.amypdb.org/model/> CONSTRUCT {{ <{s}> rdfs:label ?label . <{s}> model:accession ?accession }} WHERE {{ <{s}> rdfs:label ?label . <{s}> model:accession ?accession }}".format(s=self._stUri)
        sparql.setQuery(query)
        sparql.setReturnFormat(N3)
        results = sparql.query().convert()
        rg = Graph()
        rg.parse(data=results, format="n3")
        return rg



    def exists(self,linkedRef=None) :
        ref = ""
        if linkedRef != None :
            ref=str(linkedRef)            
        else : 
            ref=self._stUri
        query= " ASK {{ <{uri}> ?p ?o }} ".format(uri=ref)     
        headers = {'content-type' : 'application/x-www-form-urlencoded'}
        myparam = { 'query': query }
        try : 
            r=requests.get(self._queryEndpoint,myparam,headers=headers)
        except :
            print("There was a problem trying to check the existence of  "  + ref)
            return False
        else : 
            exists = r.json()['boolean']
            return exists



    def existsLiteral(self,label) :
        ref=self._stUri
        query= " ASK {{ <{uri}> ?p  '{label}' }} ".format(uri=ref,label=label)     
        headers = {'content-type' : 'application/x-www-form-urlencoded'}
        myparam = { 'query': query }
        try : 
            r=requests.get(self._queryEndpoint,myparam,headers=headers)
        except :
            print("There was a problem trying to check the existence of  "  + label)
            return False
        else : 
            exists = r.json()['boolean']
            return exists

    def existsAccess(self) :
        ref=self._stUri
        query= "PREFIX model: <http://purl.amypdb.org/model/>  ASK {{ <{uri}> model:accession  ?acc }} ".format(uri=ref)     
        headers = {'content-type' : 'application/x-www-form-urlencoded'}
        myparam = { 'query': query }
        try : 
            r=requests.get(self._queryEndpoint,myparam,headers=headers)
        except :
            print("There was a problem trying to check the existence of  "  + label)
            return False
        else : 
            exists = r.json()['boolean']
            return exists

    def addEntityGraphToStore(self,graph) :
        #print("adding statements to endpoint " + self._updateEndpoint)
        #print(self._stUriBase)
        query = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=graph.serialize(format='nt').decode(),g=self._stUriBase)
        headers = {'content-type' : 'application/x-www-form-urlencoded'}
        #headers = {'content-type' : 'application/sparql-update'}
        #cont = "<"+self._stUriBase+">"
        cont = URIRef(self._stUriBase)
        myparam = { 'context' : cont,'update': query}
        #print("update endpoint" + self._updateEndpoint)
        #print("query" + query)
        try : 
            r=requests.post(self._updateEndpoint,myparam,headers=headers) 
        except :
            print("There was a problem trying to add  "  + self._stUri + " to the triple store")
        

        #print("Adding graph " + str(r.status_code))



class KbEntry(Entry) :

    def __init__(self,identifier):
        super().__init__(identifier,STR_AMKB,AMQUERY,AMUPDATE)

    def _getMondo(self,uriDis) :
        sparql = SPARQLWrapper(MONDOQUERY)
        queryMondo = "PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> \
        PREFIX oboInOwl:<http://www.geneontology.org/formats/oboInOwl#> \
        SELECT distinct ?mondo WHERE {{ ?mondo skos:exactMatch <{dis}> }}".format(dis=uriDis)
        sparql.setQuery(queryMondo)
        sparql.setReturnFormat(JSON)
        mondoL=[]
        try : 
            results=sparql.query().convert()
        except : 
            print("unable to retrieve mondo label")
            return mondoL
        else :         
            for row in results["results"]["bindings"] : 
                mondoL.append(row["mondo"]["value"])
            return mondoL

    def _getMondoFromLabel(self,label) :
        mondoL=[]
        sparql = SPARQLWrapper(MONDOQUERY)
        queryMondo = "PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> \
        PREFIX oboInOwl:<http://www.geneontology.org/formats/oboInOwl#> \
        SELECT distinct ?mondo WHERE {{ ?mondo rdfs:label|oboInOwl:hasExactSynonym|oboInOwl:hasRelatedSynonym '{label}' }}".format(label=label)
        sparql.setQuery(queryMondo)
        sparql.setReturnFormat(JSON)
        try : 
            results=sparql.query().convert()
        except : 
            print("unable to retrive mondo label")
            return mondoL
        else : 
            for row in results["results"]["bindings"] : 
                mondoL.append(row["mondo"]["value"])
            return mondoL


    def _getDiseaseLabel(self,uniDisUri) : 
        sparql = SPARQLWrapper(UNIQUERY)
        queryLabel = "PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
        SELECT distinct ?label WHERE {{ <{s}> skos:prefLabel ?label . \
        }} ".format(s=str(uniDisUri))
        sparql.setQuery(queryLabel)
        sparql.setReturnFormat(JSON)
        label = ""
        try : 
            results=sparql.query().convert()
        except : 
            print("unable to get Disease set_prefLabel")
            return label
        else : 
            for row in results["results"]["bindings"] : 
                label = row["label"]["value"]
            return label


  
class UniKbEntry(Entry) :

    def __init__(self,identifier):
        super().__init__(identifier,STR_UNIKB,UNIQUERY,UNIUPDATE)


class UniProtein(UniKbEntry):    

    def __init__(self,identifier):
        super().__init__(identifier)

    def load(self) :
        queryL = 'LOAD <https://www.uniprot.org/uniprot/{file}.rdf> INTO GRAPH <{g}>  '.format(file=self._id,g=self._stUriBase)
        #print(queryL)
        myparam = { 'update': queryL }
        headers = {'content-type' : 'application/x-www-form-urlencoded'}
        results1=requests.post(self._updateEndpoint,params=myparam, headers=headers)
        print(str(results1.status_code) + " file added to endpoint "+self._updateEndpoint)



class Protein(KbEntry):

    def __init__(self,identifier):
        super().__init__(identifier)
        self._type = AMMODEL.Protein
     
    def get_type(self):
        return self._type

    type = property(get_type,doc="Entry Type")

    def getPublications(self) :
        uniId = STR_UNIKB + self._id
        query="PREFIX unicore:<http://purl.uniprot.org/core/> \
        PREFIX owl: <http://www.w3.org/2002/07/owl#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        PREFIX foaf: <http://xmlns.com/foaf/0.1/> \
        SELECT ?title ?date ?name ?volume ?pages ?pubmed \
        (GROUP_CONCAT(DISTINCT ?author; SEPARATOR=', ') AS ?authors) \
        FROM <http://purl.uniprot.org/uniprot/> \
        WHERE {{ \
        <{prot}> unicore:citation ?citation . \
        ?citation unicore:author ?author . \
        ?citation unicore:title ?title . \
        ?citation unicore:date ?date . \
        ?citation unicore:name ?name . \
        ?citation foaf:primaryTopicOf ?pubmed . \
        OPTIONAL {{?citation unicore:volume ?volume . ?citation unicore:pages ?pages . }} \
        }} GROUP BY ?title ?date ?name ?volume ?pages ?pubmed ORDER BY DESC(?date) LIMIT 30 ".format(prot=uniId)

        sparql = SPARQLWrapper(UNIQUERY)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        data = []
        for row in results["results"]["bindings"] :
            dict =  {}
            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""
            data.append(dict)

        return data


    def getDisComment(self) :
        query="PREFIX unicore:<http://purl.uniprot.org/core/> \
        PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX owl: <http://www.w3.org/2002/07/owl#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        PREFIX foaf: <http://xmlns.com/foaf/0.1/> \
        SELECT distinct ?disComment \
        WHERE {{ \
        <{prot}> model:disComment ?disComment . \
        }}".format(prot=self._stUri)

        sparql = SPARQLWrapper(AMQUERY)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        data = []
        for row in results["results"]["bindings"] :
            dict =  {}
            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""
            data.append(dict)

        return data


    def getDiseaseInfo(self) :
        query="PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX owl: <http://www.w3.org/2002/07/owl#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        SELECT ?diseaseId ?diseaseLabel ?diseaseDesc ?diseaseCom \
        (GROUP_CONCAT(DISTINCT ?pub; SEPARATOR='|') AS ?pubList) \
        WHERE {{ \
        <{prot}> model:mentionedIn ?assoc . \
        ?assoc rdf:type model:Proda . \
        ?assoc model:referringTo ?disease . \
        ?assoc model:accession ?acc . \
        ?disease rdf:type model:Disease . \
        ?disease model:thesRef ?disRef . \
        ?disRef skos:prefLabel ?diseaseLabel .\
         ?disease model:accession ?diseaseId . \
         FILTER(regex(?acc,?diseaseId)) \
        OPTIONAL {{ ?disease model:description ?diseaseDesc . }}\
        OPTIONAL {{ ?assoc model:disComment ?diseaseCom . }} \
        OPTIONAL {{ ?assoc model:supportingEvidence ?evi . \
        ?evi model:publication ?pub . }}\
        }} GROUP BY ?diseaseLabel ?diseaseId ?diseaseDesc ?diseaseCom ".format(prot=self._stUri)

        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        data = []
        for row in results["results"]["bindings"] :
            dict =  {}
            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""
            data.append(dict)

        return data






    def getVariantInfo(self) :
        query="PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX owl: <http://www.w3.org/2002/07/owl#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        SELECT ?variantId ?variantDesc ?score ?link ?disLabel ?disId ?old ?new ?pos  \
        WHERE {{\
        <{prot}> model:isoform ?isoform .\
        ?isoform model:variant ?variant .\
        ?variant model:mentionedIn ?vda .\
        ?variant rdf:type model:VariantAa . \
        ?variant rdfs:seeAlso ?link . \
        ?variant model:accession ?variantId . \
        ?variant model:description ?variantDesc . \
        ?variant model:substitution ?new . \
        ?variant model:substituted ?old. \
        ?variant model:positionRef ?posref. \
        ?posref model:position ?pos.\
        ?vda rdf:type model:Vda . \
        ?vda model:score ?score . \
        ?vda model:accession ?acc . \
        ?vda model:referringTo ?disease . \
        ?disease rdf:type model:Disease .\
        ?disease rdfs:label ?disLabel .\
        ?disease model:accession ?diseaseId .\
        FILTER(regex(?acc,?diseaseId)) \
        OPTIONAL {{ ?disease model:accession ?disId . }} \
        }} ORDER BY DESC(?score) ".format(prot=self._stUri)

        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        data = []
        for row in results["results"]["bindings"] :
            dict =  {}
            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""
            data.append(dict)

        return data


    def getIsoformInfo(self) :
        query="PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX owl: <http://www.w3.org/2002/07/owl#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        SELECT ?iso ?isoAcc ?isoLength ?isoSeq ?isoCano ?bestPdbId  ?isoMolWeight ?isoInstability ?isoElectricPoint ?isoAminoPercent ?isoAromaticity  \
        (COUNT(DISTINCT ?signature) AS ?nbSignature) (GROUP_CONCAT(DISTINCT ?signInfo; SEPARATOR='|') AS ?signInfoList)  \
        WHERE {{ \
        <{prot}> model:isoform ?iso . \
        ?iso model:accession ?isoAcc . \
        ?iso model:aaCount ?isoLength . \
        OPTIONAL {{?iso model:molWeight ?isoMolWeight . }} \
        OPTIONAL {{?iso model:instabilityIndex ?isoInstability . }}\
        ?iso model:isoelectricPoint ?isoElectricPoint . \
        ?iso model:aminoPercent ?isoAminoPercent . \
        ?iso model:aromaticity ?isoAromaticity . \
        OPTIONAL {{?iso model:primStruct ?primStruct . \
        ?primStruct model:sequence ?isoSeq . \
        }} \
        OPTIONAL {{?iso model:signature ?signature . \
        ?signature skos:prefLabel ?signLabel . \
        ?signature skos:notation ?signAcc . \
        BIND(concat(?signAcc, '::', ?signLabel) AS ?signInfo) \
        }} \
        OPTIONAL {{?iso model:isCanonical ?isoCano . \
        }} \
        OPTIONAL {{<{prot}> model:bestPdbId ?bestPdbId . \
        }} \
        }} GROUP BY ?iso ?isoAcc ?isoLength ?isoSeq ?isoCano ?bestPdbId  ?isoMolWeight ?isoInstability ?isoElectricPoint ?isoAminoPercent ?isoAromaticity ".format(prot=self._stUri)
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        data = []
        for row in results["results"]["bindings"] :
            dict =  {}
            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""
            data.append(dict)


        return data




    def getAmyloidAnnotations(self) : 
        

        query="""
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX owl: <http://www.w3.org/2002/07/owl#> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        SELECT distinct ?desc ?source1 ?indicator ?indicLabel ?qualiLabel (GROUP_CONCAT(DISTINCT ?source2; SEPARATOR='|') AS ?sourceList)   
        WHERE {{ 
        {{ ?annot model:source ?source1 . 
        ?annot model:description ?desc . 
        ?annot model:qualifier ?qualifier . 
        ?qualifier skos:prefLabel ?qualiLabel . 
        ?annot model:referringTo ?entity . ?entity rdf:type ?entityType . 
        VALUES ?entity {{ <{prot}>  }}
        VALUES ?entityType {{ model:Protein }}        
        OPTIONAL {{ 
        {{ ?annot model:basedOn ?indicator . }}
        UNION {{ ?annot model:basedOn ?indicator . ?indicator rdfs:label ?indicLabel .  }} 
        }}  
        OPTIONAL {{  ?annot model:supportingEvidence ?evi . ?evi model:source ?source2 .}}   
            }} 
        UNION 
        {{ ?annot model:source ?source1 . 
        ?annot model:description ?desc . 
        ?annot model:qualifier ?qualifier . 
        ?qualifier skos:prefLabel ?qualiLabel . 
        ?annot model:referringTo ?entity . ?entity rdf:type ?entityType . 
        ?prot model:isoform ?entity  . ?prot rdf:type model:Protein . 
        VALUES ?prot {{ <{prot}>  }} 
        VALUES ?entityType {{ model:Isoform }}          
        ?entity rdfs:label ?entityLabel . 
        ?entity model:accession ?entityAcc . 
         OPTIONAL {{ 
        {{ ?annot model:basedOn ?indicator . }} 
        UNION {{ ?annot model:basedOn ?indicator . ?indicator rdfs:label ?indicLabel . }}  
        }} 
        OPTIONAL {{  ?annot model:supportingEvidence ?evi . ?evi model:source ?source2 .}}        
        }}  
        UNION 
        {{  ?annot model:source ?source1 . 
        ?annot model:description ?desc . 
        ?annot model:qualifier ?qualifier . 
        ?qualifier skos:prefLabel ?qualiLabel . 
        ?annot model:referringTo ?entity . ?entity rdf:type ?entityType . 
        ?iso model:cleavedInto ?entity . ?iso rdf:type model:Isoform . 
        ?prot model:isoform ?iso . ?prot rdf:type model:Protein . 
        VALUES ?prot {{  <{prot}>  }}  
        VALUES ?entityType {{  model:Polypeptide }}         
        ?entity rdfs:label ?entityLabel . 
        ?entity model:accession ?entityAcc .  
         OPTIONAL {{
        {{ ?annot model:basedOn ?indicator . }} 
        UNION {{ ?annot model:basedOn ?indicator . ?indicator rdfs:label ?indicLabel . }}  
        }} 
        OPTIONAL {{  ?annot model:supportingEvidence ?evi . ?evi model:source ?source2 .}}  
       
        }}  
        }}  GROUP BY ?entity ?entityType  ?desc ?source1 ?indicator ?indicLabel ?qualiLabel 
        """.format(prot=self._stUri)
        
        
        
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        
        data = []
        

        for row in results["results"]["bindings"] :
            dict =  {}
            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""
            data.append(dict)


        return data

    def getBioThread(self) :

        dataAll = {} #what we will return in the end
        dataGeneProt = [] #to store gene-protein relations
        data = [] # to store transcripts - isoforms - poly - complex
        node_colors = {} # to store color info
        legend = []

        sparql = SPARQLWrapper(self._queryEndpoint)
        querygene = """PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> 
        SELECT * 
        WHERE {{ {{ 
        <{s}> rdfs:label ?cplabel . 
        <{s}> model:accession ?cpacc . 
        ?gene model:encodes <{s}> . 
        ?gene rdfs:label ?glabel . 
        ?gene model:accession ?gacc . 
         }} UNION {{ 
         ?gene model:encodes <{s}> . 
         ?gene rdfs:label ?glabel . 
         ?gene model:accession ?gacc . 
         ?gene model:encodes ?prot2 . 
         ?prot2 rdfs:label ?plabel . 
         ?prot2 model:accession ?pacc .
         FILTER (?prot2 !=  <{s}> ) 
         }} UNION {{ 
         ?gene model:encodes <{s}> . 
         ?gene model:transcript ?trans . 
         ?gene rdfs:label ?glabel . 
         ?gene model:accession ?gacc . 
         ?trans rdfs:label ?tlabel . 
         ?trans model:accession ?tacc . 
         FILTER NOT EXISTS {{ ?trans model:translatedTo ?iso }} 
         }} UNION {{ 
         ?gene model:encodes <{s}> . 
         ?gene model:transcript ?trans . 
         ?gene rdfs:label ?glabel .
         ?gene model:accession ?gacc . 
         ?trans rdfs:label ?tlabel . 
         ?trans model:accession ?tacc . 
         ?trans model:translatedTo ?iso . 
         ?iso model:accession ?iacc . 
         ?iso rdfs:label ?ilabel . 
         FILTER NOT EXISTS {{ ?iso model:cleavedInto ?poly }}
         }} UNION {{ 
         ?gene model:encodes <{s}> . 
         ?gene rdfs:label ?glabel . 
         ?gene model:accession ?gacc . 
         ?gene model:transcript ?trans . 
         ?trans rdfs:label ?tlabel . 
         ?trans model:accession ?tacc . 
         ?trans model:translatedTo ?iso . 
         ?iso model:accession ?iacc .  
         ?iso rdfs:label ?ilabel . 
         ?iso model:cleavedInto ?poly . 
         ?poly rdfs:label ?polylabel . 
         ?poly model:accession ?polyacc . 
          FILTER NOT EXISTS {{ ?poly model:partOf ?complex }} 
         }} UNION {{ 
         ?gene model:encodes <{s}> .
        ?gene rdfs:label ?glabel . 
        ?gene model:accession ?gacc . 
         ?gene model:transcript ?trans . 
         ?trans rdfs:label ?tlabel . 
         ?trans model:accession ?tacc . 
         ?trans model:translatedTo ?iso . 
         ?iso model:accession ?iacc .  
         ?iso rdfs:label ?ilabel . 
         ?iso model:cleavedInto ?poly . 
         ?poly rdfs:label ?polylabel . 
         ?poly model:accession ?polyacc . 
         ?poly model:partOf ?complex . 
         ?complex model:accession ?compacc . 
         ?complex rdfs:label ?complabel . 
         }} }}""".format(s=self._stUri)

        #print(self._stUri)
        #params={'query' : querygene}
        #headers={'Accept':'application/sparql-results+json','content-type' : 'application/x-www-form-urlencoded'}
        #r=requests.get(self._queryEndpoint,params=params,headers=headers)
        #results = r.json()
        
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(querygene)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        #print(r.status_code)

        geneList = []
        #protList = []
        #transList = []
        isoList = []
        #polyList = []
        #compList = []
        countg = 1
        null = None

        for row in results["results"]["bindings"] : 
            gene = row["gene"]["value"]

            #add Gene-prot and add Gene-trans
            if "cplabel" in row :    
                geneList.append(row["glabel"]["value"])
                dictGP = {}
                dictGP["from"] = row["glabel"]["value"]
                dictGP["to"] = row["cplabel"]["value"]
                dictGP["weight"] = 1
                dictGP["custom_field"] = row["gacc"]["value"] + " -> " + row["cpacc"]["value"]
                if dictGP not in dataGeneProt : 
                    dataGeneProt.append(dictGP)
                    #green for genes
                    if row["glabel"]["value"] not in node_colors : 
                        gcolor="hsla(157, 34%, 40%, {trans})".format(trans=str(countg))
                        node_colors[row["glabel"]["value"]] = gcolor
                        countg=countg-0.2
                        gene_legend_name = "Gene "+ row["glabel"]["value"] + " and its transcripts"
                        legend_info = {"text": gene_legend_name,"iconFill": gcolor}
                        legend.append(legend_info)
                    node_colors[row["cplabel"]["value"]] = "hsla(189, 98%, 28%)"
                    prot_legend_name = "Protein " + row["cplabel"]["value"] + ", its isoforms and related molecules"
                    prot_legend_info = {"text":prot_legend_name,"iconFill":"hsla(189, 98%, 28%)"}
                    legend.append(prot_legend_info)
                
            elif "prot2" in row : 
                prot2 = row["prot2"]["value"]
                dictGP = {}
                dictGP["from"] = row["glabel"]["value"]
          
                dictGP["to"] = row["plabel"]["value"]
                dictGP["weight"] = 1
                dictGP["custom_field"] = row["gacc"]["value"] + " -> " + row["pacc"]["value"]
                if dictGP not in dataGeneProt : 
                    dataGeneProt.append(dictGP)
                    #green for genes
                    prot2color="hsla(189, 21%, 45%)"
                    node_colors[row["plabel"]["value"]] = prot2color
                    prot_legend_name = "Protein "+ row["plabel"]["value"] + ", its isoforms and related molecules"
                    prot_legend_info = {"text":prot_legend_name,"iconFill": prot2color}
                    legend.append(prot_legend_info)


            elif "trans" in row : 
                trans = row["trans"]["value"]              
                dictGT = {}
                dictGT["from"] = row["glabel"]["value"]
                dictGT["to"] =  row["tlabel"]["value"]
                dictGT["weight"] = 1
                dictGT["custom_field"] = row["gacc"]["value"] + " -> " + row["tacc"]["value"]
                if dictGT not in data : 
                    data.append(dictGT)
                    if row["glabel"]["value"] not in node_colors : 
                        gcolor="hsla(157, 34%, 40%, {trans})".format(trans=str(countg))
                        node_colors[row["glabel"]["value"]] = gcolor
                        countg=countg-0.2
                        node_colors[row["tlabel"]["value"]] = gcolor

                    else :
                        node_colors[row["tlabel"]["value"]] = node_colors[row["glabel"]["value"]]

                if "iso" not in row : 
                    dictTI = {}
                    dictTI["from"] = row["tlabel"]["value"]
                    dictTI["to"] =  null
                    dictTI["weight"] = 1
                    dictTI["custom_field"] = row["tacc"]["value"] + " -> unknown "
                    if dictTI not in data : 
                        data.append(dictTI) 

                else :  
                    iso = row["iso"]["value"]
                    if iso not in isoList : 
                        isoList.append(iso)
                    dictTI = {}
                    dictTI["from"] = row["tlabel"]["value"]
                    dictTI["to"] =  row["ilabel"]["value"]
                    dictTI["weight"] = 1
                    dictTI["custom_field"] = row["tacc"]["value"] + " -> " + row["iacc"]["value"]
                    if dictTI not in data : 
                        data.append(dictTI) 
                            #par defaut on ne met pas la couleur principale (ce sera modifié après)
                        node_colors[row["ilabel"]["value"]] = "hsla(189, 21%, 45%)"

                    if "poly" not in row : 
                        dictIsoPoly = {}
                        dictIsoPoly["from"] = row["ilabel"]["value"]
                        dictIsoPoly["to"] =  null
                        dictIsoPoly["weight"] = 1
                        dictIsoPoly["custom_field"] = row["iacc"]["value"] + " -> unknown " 
                        if dictIsoPoly not in data : 
                            data.append(dictIsoPoly) 

                    else :   
                        poly = row["poly"]["value"]
                        dictIsoPoly = {}
                        dictIsoPoly["from"] = row["ilabel"]["value"]
                        dictIsoPoly["to"] =  row["polylabel"]["value"]
                        dictIsoPoly["weight"] = 1
                        dictIsoPoly["custom_field"] = row["iacc"]["value"] + " -> " + row["polyacc"]["value"]
                        if dictIsoPoly not in data : 
                            data.append(dictIsoPoly)
                            node_colors[row["polylabel"]["value"]] = "hsla(189, 21%, 45%)"
                       
                        if "complex" not in row : 
                            dictPolyComp = {}
                            dictPolyComp["from"] = row["polylabel"]["value"]
                            dictPolyComp["to"] =  null
                            dictPolyComp["weight"] = 1
                            dictPolyComp["custom_field"] = row["polyacc"]["value"] + " -> unknown" 
                            if dictPolyComp not in data : 
                                data.append(dictPolyComp)
                        else : 
                            compl = row["complex"]["value"]
                            dictPolyComp = {}
                            dictPolyComp["from"] = row["polylabel"]["value"]
                            dictPolyComp["to"] =  row["complabel"]["value"]
                            dictPolyComp["weight"] = 1
                            dictPolyComp["custom_field"] = row["polyacc"]["value"] + " -> " + row["compacc"]["value"]
                            if dictPolyComp not in data :
                                data.append(dictPolyComp)
                                node_colors[row["complabel"]["value"]] = "hsla(189, 21%, 45%)"
    
        

        queryiso = "PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> \
        SELECT *\
        WHERE {{  {{ \
        <{s}> model:isoform ?iso . \
        ?iso model:accession ?iacc . \
        ?iso rdfs:label ?ilabel . \
        FILTER NOT EXISTS {{ ?iso model:cleavedInto ?poly .  }} \
        }} UNION {{ \
        <{s}> model:isoform ?iso . \
        ?iso model:accession ?iacc . \
        ?iso rdfs:label ?ilabel . \
        ?iso model:cleavedInto ?poly . \
         ?poly rdfs:label ?polylabel . \
         ?poly model:accession ?polyacc . \
         FILTER NOT EXISTS {{ ?poly model:partOf ?complex . }} \
         }} \
         UNION {{ \
         <{s}> model:isoform ?iso . \
        ?iso model:accession ?iacc .  \
        ?iso rdfs:label ?ilabel . \
         ?iso model:cleavedInto ?poly . \
         ?poly rdfs:label ?polylabel . \
         ?poly model:accession ?polyacc . \
         ?poly model:partOf ?complex . \
         ?complex model:accession ?compacc . \
         ?complex rdfs:label ?complabel . \
        }} }} ".format(s=self._stUri)

        #print(queryiso)

        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(queryiso)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()

        #paramsiso={'query' : queryiso}
        #headers={'Accept':'application/sparql-results+json','content-type' : 'application/x-www-form-urlencoded'}
        #r2=requests.get(self._queryEndpoint,params=paramsiso,headers=headers)

        isoListA = []
        polyList2 = []
        compList2 = []

        #results = r.json()

        for row in results["results"]["bindings"] : 
            iso = row["iso"]["value"]

            if iso not in isoListA :
                isoListA.append(iso)

            if iso not in isoList and len(isoList) > 0 :  
                    
                dictGT = {}
                geneV = "Potentially "+",".join(geneList)
                if len(geneList) > 1 : 
                    geneV = geneV[:-1]
                dictGT["from"] = geneV
                dictGT["to"] =  "unknown transcripts"
                dictGT["weight"] = 1
                dictGT["custom_field"] = "unknown -> unknown"
                data.append(dictGT) 
                node_colors[geneV] = "hsla(0, 0%, 75%, 1)"

                dictTI = {}
                dictTI["from"] = "unknown transcripts"
                dictTI["to"] =  row["ilabel"]["value"]
                dictTI["weight"] = 1
                dictTI["custom_field"] = "unknown -> " + row["iacc"]["value"]
                data.append(dictTI) 
                #par defaut on ne met pas la couleur principale (ce sera modifié après)
                node_colors["unknown transcripts"] = "hsla(0, 0%, 75%, 1)"

            if "poly" not in row : 
                dictIsoPoly = {}
                dictIsoPoly["from"] = row["ilabel"]["value"]
                dictIsoPoly["to"] =  null
                dictIsoPoly["weight"] = 1
                dictIsoPoly["custom_field"] = row["iacc"]["value"] + " -> unknown "
                if dictIsoPoly not in data : 
                    data.append(dictIsoPoly)
                    #even if it is already mentioned, the color needs to be changed
                if row["ilabel"]["value"] in node_colors : 
                    node_colors.update({row["ilabel"]["value"]:"hsla(189,98%,28%)"})
                else : 
                    node_colors[row["ilabel"]["value"]] = "hsla(189, 98%, 28%)"
            else : 
                dictIsoPoly = {}
                dictIsoPoly["from"] = row["ilabel"]["value"]
                dictIsoPoly["to"] =  row["polylabel"]["value"]
                dictIsoPoly["weight"] = 1
                dictIsoPoly["custom_field"] = row["iacc"]["value"] + " -> " + row["polyacc"]["value"]
                if dictIsoPoly not in data : 
                    data.append(dictIsoPoly)
                node_colors[row["ilabel"]["value"]] = "hsla(189, 98%, 28%)"
                if row["polylabel"]["value"] in node_colors : 
                    node_colors.update({row["polylabel"]["value"]:"hsla(189,98%,28%)"})
                else : 
                    node_colors[row["polylabel"]["value"]] = "hsla(189, 98%, 28%)"

                if "complex" in row : 
                    compl = row["complex"]["value"]
                    dictPolyComp = {}
                    dictPolyComp["from"] = row["polylabel"]["value"]
                    dictPolyComp["to"] =  row["complabel"]["value"]
                    dictPolyComp["weight"] = 1
                    dictPolyComp["custom_field"] = row["polyacc"]["value"] + " -> " + row["compacc"]["value"]
                    if dictPolyComp not in data : 
                        data.append(dictPolyComp)
                    node_colors[row["complabel"]["value"]] = "hsla(189, 98%, 28%)"
                


       
        dataAll["geneProt"]=dataGeneProt
        dataAll["data"]=data
        dataAll["node_colors"]=node_colors
        dataAll["legend"] = legend

        return dataAll

        

    def getIsoforms(self) :
        query="SELECT distinct ?item WHERE {{ <{subj}> <{prop}> ?item }}".format(subj=self._stUri,prop=str(AMMODEL.isoform))
        params={'query' : query}
        headers={'Accept':'application/sparql-results+json','content-type' : 'application/x-www-form-urlencoded'}
        r=requests.get(self._queryEndpoint,params=params,headers=headers)
        isoforms=[]
        for row in r.json()["results"]["bindings"] : 
            isoforms.append(row["item"]["value"])
        return isoforms

    def _getOmim(self,uniDisUri) : 
        sparql = SPARQLWrapper(UNIQUERY)
        queryOmim = "PREFIX unicore:<http://purl.uniprot.org/core/> \
        PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> \
        SELECT distinct ?omim WHERE {{ <{s}> rdfs:seeAlso ?omim . ?omim unicore:database <http://purl.uniprot.org/database/MIM> . \
        }} ".format(s=str(uniDisUri))
        sparql.setQuery(queryOmim)
        sparql.setReturnFormat(JSON)
        uriOmim = ""
        try : 
            results=sparql.query().convert()
        except : 
            print("unable to retrive omim info from uniprot")
            return uriOmim
        else : 
            for row in results["results"]["bindings"] : 
                uriOmim = row["omim"]["value"]
                uriOmim = "http://identifiers.org/omim/"+uriOmim.split("/")[-1]
            return uriOmim

  
    def getMainInfo(self) :

        query="PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX owl: <http://www.w3.org/2002/07/owl#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        SELECT ?label ?organism  \
        (GROUP_CONCAT(DISTINCT ?desc; SEPARATOR=' ') AS ?description) \
        (GROUP_CONCAT(DISTINCT ?qualiLabel; SEPARATOR='|') AS ?qualiList) \
        (GROUP_CONCAT(DISTINCT ?altLabel; SEPARATOR=', ') AS ?altList) \
        (GROUP_CONCAT(DISTINCT ?geneInfo; SEPARATOR='|') AS ?geneList) \
        WHERE {{\
        <{prot}> rdfs:label ?label . \
        <{prot}> model:description ?desc .\
        <{prot}> model:organism ?org . ?org skos:prefLabel ?organism .\
        OPTIONAL {{<{prot}> model:thesRef ?ref . ?ref skos:altLabel ?altLabel }}\
        OPTIONAL {{<{prot}> model:encodedBy ?gene . ?gene model:accession ?geneId . ?gene rdfs:label ?geneLabel . BIND(concat(?geneId, '::', ?geneLabel) AS ?geneInfo) }} \
        OPTIONAL {{<{prot}> model:tag ?tag . ?tag skos:prefLabel ?qualiLabel }}\
        }} GROUP BY ?label ?organism ".format(prot=self._stUri)
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        dict =  {}
        for row in results["results"]["bindings"] :

            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""

        return dict

 
    
    def _createBasicProfile(self,uniPG,uniP) :   
        
        tempG = Graph()
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)

        print("creating info from uniprot gr")
        ############# TYPE AND LINK TO GENERAL INFO ON MAIN PROTEIN DB ##################################   
        tempG.add([self._uri,RDF.type, self._type])
        tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])
        tempG.add([self._uri,RDFS.seeAlso,uniP.uri])
        tempG.add([self._uri,AMMODEL.uniprotRef,uniP.uri])
        if (self._uri,RDFS.seeAlso, URIRef(STR_NEXTKB+"NX_"+self._id)) in uniPG :
            tempG.add([self._uri,RDFS.seeAlso,URIRef(STR_NEXTKB+"NX_"+self._id)])       

        ############# ADDING LABELS (prefLabel, altLabel, abbr, mnemonic) #####################
        
        
        if (uniP.uri,UNICORE.recommendedName,None) in uniPG :
            recName = uniPG.value(uniP.uri,UNICORE.recommendedName,None)
            tempG.add([self._uri,RDFS.label,uniPG.value(recName,UNICORE.fullName,None)])
        else :
            tempG.add([self._uri,RDFS.label,Literal("unnamed")])
        d=ThesEntry(STR_UNINAME+"_"+self._id,STR_UNINAME)
        if not d.exists() : 
            d.createThesProt(uniPG,uniP.uri)
        tempG.add([self._uri,AMMODEL.thesRef,d.uri])

        #################################### KEYWORDS ##################################################
        #to get directly from seeAlso UNIPROT. Keywords / Tissues / Diseases in repository

        ############ ADDING FUNCTIONAL DESCRIPTION ####################################################
        for subj in uniPG.subjects(RDF.type,UNICORE.Function_Annotation) :
            #print("Addding function Annotation")
            desc = uniPG.value(subj,RDFS.comment,None)
            tempG.add([self._uri,AMMODEL.description,desc])

#             "cellular_component"^^<http://www.w3.org/2001/XMLSchema#string>
# "molecular_function"^^<http://www.w3.org/2001/XMLSchema#string>
# "biological_process"^^<http://www.w3.org/2001/XMLSchema#string>
        #goterms=""
        #print("Checking go terms")
        for obj in uniPG.objects(uniP.uri,UNICORE.classifiedWith) :
        
            if "obo/GO_" in str(obj) :
                goId = str(obj).split("/")[-1]
                #goterms+= "<"+str(obj)+"> "
                #print(goId)
                
                sparql = SPARQLWrapper(GOQUERY)
                query = "PREFIX oboInOwl:<http://www.geneontology.org/formats/oboInOwl#> \
                PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> \
                SELECT distinct ?sub ?type ?label (group_concat(?altLabel;separator='---') as ?alt) \
                WHERE {{ VALUES ?sub {{ <{goterms}> }} \
                ?sub oboInOwl:hasOBONamespace ?type . ?sub rdfs:label ?label . OPTIONAL {{ ?sub oboInOwl:hasExactSynonym ?altLabel .}}  }} \
                GROUP BY ?type ?sub ?label ".format(goterms=str(obj))
                #print(query)
                #print(GOQUERY)
                sparql.setQuery(query)
                sparql.setReturnFormat(JSON)
                results=sparql.query().convert()
                
                #print("RESULTS for GO QUERY : " + results)
                for row in results["results"]["bindings"] : 
                    if "sub" in row :                 
                        #print("row type value" + row["type"]["value"] )
                    #print(row["sub"]["value"])
                        thes=ThesEntry(goId,Go().name)
                        if not thes.exists() : 
                            thes.createThesGo(row["sub"]["value"],row["label"]["value"],row["alt"]["value"])
                        if row["type"]["value"] == "molecular_function" : 
                            tempG.add([self._uri,AMMODEL.function,thes.uri])
                        elif row["type"]["value"] == "biological_process" :
                            tempG.add([self._uri,AMMODEL.bioProcess,thes.uri])
                        elif row["type"]["value"] == "cellular_component" :
                            tempG.add([self._uri,AMMODEL.cellComponent,thes.uri])


        ############################# ADDING FAMILY ####################################################

        if (None,RDF.type,UNICORE.Similarity_Annotation) in uniPG :
            #print("Adding Family from Similarity annotation")
            value = uniPG.value(uniPG.value(None,RDF.type,UNICORE.Similarity_Annotation),RDFS.comment,None)
            value = value.replace("Belongs to the ","")
            value = value.replace(".","")
            uriV = value.replace(" ","_")
            d=ThesEntry(STR_AMNAME+"_"+uriV,STR_AMNAME)
            d.createThesPrefOnly(value)
            tempG.add([self._uri,AMMODEL.family,d.uri])

        try : 
            r = requests.get("https://www.ebi.ac.uk/pdbe/api/mappings/best_structures/"+uniP.id)
        except : 
            print("unable to retrieve best structures from ebi")
        else :  

            data = r.json()
            if  uniP.id in data : 
                items = data[uniP.id]
                bestPdb = items[0]["pdb_id"]
                if bestPdb != "" and bestPdb != None  : 
                    tempG.add([self._uri,AMMODEL.bestPdbId,Literal(bestPdb)])
                    r2 = requests.get("https://www.ebi.ac.uk/pdbe/api/mappings/cath/"+bestPdb)
                    if r2.status_code == 200 :
                        data2 = r2.json()
                        items2 = data2[bestPdb]
                        cathTab = items2['CATH']
                        cath=""
                        toponame=""
                        for elt in cathTab :
                            cath=elt
                            toponame = cathTab[elt]["topology"]
                        lastIndex = cath.rfind(".")
                        cathId = cath[:lastIndex]
                        #print("CATH : "+cath + "," + cathId + " toponame" + toponame)
                        dcath=ThesEntry("CATH_"+cathId,"CATH")
                        if not dcath.exists() : 
                            dcath.createThesCath(toponame,cathId)
                        tempG.add([self._uri,AMMODEL.family,dcath.uri])

        finally : 
        ############################# ADDING ORGANISM ####################################################

            if (uniP.uri,UNICORE.organism,None) in uniPG :
                #print("Adding taxon to protein")
                org = uniPG.value(uniP.uri,UNICORE.organism,None)
                lastIndex = str(org).rfind("/")
                orgId = str(org)[lastIndex+1:]
                d=ThesEntry(Uniprot().name+"_taxonomy_"+orgId,Uniprot().name+"_taxonomy")
                if not d.exists() :
                    d.createThesOrg()
                tempG.add([self._uri,AMMODEL.organism,d.uri])
            
            ################################ FIL BIO ####################################################
            geneUri=""
            if (None,UNICORE.transcribedFrom,None) in uniPG :
                for obj in uniPG.objects(None,UNICORE.transcribedFrom) : 
                    geneUri = str(obj)
            else :
                for row in uniPG.query('SELECT distinct ?s WHERE {?s ?p ?o FILTER regex(str(?s),"ENS[A-Z]{3}G0|ENSG0")}'):
                    geneUri=row["s"]
                    #print(geneUri)
           
            if "ENS" in geneUri :
                lastIndex = geneUri.rfind("/")
                gId = geneUri[lastIndex+1:]
                if "." in gId : 
                    gId = gId.split('.')[0]
                gene=Gene(gId)
                if not gene.exists() :
                    #print("adding gene to protein")
                    gene.create(uniPG)
                #tempG.add([self._uri,AMMODEL.encodedBy,gene.uri])
                #tempG.add([gene.uri,AMMODEL.encodes,self._uri])


            ################################ PROT-DISEASE ####################################################

            if (None,RDF.type,URIRef("http://purl.uniprot.org/core/Disease_Annotation")) in uniPG :
                for subj in uniPG.subjects(RDF.type,URIRef("http://purl.uniprot.org/core/Disease_Annotation")) : 
                    uniDisUri = uniPG.value(subj,UNICORE.disease,None)
                    uniComment = uniPG.value(subj,RDFS.comment,None)
                    if uniDisUri != None : 
                        uriOmim = self._getOmim(uniDisUri)
                        attribBlockUri = uniPG.value(subj)
                        if uriOmim != "" : 
                            mondoL = self._getMondo(uriOmim)
                            if len(mondoL) < 1 : 
                                label = self._getDiseaseLabel(uniDisUri)
                                mondoL = self._getMondoFromLabel(label)
                        
                            for elt in mondoL :
                                disId = elt.split("/")[-1]
                                #disId = disId.split("_")[-1]
                                dis = Disease(disId)
                                if not dis.exists() : 
                                    dis.create()

                                ProdaId = self._id+"_"+dis._id
                                proda = Proda(ProdaId,self._uri,dis.uri,uniComment)
                                if not proda.exists() : 
                                    proda.create(self._id,uniDisUri)
                    else :
                        if uniComment != None : 
                            tempG.add([self._uri,AMMODEL.disComment,Literal(uniComment)])




            ################################ ISOFORME  ######################################
            
            if (uniP.uri,UNICORE.sequence,None) in uniPG :

                for obj in uniPG.objects(uniP.uri,UNICORE.sequence) : 
                    isoUri = str(obj)
                    lastIndex = isoUri.rfind("/")
                    isoId = isoUri[lastIndex+1:]
                    iso=Isoform(isoId)
                    #on check avec labelExists et non pas Exists, car l'entité a pu être créée au préalable poru définir les relations transcrits-isoformes
                    if not iso.labelExists() :
                        iso.create(uniPG,uniP,obj)
                    if (obj,UNICORE.precursor,None) in uniPG : 
                        tempG.add([self._uri,AMMODEL.isPrecursor,Literal("True", datatype=XSD.boolean)])
                    tempG.add([self._uri,AMMODEL.isoform,iso.uri])        
            
            
            self.addEntityGraphToStore(tempG)





    def _createProteinComplex(self,uniPG) :
                #checking if popypeptide is part of a complex
        dbComplexPortal = URIRef("http://purl.uniprot.org/database/ComplexPortal")
        if (None,UNICORE.database,dbComplexPortal) in uniPG : 
            for subj in uniPG.subjects(UNICORE.database,dbComplexPortal) : 
                compId = str(subj).split("/")[-1]
                mCompl = MacromolComplex(compId)
                if not mCompl.exists() : 
                    mCompl.create()




    ################################# PROTEIN ENTRY CREATION ####################################################""  
    
    def create(self) :
        uniP = UniProtein(self._id)
        if not uniP.exists() :
            uniP.load()     

        uniPG = uniP.getEntityGraph()

        #temporary RDFLib graph to store the data : the graph will be added to the online rep at the end

        self._createBasicProfile(uniPG,uniP)      
        self._createProteinComplex(uniPG)

        #print(tempG.serialize(format="nt"))

class Isoform(KbEntry): 

    def __init__(self,identifier):
        super().__init__(identifier)
        self._type = AMMODEL.Isoform
        self._pdbBestIsoforms = []

    def get_type(self):
        return self._type

    type = property(get_type,doc="Entry Type")

    def isCanonical(self):
        query="ASK {{ <{subj}> <{prop}> 'True' }}".format(subj=self._stUri,prop=str(AMMODEL.isCanonical))
        params = {"query" : query}
        headers = {}
        r=requests.query(self._queryEndpoint,params=params,headers=headers)
        return r.json()["boolean"]
    
    """
    def get_pdbBestIsoforms(self):
        return self._pdbBestIsoforms

    def set_pdbBestIsoforms(self,value):
        self._pdbBestIsoforms = value

    pdbBestIsoforms = property(get_pdbBestIsoforms,set_pdbBestIsoforms,doc="Entry Type")
    """

    def labelExists(self) :
        query= " ASK {{ <{uri}> <http://www.w3.org/2000/01/rdf-schema#label> ?o }} ".format(uri=self._stUri)     
        headers = {'content-type' : 'application/x-www-form-urlencoded'}
        myparam = { 'query': query }
        r=requests.get(self._queryEndpoint,myparam,headers=headers)    
        labelExists = r.json()['boolean']
        return labelExists

    def addInfoTrans(self,isoUni,isoEnsId,trKB) :
         ########## TRANSCRIPTS INFOS #############################################
        tempG = Graph(identifier=self._uri)
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)
        tempG.add([self._uri,RDF.type, self._type])
        if isoEnsId not in (None,""):
            isoEnsUri = URIRef("http://rdf.ebi.ac.uk/resource/ensembl.protein/" + isoEnsId)
            tempG.add([self._uri,RDFS.seeAlso,isoEnsUri])
        tempG.add([self._uri,RDFS.seeAlso,isoUni])
        tempG.add([self._uri,AMMODEL.translatedFrom,trKB])

        self.addEntityGraphToStore(tempG)  


    def getSecStInfo(self,subj,letter) :
        
        sparql = SPARQLWrapper(UNIQUERY)
        query = "PREFIX faldo:<http://biohackathon.org/resource/faldo#> \
        PREFIX unicore:<http://purl.uniprot.org/core/> \
        SELECT ?start ?end WHERE {{ <{ent1}> unicore:range ?r . \
        ?r faldo:begin ?b . ?b faldo:position ?start . ?r faldo:end ?e . ?e faldo:position ?end }} ".format(ent1=str(subj))
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        seqRange=[]
        try : 
            results=sparql.query().convert()
        except : 
            print("Unable to retrieve sequence range from uniprot local rep")
            return seqRange
        else : 

            for row in results["results"]["bindings"]:
                seqRange.append(row['start']['value']+"-"+row['end']['value']+":"+letter)
            return seqRange


    def getPolyGraph(self,subj) :        
        sparql = SPARQLWrapper(UNIQUERY)
        query = "PREFIX unicore:<http://purl.uniprot.org/core/> \
        CONSTRUCT {{ ?sub unicore:component ?comp .  ?comp  ?prop ?nameRef . ?nameRef ?propL ?label }} \
        WHERE {{ VALUES ?sub {{ <{ent1}> }} ?sub unicore:component ?comp . \
        ?comp  ?prop ?nameRef .  ?nameRef ?propL ?label }} ".format(ent1=str(subj))
        sparql.setQuery(query)
        sparql.setReturnFormat(N3)
        results = sparql.query().convert()

        queryAnnot = "PREFIX unicore:<http://purl.uniprot.org/core/> \
        CONSTRUCT {{ ?sub unicore:annotation ?annot . ?annot ?prop ?r . ?annot a ?type . ?annot <http://www.w3.org/2000/01/rdf-schema#comment> ?com . ?r ?propos ?pos . ?pos ?att ?value }} \
        WHERE {{ VALUES ?sub {{ <{ent1}> }} VALUES ?type {{ unicore:Chain_Annotation unicore:Peptide_Annotation unicore:Propeptide_Annotation  }}  ?sub unicore:annotation ?annot . \
        ?annot ?prop ?r . ?annot a ?type . ?annot <http://www.w3.org/2000/01/rdf-schema#comment> ?com . ?r ?propos ?pos . ?pos ?att ?value }} ".format(ent1=str(subj))
        sparql.setQuery(queryAnnot)
        sparql.setReturnFormat(N3)
        resultsAnnot = sparql.query().convert()

        rg = Graph()
        rg.parse(data=results, format="n3")
        rg.parse(data=resultsAnnot, format="n3")
        #print(rg.serialize(format="n3"))
        return rg


    def getVarGraph(self,subj) :
        sparql = SPARQLWrapper(UNIQUERY)
        queryVar = "PREFIX unicore:<http://purl.uniprot.org/core/> \
                CONSTRUCT {{ ?sub unicore:organism ?org . ?sub unicore:annotation ?annot . ?annot ?prop ?r . ?annot a ?type . ?annot <http://www.w3.org/2000/01/rdf-schema#comment> ?com . \
                ?annot <http://www.w3.org/2000/01/rdf-schema#seeAlso> ?db . \
                ?r ?propos ?pos . ?pos ?att ?value }} \
        WHERE {{ VALUES ?sub {{ <{ent1}> }} VALUES ?type {{ unicore:Natural_Variant_Annotation }} ?sub unicore:organism ?org . ?sub unicore:annotation ?annot . \
        ?annot ?prop ?r . ?annot a ?type . ?annot <http://www.w3.org/2000/01/rdf-schema#comment> ?com . \
        ?annot <http://www.w3.org/2000/01/rdf-schema#seeAlso> ?db . \
        ?r ?propos ?pos . ?pos ?att ?value }} ".format(ent1=str(subj))
        sparql.setQuery(queryVar)
        sparql.setReturnFormat(N3)
        resultsVar = sparql.query().convert()
        rg = Graph()
        rg.parse(data=resultsVar, format="n3")
        #print(rg.serialize(format="n3"))
        return rg


    def getSeqFromRange(self,seqRange,aaCount) :
        seqLetter = []
        for i in range(0, aaCount):
            seqLetter.append("-")
        for item in seqRange:
            tirIndex = item.index("-")
            dotIndex = item.index(":")
            posBegin = item[:tirIndex]
            posEnd = item[tirIndex+1:dotIndex]
            letter = item[dotIndex+1:]
            for i in range(int(posBegin), int(posEnd)+1):
                seqLetter[i-1:i-1]=[letter]
        stSeqL = ""
        for item in seqLetter:
            stSeqL += item
        return stSeqL

    def createVariants(self,varG,uniIso) :
        for subj in varG.subjects(RDF.type,UNICORE.Natural_Variant_Annotation) : 
            #print("VARIANT SUBJ FOUN " + str(subj))
            #print(varG.serialize(format="xml"))
            dbSnp = varG.value(subj,RDFS.seeAlso,None)
            #print("DBSNP" + str(dbSnp))
            if dbSnp != None : 
                dbSnpId = str(dbSnp).split("/")[-1]
                #print("CREATE VARIANT " + dbSnpId)
                var = VariantAa(dbSnpId)
                if not var.exists() : 
                    var.create(varG,subj,uniIso,self._uri)



    def create(self,uniPG,uniP,uniIso) :
        print("iso. "+ self._id)
        tempG = Graph(identifier=self._uri)
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)
        tempG.add([self._uri,RDF.type, self._type])
        tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])

        ########## CREATING THES ENTRY #############################################
        #mainLabel = uniPG.value(uniIso,UNICORE.name)
        #if not isinstance(mainLabel,Literal) : mainLabel=Literal("unnamed ("+self._id+")")

        mainLabel = Literal(self._id)

        tempG.add([self._uri,RDFS.label,mainLabel])
        dtr=ThesEntry(STR_UNINAME+"_"+self._id,STR_UNINAME)
        if not dtr.exists() :                  
            dtr.createThesPrefOnly(str(mainLabel))
            for obj in uniPG.objects(uniIso,UNICORE.name) : 
                if obj != mainLabel and str(obj) not in ["1","2","3","4","5","6","7","8","9","10","11","12"] :
                    dtr.addAltLabel(obj)                    
        tempG.add([self._uri,AMMODEL.thesRef,dtr.uri])

        ########## CANONICAL ???  ##################################################
        canonical=False
        if (uniIso,UNICORE.basedOn,None) not in uniPG : 
            tempG.add([self._uri,AMMODEL.isCanonical,Literal("True",datatype=XSD.boolean)])
            canonical=True

        ########## PRECURSOR  ##################################################
        if (uniIso,UNICORE.precursor,None) in uniPG :
            tempG.add([self._uri,AMMODEL.isPrecursor,Literal("True",datatype=XSD.boolean)])



        ###### VARIANTS #######################################################

        if canonical :
            #print("GET VARIANTS")
            varG = self.getVarGraph(uniP.uri)
            if (None,RDF.type,UNICORE.Natural_Variant_Annotation) in varG :         
                #Variation = URIRef(self._stUri+"#VariantRef")
                #tempG.add([self._uri,AMMODEL.variantRef,Variation])
                #tempG.add([Variation,RDF.type,URIRef(STR_AMMODEL+"VariantRef")])
                #print("CREATEV VARIANTS")
                self.createVariants(varG,uniIso)



        ###### SIGNATURES #######################################################
        
        #SignatureSet = URIRef(self.stUri+"#SignatureSet")
        #tempG.add([self._uri,AMMODEL.signatureSet,SignatureSet])        
        requestURL = "https://www.ebi.ac.uk/proteins/api/uniparc/accession/"+self._id
        r = requests.get(requestURL, headers={ "Accept" : "application/json"})
        if not r.ok:
            if "-1" in self._id : 
                protId = self._id[:-2]
                requestURL = "https://www.ebi.ac.uk/proteins/api/uniparc/accession/"+protId
                r = requests.get(requestURL, headers={"Accept" : "application/json"})
        data=r.json()
        if "signatureSequenceMatch" in data : 
            for row in data["signatureSequenceMatch"] :
                #row["lcn"]["start"]
                #row["lcn"]["end"]
                pref = row["ipr"]["name"]
                idMatch= row["ipr"]["id"]
                also = URIRef("https://www.ebi.ac.uk/interpro/entry/"+idMatch)
                #match = URIRef(STR_AMTHES+"Interpro_"+idMatch)
                db=row["database"]
                idDb=row["id"]

                if db == "Gene3D" :
                    idDb = idDb.split(":")[-1]
                    dcath=ThesEntry("CATH_"+idDb,"CATH")
                    if not dcath.exists() :
                        #dcath.createThesPrefAndMatch(pref,also,match)
                        dcath.createThesPrefAndMatch(pref,also,idMatch)
                    elif not dcath.existsLiteral(idMatch) :
                        dcath.addLiteral(SKOS.notation,idMatch)
                        dcath.addResource(RDFS.seeAlso,also) 
                    tempG.add([self._uri,AMMODEL.signature,dcath.uri])
                else :
                    dthes = ThesEntry(db+"_"+idDb,db)
                    if not dthes.exists() :
                        dthes.createThesPrefAndMatch(pref,also,idMatch)
                    elif not dthes.existsLiteral(idMatch) :
                        dthes.addLiteral(SKOS.notation,idMatch)
                        dthes.addResource(RDFS.seeAlso,also) 
                    tempG.add([self._uri,AMMODEL.signature,dthes.uri])


        ########## STRUCTURE  ##################################################
        PrimStruct = URIRef(self.stUri+"#PrimStruct")     
        primSeq = uniPG.value(uniIso,RDF.value,None)        
        tempG.add([self._uri,AMMODEL.primStruct,PrimStruct])
        aaCount=0
        if isinstance(primSeq,Literal) : 
            #tempG.add([self._uri,AMMODEL.primStruct,PrimStruct])
            tempG.add([PrimStruct,AMMODEL.sequence,primSeq])
            tempG.add([self._uri,AMMODEL.aaCount,Literal(len(primSeq))])
            tempG.add([PrimStruct,AMMODEL.checksum,Literal(uniPG.value(uniIso,UNICORE.crc64Checksum,None))])
            aaCount=len(primSeq)
            analysed_seq = ProteinAnalysis(primSeq)
            try :
                mol_weight = analysed_seq.molecular_weight()
            except : 
                print("problem wit mol Weight")
            else : 
                tempG.add([self._uri,AMMODEL.molWeight,Literal(round(mol_weight,2))])
            try :            
                instability = analysed_seq.instability_index()
            except KeyError :
                pass
            else : 
                tempG.add([self._uri,AMMODEL.instabilityIndex,Literal(round(instability,3))])
            #flexibility = analysed_seq.flexibility()
            #tempG.add([self._uri,AMMODEL.flexibility,Literal(flexibility)])
            aromaticity = analysed_seq.aromaticity()
            tempG.add([self._uri,AMMODEL.aromaticity,Literal(round(aromaticity,3))])
            amino_percent = analysed_seq.get_amino_acids_percent()
            tempG.add([self._uri,AMMODEL.aminoPercent,Literal(json.dumps(amino_percent))])
            isoelectric_point = analysed_seq.isoelectric_point()
            tempG.add([self._uri,AMMODEL.isoelectricPoint,Literal(round(isoelectric_point,3))])


        SecStruct = URIRef(self.stUri+"#SecStruct")
        tempG.add([self._uri,AMMODEL.secStruct,SecStruct])
        if canonical :   
            seqRange = []        
            for subj in uniPG.subjects(RDF.type,UNICORE.Helix_Annotation) :
                seqRange.extend(self.getSecStInfo(subj,"H"))
            for subj in uniPG.subjects(RDF.type,UNICORE.Beta_Strand_Annotation) :
                seqRange.extend(self.getSecStInfo(subj,"B"))
            for subj in uniPG.subjects(RDF.type,UNICORE.Turn_Annotation) :
                seqRange.extend(self.getSecStInfo(subj,"T"))


            seqRange.sort()
            seqR = ""
            for item in seqRange : 
                seqR += item + ","
            seqR = seqR[:-1]
            if seqR !="" : 
                tempG.add([SecStruct,AMMODEL.seqRange,Literal(seqR)])
            if len(seqRange)>1 and aaCount > 0 : 
                sequence = self.getSeqFromRange(seqRange,aaCount)
                tempG.add([SecStruct,AMMODEL.sequence,Literal(sequence)])


        TerStruct = URIRef(self.stUri+"#TerStruct")
        tempG.add([self._uri,AMMODEL.terStruct,TerStruct])
        nb=0;
        for subj in uniPG.subjects(UNICORE.database,URIRef('http://purl.uniprot.org/database/PDB')) :
            pdb = str(subj)
            lastIndex = pdb.rfind("/")
            pdbId = pdb[lastIndex+1:]
            if canonical : 
                tempG.add([TerStruct,AMMODEL.pdb,subj])

            #else :

                #try : 
                   # r = requests.get("https://www.ebi.ac.uk/pdbe/api/mappings/all_isoforms/"+pdbId)
                #except : 
                    #print("unable to retrieve pdb structures")
                #else : 
                    #data = r.json()
                    #items = data[pdbId.lower()]
                    #isoforms = items['UniProt']
                    #for elt in isoforms : 
                        #if elt == self._id : 
                            #tempG.add([TerStruct,AMMODEL.pdb,subj])
   


        ########## PROCESSED COMPONENTS ################################################
        if canonical : 

            if (uniP.uri,UNICORE.component,None) :
                polyG = self.getPolyGraph(uniP.uri)
                #getting info unicore:Chain_Annotation unicore:Peptide_Annotation unicore:Propeptide_Annotation 
            for obj in polyG.objects(uniP.uri,UNICORE.annotation):
                poly=str(obj)
                lastIndex = poly.rfind("/")
                polyId = poly[lastIndex+1:]
                poly=Polypeptide(polyId)
                if not poly.exists() : 
                    poly.create(uniPG,uniIso,polyG,obj)
                tempG.add([self._uri,AMMODEL.cleavedInto,poly.uri])


        self.addEntityGraphToStore(tempG)  




    """
    def retrieveENSP(self) :
        url = "https://www.uniprot.org/uploadlists/"
        params = {"from":"ACC", "to":"P_REFSEQ_AC", "format" : "tab", "query" : "P13368 P20806 Q9UM73 P97793 Q17192"}
        header = {"User-Agent" : "Python lnoel@inria.fr"}
        r = requests.get(url, params=params, headers=header)
    """



class VariantAa(KbEntry): 

    def __init__(self,identifier):
        super().__init__(identifier)
        self._type = AMMODEL.VariantAa

    def get_type(self):
        return self._type

    type = property(get_type,doc="Entry Type")



    def addVda(self,disVarUri) : 


        disVarId = str(disVarUri).split("/")[-1]
        queryVda= "SELECT  DISTINCT ?vda ?disease ?vdaScore FROM <{g}> WHERE {{ ?vda a sio:SIO_000897 . ?vda sio:SIO_000628 <{disvUri}>, ?disease .  ?vda sio:SIO_000216 ?valueR . ?valueR sio:SIO_000300 ?vdaScore .  FILTER (?vdaScore >= 0.2)  VALUES ?disType {{sio:SIO_010299 ncit:C7057}} ?disease a ?disType .   }}".format(g="http://rdf.disgenet.org",disvUri=str(disVarUri))
        #result = DisgenetEndpoint().execQuery(queryVda,JSON)
        requestURL = DisgenetEndpoint().endpoint
        myparam = { 'query': queryVda }
        try : 
            r = requests.get(requestURL, params=myparam,headers={ "Content-type":"application/x-www-form-urlencoded","Accept" : "application/json"})
        except : 
            print("Unable to retrieve info from Disgenet")
        else : 
            try : 
                result = r.json()
            except :
                print("problem retrieving variant disease info from disgenet")
            else : 
                for row in result["results"]["bindings"] : 
                    disDisStr = row['disease']['value']
                    vdaScore = row['vdaScore']['value']
                    disDisResult = self._getMondo(disDisStr)
                    #print("mondo URI " + disDisUri)
                    for elt in disDisResult :
                        disDisUri = URIRef(elt)
                        disDisId = elt.split("/")[-1]
                        #disDisId = disDisId.split("_")[-1]
                        #print("mondo Id " + disDisUri)
                        disease = Disease(disDisId)
                        if not disease.exists() :
                            disease.create()

                        #http://linkedlifedata.com/resource/umls/id/C1863052
                        vdaId = disVarId+"_"+disDisId
                        vda = Vda(vdaId,vdaScore,self._uri,disease.uri)
                        if not vda.exists() : 
                            vda.create(disVarUri,disDisUri)





    def create(self,varG,uniVarUri,uniIso,isoform) :
        #print("creating var : "+ self._id)
        tempG = Graph()
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)
        tempG.add([self._uri,RDF.type, self._type])
        tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])
        tempG.add([isoform,AMMODEL.variant,self._uri])

        faldo = "http://biohackathon.org/resource/faldo#"

        #link towards swiss prot variant page
        uniId = str(uniVarUri).split("/")[-1]
        tempG.add([self._uri,RDFS.seeAlso,URIRef("https://web.expasy.org/variant_pages/"+uniId+".html")])

        #comment
        if (uniVarUri,RDFS.comment,None) in varG : 
            tempG.add([self._uri,AMMODEL.description, Literal(varG.value(uniVarUri,RDFS.comment))])

        #substitution
        if (uniVarUri,UNICORE.substitution,None) in varG : 
            tempG.add([self._uri,AMMODEL.substitution, Literal(varG.value(uniVarUri,UNICORE.substitution))])

        #Ajout de la position par rapport à l'isoforme
        range = varG.value(uniVarUri,UNICORE.range,None)
        begin = varG.value(range,URIRef(faldo+"begin"),None)
        end = varG.value(range,URIRef(faldo+"end"),None)
        posBegin = varG.value(begin,URIRef(faldo+"position"),None)
        posEnd = varG.value(end,URIRef(faldo+"position"),None)
        seqRange = str(posBegin) + "-" + str(posEnd)
        uniIsoId = str(uniIso).split("/")[-1]
        posRef = URIRef(STR_AMKB + self._id+"_"+uniIsoId)
        tempG.add([self._uri,AMMODEL.positionRef,posRef])
        tempG.add([posRef,RDF.type,AMMODEL.PositionRef])
        tempG.add([posRef,AMMODEL.variant,self._uri])
        tempG.add([posRef,AMMODEL.isoform,Isoform(uniIsoId).uri])
        tempG.add([posRef,AMMODEL.position,Literal(seqRange)])

        #TO DO : Récupérer les polypeptides, vérifier si position de début est inférieure à position variant et si position de fin est supérieure
        
        #si l'organisme est homo sapiens, on va chercher les maladies
        #print("SI ORGANISM HOMOSAPIENS,ADD VDA")
        if (None,UNICORE.organism,URIRef("http://purl.uniprot.org/taxonomy/9606")) in varG :
            #print("HOMOSAPIENS")
            self.addVda(URIRef("http://identifiers.org/dbsnp/"+self._id))     

        self.addEntityGraphToStore(tempG)  



class Polypeptide(KbEntry): 

    def __init__(self,identifier):
        super().__init__(identifier)
        self._type = AMMODEL.Polypeptide

    def get_type(self):
        return self._type

    type = property(get_type,doc="Entry Type")


    def create(self,uniPG,uniIso,polyG,uniAnnotRef) :
        print("poly: "+ self._id)
        tempG = Graph(identifier=self._uri)
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)
        tempG.add([self._uri,RDF.type, self._type])
        tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])

        #id = PRO....
        #avant de créer l'entrée, il faut faire le lien entre annot et comp via les libellés ...
        faldo = "http://biohackathon.org/resource/faldo#"

        annotLabel = polyG.value(uniAnnotRef,RDFS.comment,None)
        #print(annotLabel)

        if (None,UNICORE.fullName,annotLabel) in polyG :
            #print("polyp correspondance found")
            name = polyG.value(None,UNICORE.fullName,annotLabel)
            #print("name Ref: " + str(name))
            #for subj in polyG.subjects(None,None,name)
            uniCompRef = polyG.value(None,UNICORE.recommendedName,name)
            #print("unicomp: " + str(uniCompRef))
            #print(str(uniCompRef) + " " + str(uniAnnotRef) + " " + annotLabel)

            dtr=ThesEntry(STR_AMNAME+"_"+self._id,STR_AMNAME)
            if not dtr.exists() :                  
                dtr.createThesProt(polyG,uniCompRef)
            tempG.add([self._uri,AMMODEL.thesRef,dtr.uri])   

        tempG.add([self._uri,RDFS.label,annotLabel])
        
        #Ajout du type "Chain", "Propeptide" ...
        typeUniA = polyG.value(uniAnnotRef,RDF.type,None)
        fullLabel = str(typeUniA)[str(typeUniA).rfind("/")+1:]
        typeLabel = fullLabel[:fullLabel.index("_")]
        #print("typeLabel" + typeLabel)
        typeA = URIRef(STR_AMMODEL + typeLabel)
        tempG.add([self._uri,RDF.type,typeA])



        ########## STRUCTURE  ##################################################
        PrimStruct = URIRef(self.stUri+"#PrimStruct")     
        tempG.add([self._uri,AMMODEL.primStruct,PrimStruct])


        #Ajout de la position par rapport à l'isoforme
        range = polyG.value(uniAnnotRef,UNICORE.range,None)
        begin = polyG.value(range,URIRef(faldo+"begin"),None)
        end = polyG.value(range,URIRef(faldo+"end"),None)
        posBegin = polyG.value(begin,URIRef(faldo+"position"),None)
        posEnd = polyG.value(end,URIRef(faldo+"position"),None)
        seqRange = str(posBegin) + "-" + str(posEnd)

        uniIsoId = str(uniIso).split("/")[-1]
        posRef = URIRef(STR_AMKB+self._id+"_"+uniIsoId)
        tempG.add([PrimStruct,AMMODEL.positionRef,posRef])
        tempG.add([posRef,RDF.type,AMMODEL.PositionRef])
        tempG.add([posRef,AMMODEL.polypeptide,self._uri])
        tempG.add([posRef,AMMODEL.isoform,Isoform(uniIsoId).uri])
        tempG.add([posRef,AMMODEL.position,Literal(seqRange)])

        #tempG.add([PrimStruct,AMMODEL.positionOnIsoform,Literal(seqRange)])
        
        
        #iAjout de la séquence
        if posBegin != None and posEnd != None :
            if (uniIso,RDF.value,None) in uniPG :
                seqIso = uniPG.value(uniIso,RDF.value,None)            
                seq = seqIso[int(str(posBegin)):int(str(posEnd))+1]
                tempG.add([PrimStruct,AMMODEL.sequence,Literal(seq)])
                tempG.add([PrimStruct,AMMODEL.aaCount,Literal(len(seq))])

        self.addEntityGraphToStore(tempG)  


class Fragment(KbEntry): 

    def __init__(self,identifier):
        super().__init__(identifier)
        self._type = AMMODEL.Fragment

    def get_type(self):
        return self._type

    type = property(get_type,doc="Entry Type")


    def create(self,label,sequence,source) :
        print("frag. "+ self._id)
        tempG = Graph(identifier=self._uri)
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)
        tempG.add([self._uri,RDF.type, self._type])
        tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])
        tempG.add([self._uri,AMMODEL.source, Literal(source)])
        #tempG.add([self._uri,AMMODEL.amyloidogenic,Literal("True",datatype=XSD.boolean)])

        # pas d'entrée dans le thésaurus, on créé uniquement un label
        # dtr=ThesEntry(sourceName+"_"+self._id,sourceName)
        # if not dtr.exists() :                  
        #     dtr.createThesProt(polyG,uniCompRef,self._type)
        #tempG.add([self._uri,AMMODEL.thesRef,dtr.uri])   
        tempG.add([self._uri,RDFS.label,Literal(label)])
       
        
        ########## STRUCTURE  ##################################################
        PrimStruct = URIRef(self.stUri+"#PrimStruct")     
        tempG.add([self._uri,AMMODEL.primStruct,PrimStruct])
        tempG.add([PrimStruct,AMMODEL.sequence,Literal(sequence)])

       
        self.addEntityGraphToStore(tempG)  



class MacromolComplex(KbEntry): 

    def __init__(self,identifier):
        super().__init__(identifier)
        self._type = AMMODEL.MacromolComplex

    def get_type(self):
        return self._type

    type = property(get_type,doc="Entry Type")


    def create(self) :
        print("complex "+ self._id)
        tempG = Graph(identifier=self._uri)
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)
        tempG.add([self._uri,RDF.type, self._type])
        tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])

        compUrl = "https://www.ebi.ac.uk/intact/complex-ws/complex/"+self._id

        try : 
            r=requests.get(compUrl)
        except : 
            print("unable to retrieve info for protein complex from ebi")
        else : 
            data=r.json()


            #thesaurus entry + label
            
            pref = tempG.add([self._uri,RDFS.label,Literal(data["name"])])
            
            dtr=ThesEntry(STR_AMNAME+"_"+self._id,STR_AMNAME)
            if not dtr.exists():
                dtr.createThesMacro(pref,data["synonyms"])
            tempG.add([self._uri,AMMODEL.thesRef,dtr.uri])   

            #organism
            if "species" in data :
                orgId = data["species"].split(";")[-1].strip()
                #print(orgId)
                tempG.add([self._uri,AMMODEL.organism, URIRef(STR_AMTHES+Uniprot().name+"_taxonomy_"+orgId)])

            #AMMODEL.description "desc"
            if "properties" in data :
                tempG.add([self._uri,AMMODEL.description, Literal(data["properties"])])

            if "functions" in data :
                tempG.add([self._uri,AMMODEL.description, Literal(data["functions"])])


            #participant + part of 
            for row in data["participants"] : 
                identif = row["identifier"]
                if "PRO_" in identif : 
                    lastIndex = identif.rfind("PRO_")
                    polyId = identif[lastIndex:].strip()
                    polyp = Polypeptide(polyId)
                    if not polyp.exists() :
                        if "name" in row and "description" in row :
                            if row["name"] != None and row["description"] != None : 
                                tempG.add([self._uri,AMMODEL.participant, Literal("[" + row["name"] + "] " + row["description"])])
                    else :
                        tempG.add([self._uri,AMMODEL.participant,polyp.uri])
                        tempG.add([polyp.uri,AMMODEL.partOf,self._uri])
                else :
                    if "name" in row  and "description" in row :
                        #print(row["name"])
                        #print(row["description"])
                        if row["name"] != None and row["description"] != None :
                            tempG.add([self._uri,AMMODEL.participant, Literal("[" + row["name"] + "] " + row["description"])])

            
        #AMMODEL.disease

        

        self.addEntityGraphToStore(tempG)  




class Gene(KbEntry): 

    def __init__(self,identifier):
        super().__init__(identifier)
        self._type = AMMODEL.Gene

    def get_type(self):
        return self._type

    type = property(get_type,doc="Entry Type")



    def getDiseases(self) :
     
        query="PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX owl: <http://www.w3.org/2002/07/owl#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        SELECT distinct ?diseaseId ?diseaseLabel ?diseaseDesc ?score \
        WHERE {{\
        <{gene}> model:mentionedIn ?gda .\
        ?gda rdf:type model:Gda . \
        ?gda model:score ?score . \
        ?gda model:referringTo ?disease . \
        ?disease rdf:type model:Disease . \
        ?disease rdfs:label ?diseaseLabel . \
        ?disease model:accession ?diseaseId . \
        ?disease model:description ?diseaseDesc . \
        }}".format(gene=self._stUri)
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        data = []
        for row in results["results"]["bindings"] :
            dict =  {}
            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""
            data.append(dict)
        return data



    def getProteins(self) :
     
        query="PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX owl: <http://www.w3.org/2002/07/owl#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        SELECT distinct ?proteinLabel ?proteinId \
        WHERE {{\
        <{gene}> model:encodes ?protein .\
        ?protein rdf:type model:Protein . \
        ?protein rdfs:label ?proteinLabel . \
        ?protein model:accession ?proteinId . \
        }}".format(gene=self._stUri)
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        data = []
        for row in results["results"]["bindings"] :
            dict =  {}
            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""
            data.append(dict)
        return data


    def getBioThread(self) :
        dataAll = {} #what we will return in the end
        data = [] # to store gene-disease-relation
        node_colors = {} # to store color info
        legend = []
        null = None


        sparql = SPARQLWrapper(self._queryEndpoint)
        query="PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX owl: <http://www.w3.org/2002/07/owl#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        SELECT distinct ?geneLabel ?transLabel ?isoLabel \
        WHERE {{ {{<{gene}> model:transcript ?trans .\
        <{gene}> rdfs:label ?geneLabel . \
        ?trans rdfs:label ?transLabel . \
        FILTER NOT EXISTS {{?trans model:translatedTo ?iso }} \
        }} UNION {{ \
        <{gene}> model:transcript ?trans . \
        <{gene}> rdfs:label ?geneLabel . \
        ?trans rdfs:label ?transLabel . \
        ?trans model:translatedTo ?iso . \
        ?iso rdfs:label ?isoLabel . \
        }} }} ".format(gene=self._stUri)

        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        #print(r.status_code)
        gcolor="hsla(157, 34%, 40%)"
        pcolor = "hsla(189, 98%, 28%)"
        legend = [{"text": "Genes and transcripts","iconFill": gcolor}, {"text": "Isoforms","iconFill": pcolor}]

        for row in results["results"]["bindings"] : 
            gene = row["geneLabel"]["value"]
            transcript = row["transLabel"]["value"]

            dictGT = {}
            dictGT["from"] = gene
            dictGT["to"] = transcript
            dictGT["weight"] = 1
            if dictGT not in data : 
                data.append(dictGT)

            if gene not in node_colors :                 
                node_colors[gene] = gcolor

            if transcript not in node_colors :
                node_colors[transcript] = gcolor


            if "isoLabel" not in row : 
                dictTI = {}
                dictTI["from"] = transcript
                dictTI["to"] =  null
                dictTI["weight"] = 1
                if dictTI not in data : 
                    data.append(dictTI) 

            else : 
                iso = row["isoLabel"]["value"]
                dictTI = {}
                dictTI["from"] = transcript
                dictTI["to"] =  iso
                dictTI["weight"] = 1
                if dictTI not in data : 
                    data.append(dictTI) 

                if iso not in node_colors :
                    node_colors[iso] = pcolor
   
        dataAll["data"]=data
        dataAll["node_colors"]=node_colors
        dataAll["legend"] = legend

        return dataAll


    def getPheno(self) :
        query="PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX owl: <http://www.w3.org/2002/07/owl#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        SELECT ?phenoLabel \
        WHERE {{ \
        <{gene}> model:mentionedIn ?gda .\
        ?gda model:referringTo ?disease . \
        ?disease rdf:type model:Disease . \
        ?disease model:phenoRef ?phenoThes .\
        ?phenoThes skos:prefLabel ?phenoLabel .\
        }} ".format(gene=self._stUri)
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        data = []
        for row in results["results"]["bindings"] :
            data.append(row["phenoLabel"]["value"])
        return data


    def getMainInfo(self) :

        query="PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX owl: <http://www.w3.org/2002/07/owl#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        SELECT ?label ?organism\
        (GROUP_CONCAT(DISTINCT ?desc; SEPARATOR=' ') AS ?description) \
        (GROUP_CONCAT(DISTINCT ?altLabel; SEPARATOR=', ') AS ?altList) \
        WHERE {{\
        <{gene}> rdfs:label ?label . \
        <{gene}> model:description ?desc .\
        <{gene}> model:organism ?org . ?org skos:prefLabel ?organism .\
        OPTIONAL {{<{gene}> model:thesRef ?ref . ?ref skos:altLabel ?altLabel }}\
        }} GROUP BY ?label ?organism ".format(gene=self._stUri)
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        dict =  {}
        for row in results["results"]["bindings"] :

            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""

        return dict

    def addGda(self,disGenUri) : 
        #queryGda= "SELECT  DISTINCT ?disease ?gdaScore ?pubmed ?source FROM <{g}> WHERE {{ ?gda sio:SIO_000628 <{disgUri}>, ?disease .  ?gda sio:SIO_000216 ?valueR .  ?valueR sio:SIO_000300 ?gdaScore .  FILTER (?gdaScore > 0.2) ?gda sio:SIO_000772 ?pubmed . ?gda sio:SIO_000253 ?source .    VALUES ?disType {sio:SIO_010299 ncit:C7057} ?disease a ?disType .   }}".format(g="http://rdf.disgenet.org",disgUri=str(disgenetUri))
        disGenId = str(disGenUri).split("/")[-1]
        #print("GDA "+ disGenId + " " + disGenUri)
        queryGda= """
        PREFIX sio:<http://semanticscience.org/resource/>  
        PREFIX ncit:<http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#>
        SELECT  DISTINCT ?gda ?disease ?gdaScore 
        FROM <{g}> 
        WHERE {{ ?gda sio:SIO_000628 <{disgUri}>, ?disease .  ?gda sio:SIO_000216 ?valueR .  
        ?valueR sio:SIO_000300 ?gdaScore .  FILTER (?gdaScore >= 0.2) 
        VALUES ?disType {{ sio:SIO_010299 ncit:C7057 }} 
        ?disease a ?disType .   }}""".format(g="http://rdf.disgenet.org",disgUri=str(disGenUri))
        #print("QUERY " + queryGda)

        requestURL = DisgenetEndpoint().endpoint
        myparam = { 'query': queryGda }
        try : 
            r = requests.get(requestURL, params=myparam,headers={ "Content-type":"application/x-www-form-urlencoded","Accept" : "application/json"})
        except : 
            print("Unable to retrieve info from Disgenet for GDA")
        else :
            if r.ok : 
                result = r.json()
                for row in result["results"]["bindings"] : 
                    disDisStr = row['disease']['value']
                    gdaScore = row['gdaScore']['value']
                    disDisResult = self._getMondo(disDisStr)
                    
                    for elt in disDisResult : 
                        disDisUri = URIRef(elt)
                        disDisId = elt.split("/")[-1]
                        #disDisId = disDisId.split("_")[-1]
                        disease = Disease(disDisId)
                        if not disease.exists() :
                            disease.create()

                #http://linkedlifedata.com/resource/umls/id/C1863052
                        gdaId = disGenId+"_"+disDisId
                        gda = Gda(gdaId,gdaScore,self._uri,disease.uri)
                        if not gda.exists() : 
                            gda.create(disGenUri,disDisUri)
                

    def create(self,uniPG) :
        print("gene "+ self._id)
        tempG = Graph(identifier=self._uri)
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)
        tempG.add([self._uri,RDF.type, self._type])
        tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])        
        stEnsGuri = STR_ENS+self._id
        ensGuri=URIRef(STR_ENS+self._id)
        tempG.add([self._uri,RDFS.seeAlso,ensGuri])
           

        
        """
        ensGG = Graph()

        queryAll=" DESCRIBE <{val}> ".format(val=stEnsGuri)
        queryTrans= "CONSTRUCT {{ ?s ?p ?o . ?s <http://www.w3.org/2000/01/rdf-schema#label> ?label }} WHERE {{ ?s <http://www.w3.org/2000/01/rdf-schema#label> ?label . ?s ?p ?o . VALUES ?p {{<http://purl.obolibrary.org/obo/SO_transcribed_from>}} VALUES ?o {{<{val}>}} }}".format(val=stEnsGuri)
      
        requestURL = EbiEndpoint().endpoint
        myparam = { 'query': queryAll , 'format' : 'XML'}
        try : 
            result = requests.get(requestURL, params=myparam,headers={ "Content-type":"application/x-www-form-urlencoded","Accept" : "application/rdf+xml" })
            ensGG.parse(data=result.content.decode(), format="xml")
        except : 
            print("Unable to retrieve transcript info from EBI")  

        try :
            myparam = { 'query': queryTrans , 'format' : 'XML'}
            results2 = requests.get(requestURL, params=myparam,headers={ "Content-type":"application/x-www-form-urlencoded","Accept" : "application/rdf+xml" })
            ensGG.parse(data=results2.content.decode(), format="xml")

        except : 
            print("Unable to retrieve transcript info from EBI")             

        else :          



            for obj in ensGG.objects(ensGuri,RDFS.seeAlso) :
                 tempG.add([self._uri,RDFS.seeAlso,obj])             
            
            if (ensGuri,DC.description,None) in ensGG : 
                tempG.add([self._uri,AMMODEL.description,ensGG.value(ensGuri,DC.description,None)])

            #taxon (organism)
            if (None,URIRef("http://purl.obolibrary.org/obo/RO_0002162"),None) in ensGG :
                #print("add taxon to gene")
                org = ensGG.value(ensGuri,URIRef("http://purl.obolibrary.org/obo/RO_0002162"),None)
                lastIndex = str(org).rfind("/")
                orgId = str(org)[lastIndex+1:]
                dorg=ThesEntry(Uniprot().name+"_taxonomy_"+orgId,Uniprot().name+"_taxonomy")
                if not dorg.exists() :
                    dorg.createThesOrg()
                tempG.add([self._uri,AMMODEL.organism,dorg.uri])


           #transcripts 
            #print(ensGG.serialize(format="xml"))
            if (None,URIRef('http://purl.obolibrary.org/obo/SO_transcribed_from'),None) in ensGG :
                #print("adding transcripts to gene")
                for subj in ensGG.subjects(URIRef('http://purl.obolibrary.org/obo/SO_transcribed_from'),ensGuri) :
                    #print("there are transcripts for gene "+self.stUri)
                    lastIndex = str(subj).rfind("/")
                    trId = str(subj)[lastIndex+1:]
                    tr = Transcript(trId)
                    trlabel = "unnamed"
                    if (subj,RDFS.label,None) in ensGG :
                        trlabel = ensGG.value(subj,RDFS.label,None)
                    tr.prefLabel=trlabel
                    if not tr.exists() :
                        tr.create(uniPG)
                    tempG.add([self._uri,AMMODEL.transcript,tr.uri])


                    #GDA

            """
            
        """
        ncbiID = ""
        if (None,UNICORE.database,URIRef("http://purl.uniprot.org/database/GeneID")) in uniPG : 
            subj = uniPG.value(UNICORE.database,URIRef("http://purl.uniprot.org/database/GeneID"))
            ncbiID = str(subj).split("/")[-1]
            #print("ncbiID "+ ncbiID)
            if ncbiID is not None and ncbiID != "" and ncbiID != "None" :
                tempG.add([self._uri,RDFS.seeAlso,URIRef("https://www.ncbi.nlm.nih.gov/gene/"+ncbiID)])
            #https://identifiers.org/ncbigene:351
            
            
            
            
        if (None,UNICORE.database,URIRef("http://purl.uniprot.org/database/DisGeNET")) in uniPG or ncbiID !="" :
            dis=""
            for disGenUri in uniPG.subjects(UNICORE.database,URIRef("http://purl.uniprot.org/database/DisGeNET")):
                self.addGda(disGenUri)
                dis = str(disGenUri)
            if dis == "" :
                disGenUri = URIRef("https://identifiers.org/ncbigene/"+ncbiID) 
                self.addGda(disGenUri)
                
        """

        self.addEntityGraphToStore(tempG)




# class Disease(KbEntry): 

#     def __init__(self,identifier):
#         super().__init__(identifier)
#         self._type = AMMODEL.Disease

#     def get_type(self):
#         return self._type

#     type = property(get_type,doc="Entry Type")

#     def create(self) :

#         queryPref = 'PREFIX skos-xl: <http://www.w3.org/2008/05/skos-xl#> \
#             PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
#             PREFIX umls-semnetwork: <http://linkedlifedata.com/resource/semanticnetwork/id/> \
#             PREFIX umls: <http://linkedlifedata.com/resource/umls/> \
#             PREFIX umls-concept: <http://linkedlifedata.com/resource/umls/id/> \
#             SELECT distinct ?pref \
#             WHERE {{ \
#             umls-concept:{id} skos-xl:prefLabel / skos-xl:literalForm ?pref . \
#             }}'.format(id=self._id)

#         result = DisgenetEndpoint().execQuery(queryPref,JSON)
#         try :
#             pref = result['results']['bindings'][0]['pref']['value']

#             print("creating Disease with id : "+ self._id)     
#             tempG = Graph(identifier=self._uri)
#             tempG.bind('skos',SKOS)
#             tempG.bind('rdf',RDF)
#             tempG.bind('rdfs',RDFS)
#             tempG.bind('amkb',AMKB)
#             tempG.bind('ammodel',AMMODEL)
#             tempG.bind('amthes',AMTHES)
#             tempG.add([self._uri,RDF.type, self._type])
#             tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])

#             tempG.add([self._uri,RDFS.label,Literal(pref)])        
#             dt=ThesEntry(UMLS().name+"_"+self._id,UMLS().name)
#             if not dt.exists() :                  
#                 dt.createThesDisease(AMMODEL.Disease)
#             tempG.add([self._uri,AMMODEL.thesRef,dt.uri])

#             #queryDisease = "SELECT DISTINCT ?mondo WHERE {{  ?mondo skos:exactMatch|skos:closeMatch <{umls}> }}".format(umls=disDisUri)
#             #r = OntobeeEndpoint().execQuery(queryDisease,JSON)
#             #for dis in r["results"]["bindings"] : 
#                 #dismondo = dis['mondo']['value']
            

#             ## add phenotype
#             queryPhen ="SELECT distinct str(?phenotypeTitle) as ?phenotypeName \
#                         FROM <{g}> \
#                         WHERE {{ \
#                         ?pda a sio:SIO_000897 . \
#                         ?pda sio:SIO_000628 <{disease}> . \
#                         ?phenotype a sio:SIO_010056 . \
#                         ?pda sio:SIO_000628 ?phenotype . \
#                         ?phenotype dcterms:title ?phenotypeTitle . \
#                         ?pda sio:SIO_000253 ?source . \
#                         ?source dcterms:title ?sourceTitle . \
#                         FILTER REGEX(?sourceTitle,'HPO') \
#                         FILTER (?disease != ?phenotype) \
#                         }} ".format(disease="http://linkedlifedata.com/resource/umls/id"+self._id, g="http://rdf.disgenet.org")

#             resultPhen = DisgenetEndpoint().execQuery(queryPhen,JSON)
            
#             try : 
#                 phen = resultPhen["results"]["bindings"][0]["phenotypeName"]["value"]
#                 phenoUri = URIRef(self._uri+"#Phenotype")
#                 tempG.add([self._uri,AMMODEL.phenotype,phenoUri])
#                 for row in resultPhen["results"]["bindings"]:
#                     tempG.add([phenoUri,AMMODEL.characteristics,Literal(row["phenotypeName"]["value"])])
#             except : 
#                 print("no phenotype value")
           
#             self.addEntityGraphToStore(tempG) 

#         except : 
#             print("Disease "+ self._id + "not created")


class Disease(KbEntry): 

    def __init__(self,identifier):
        super().__init__(identifier)
        self._type = AMMODEL.Disease

    def get_type(self):
        return self._type

    type = property(get_type,doc="Entry Type")

    def getProteins(self) :
     
        query="""PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX owl: <http://www.w3.org/2002/07/owl#> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        SELECT ?proteinLabel ?proteinId  (COUNT(distinct ?pub) as ?nb) 
         (GROUP_CONCAT(DISTINCT ?desc; SEPARATOR=' ') AS ?proteinDesc) 
        WHERE {{
        <{dis}> model:mentionedIn ?proda .
        ?proda rdf:type model:Proda . 
        ?proda model:referringTo ?protein . 
        OPTIONAL {{ ?proda model:supportingEvidence ?supEvi . 
        ?supEvi model:publication ?pub . }}
        ?protein rdf:type model:Protein . 
        ?protein rdfs:label ?proteinLabel . 
        ?protein model:accession ?proteinId . 
        ?protein model:description ?desc . 
        }} GROUP BY ?proteinLabel ?proteinId  """.format(dis=self._stUri)
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        data = []
        for row in results["results"]["bindings"] :
            dict =  {}
            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""
            data.append(dict)
        print("GET PROTEINS")
        print(len(data))
        return data

    def getGenes(self) :
        #query="PREFIX model: <http://purl.amypdb.org/model/> \
        # PREFIX owl: <http://www.w3.org/2002/07/owl#> \
        # PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        # PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        # SELECT ?geneLabel ?geneId ?geneDesc ?score \
        # (GROUP_CONCAT(DISTINCT ?disInfo; SEPARATOR='|') AS ?diseaseList) \
        # WHERE {{\
        # <{dis}> model:mentionedIn ?gda .\
        # ?gda model:referringTo ?gene . \
        # ?gda model:score ?score . \
        # ?gene rdf:type model:Gene . \
        # ?gene rdfs:label ?geneLabel .\
        # ?gene model:accession ?geneId . \
        # {{\
        # ?gene model:description ?geneDesc . \
        # }} UNION {{ \
        # ?gene model:description ?geneDesc . \
        # ?gene model:mentionedIn ?gda2 . \
        # ?gda2 model:referringTo ?disease . \
        # ?disease rdf:type model:Disease . \
        # ?disease model:accession ?diseaseId . \
        # ?disease rdfs:label ?diseaseLabel . \
        # BIND(concat(?diseaseId, '::', ?diseaseLabel) AS ?disInfo) \
        # }} }} GROUP BY ?geneLabel ?geneId ?geneDesc ?score ".format(dis=self._stUri)

        query="""PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX owl: <http://www.w3.org/2002/07/owl#> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        SELECT ?geneLabel ?geneId ?geneDesc ?score 
        WHERE {{
        <{dis}> model:mentionedIn ?gda .
        ?gda rdf:type model:Gda . 
        ?gda model:referringTo ?gene . 
        ?gda model:score ?score . 
        ?gene rdf:type model:Gene . 
        ?gene rdfs:label ?geneLabel .
        ?gene model:accession ?geneId . 
        ?gene model:description ?geneDesc . 
        }} ORDER BY DESC(?score) """.format(dis=self._stUri)
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        data = []
        for row in results["results"]["bindings"] :
            dict =  {}
            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""
            data.append(dict)
        return data

    def getVariants(self) :
        
        query="""PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX owl: <http://www.w3.org/2002/07/owl#> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        SELECT ?variantId ?variantDesc ?score ?link ?proteinLabel ?proteinId 
        WHERE {{
        <{dis}> model:mentionedIn ?vda .
        ?vda rdf:type model:Vda . 
        ?vda model:referringTo ?variant . 
        ?vda model:score ?score . 
        ?variant rdf:type model:VariantAa . 
        ?variant rdfs:seeAlso ?link . 
        ?variant model:accession ?variantId . 
        ?variant model:description ?variantDesc . 
        ?iso model:variant ?variant . 
        ?protein model:isoform ?iso . 
        ?protein model:accession ?proteinId . 
        ?protein rdfs:label ?proteinLabel . 
        }} ORDER BY DESC(?score) """.format(dis=self._stUri)
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        data = []
        for row in results["results"]["bindings"] :
            dict =  {}
            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""
            data.append(dict)
        print("GET VARIANTS")
        print(len(data))
        return data

    def getParent(self) :

        query="""PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX owl: <http://www.w3.org/2002/07/owl#> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        SELECT distinct ?diseaseLabel ?diseaseId 
        WHERE {{
        <{dis}> model:thesRef ?disRef .
        ?disRef skos:broader ?disParentRef .
        ?disease model:thesRef ?disParentRef .
        ?disease rdfs:label ?diseaseLabel .
        ?disease model:accession ?diseaseId .
        }}""".format(dis=self._stUri)
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        data = []
        for row in results["results"]["bindings"] :
            dict =  {}
            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""
            data.append(dict)
        return data
        
    def getChildren(self) :

        query="""PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX owl: <http://www.w3.org/2002/07/owl#> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        SELECT distinct ?diseaseLabel ?diseaseId 
        WHERE {{
        <{dis}> model:thesRef ?disRef .
        ?disChildRef skos:broader ?disRef .
        ?disease model:thesRef ?disChildRef .
        ?disease rdfs:label ?diseaseLabel .
        ?disease model:accession ?diseaseId .
        }}""".format(dis=self._stUri)
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        data = []
        for row in results["results"]["bindings"] :
            dict =  {}
            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""
            data.append(dict)
        return data
        
    def getAssociated(self) :

        query="""PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX owl: <http://www.w3.org/2002/07/owl#> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        SELECT distinct ?diseaseLabel ?diseaseId 
        WHERE {{
        <{dis}> model:thesRef ?disRef .
        ?disRef skos:broader ?disParentRef .
        ?disRef2 skos:broader ?disParentRef .
        FILTER (?disRef != ?disRef2)
        ?disease model:thesRef ?disRef2 .
        ?disease rdfs:label ?diseaseLabel .
        ?disease model:accession ?diseaseId .
        }}""".format(dis=self._stUri)
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        data = []
        for row in results["results"]["bindings"] :
            dict =  {}
            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""
            data.append(dict)
        return data    
        
    


    def getDiseaseFromGenes(self,genes) :
        print("getDiseaseFromGenes")
        dataAll = {} #what we will return in the end
        data = [] # to store gene-disease-relation
        node_colors = {} # to store color info
        legend = []
        null = None


        sparql = SPARQLWrapper(self._queryEndpoint)
        query="""PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX owl: <http://www.w3.org/2002/07/owl#> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        SELECT distinct ?geneLabel ?diseaseLabel 
        WHERE {{ 
        VALUES ?gene {{ {genes} }}
        ?gene model:mentionedIn ?gda . 
        ?gda model:score ?score . 
        ?gda model:accession ?acc .
        ?gene rdfs:label ?geneLabel . 
        ?gda model:referringTo ?disease . 
        ?disease rdf:type model:Disease . 
        ?disease rdfs:label ?diseaseLabel .
        ?disease model:accession ?diseaseId .
        FILTER(regex(?acc,?diseaseId))
        }} """.format(genes=genes)

        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        #print(r.status_code)
        gcolor="hsla(157, 34%, 40%)"
        dcolor = "hsl(211,81%,28%)"
        legend = [{"text": "Genes","iconFill": gcolor}, {"text": "Diseases and phenotypes","iconFill": dcolor}]

        for row in results["results"]["bindings"] : 
            gene = row["geneLabel"]["value"]
            disease = row["diseaseLabel"]["value"]

            dictGD = {}
            dictGD["from"] = gene
            dictGD["to"] = disease
            dictGD["weight"] = 1
            if dictGD not in data : 
                data.append(dictGD)

            if gene not in node_colors :                 
                node_colors[gene] = gcolor

            if disease not in node_colors :
                node_colors[disease] = dcolor



   
        dataAll["data"]=data
        dataAll["node_colors"]=node_colors
        dataAll["legend"] = legend

        return dataAll


    def getPhenoFromGenes(self,genes) :
        print("getPhenoLabel")
        query="""
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX owl: <http://www.w3.org/2002/07/owl#> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        SELECT ?phenoLabel 
        WHERE {{ 
        VALUES ?gene {{ {genes} }} 
        ?gene model:mentionedIn ?gda .
        ?gda model:referringTo ?disease . 
        ?gda model:accession ?acc .
        ?disease rdf:type model:Disease . 
        ?disease model:accession ?diseaseId .
        FILTER(regex(?acc,?diseaseId))
        {{ ?disease model:phenoRef ?phenoThes .
         ?phenoThes skos:prefLabel ?phenoLabel . }}
         UNION {{
         ?disease model:phenoRef ?phenoThes1 .
         ?phenoThes skos:broader ?phenoThes1 .
         ?phenoThes skos:prefLabel ?phenoLabel .
         }}
        
        }} """.format(genes=genes)
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        data = []
        for row in results["results"]["bindings"] :
            data.append(row["phenoLabel"]["value"])
        return data


    def getMainInfo(self) :
        query="PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX owl: <http://www.w3.org/2002/07/owl#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        SELECT ?label \
        (GROUP_CONCAT(DISTINCT ?desc; SEPARATOR=' ') AS ?description) \
        (GROUP_CONCAT(DISTINCT ?qualiLabel; SEPARATOR='|') AS ?qualiList) \
        (GROUP_CONCAT(DISTINCT ?altLabel; SEPARATOR=', ') AS ?altList) \
        (GROUP_CONCAT(DISTINCT ?phenoLabel; SEPARATOR='|') AS ?phenoList) \
        WHERE {{\
        <{dis}> rdfs:label ?label . \
        <{dis}> model:description ?desc .\
        OPTIONAL {{<{dis}> model:phenoRef ?phenRef . ?phenRef skos:prefLabel ?phenoLabel }}\
        OPTIONAL {{<{dis}> model:thesRef ?ref . ?ref skos:altLabel ?altLabel }}\
        OPTIONAL {{<{dis}> model:tag ?tag . ?tag skos:prefLabel ?qualiLabel }}\
        }} GROUP BY ?label ".format(dis=self._stUri)
        sparql = SPARQLWrapper(self._queryEndpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        dict =  {}
        for row in results["results"]["bindings"] :

            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""

        return dict


    def _getPrefLabel(self,mondoUri) : 
        sparql = SPARQLWrapper(MONDOQUERY)
        queryMondo = "PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> \
        SELECT distinct ?label WHERE {{ <{s}> rdfs:label ?label  }}".format(s=str(mondoUri))
        sparql.setQuery(queryMondo)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        label = ""
        for row in results["results"]["bindings"] : 
            label = row["label"]["value"]
        return label

    def _getDescription(self,mondoUri) : 
        sparql = SPARQLWrapper(MONDOQUERY)
        queryMondo = "PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
        SELECT distinct ?desc WHERE {{ <{s}> <http://purl.obolibrary.org/obo/IAO_0000115> ?desc  }}".format(s=str(mondoUri))
        sparql.setQuery(queryMondo)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        desc = ""
        for row in results["results"]["bindings"] : 
            desc = row["desc"]["value"]
        return desc

    def _getUMLS(self,mondoUri) : 
        sparql = SPARQLWrapper(MONDOQUERY)
        queryMondo = "PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
        SELECT distinct ?umls WHERE {{  <{s}> skos:exactMatch ?umls . FILTER regex(str(?umls),'umls','i')  }}".format(s=str(mondoUri))
        sparql.setQuery(queryMondo)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        umls = []
        for row in results["results"]["bindings"] : 
            umls.append(row["umls"]["value"])
            #print("UMLS")
            #print(row["umls"]["value"])
        return umls

    def _getDisOMIM(self,mondoUri) : 
        sparql = SPARQLWrapper(MONDOQUERY)
        queryMondo = "PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
        SELECT distinct ?omim WHERE {{  <{s}> skos:exactMatch ?omim . FILTER regex(str(?omim),'omim','i')  }}".format(s=str(mondoUri))
        sparql.setQuery(queryMondo)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        omim = []
        for row in results["results"]["bindings"] : 
            omim.append(row["omim"]["value"])
            #print("UMLS")
            #print(row["umls"]["value"])
        return omim
        
    def _getDisOrpha(self,mondoUri) : 
        sparql = SPARQLWrapper(MONDOQUERY)
        queryOrpha = "PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
        SELECT distinct ?o WHERE {{  <{s}> skos:exactMatch ?o . FILTER regex(str(?o),'Orphanet','i')  }}".format(s=str(mondoUri))
        sparql.setQuery(queryOrpha)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        orpha = []
        for row in results["results"]["bindings"] : 
            orpha.append(row["o"]["value"])
            #print("UMLS")
            #print(row["umls"]["value"])
        return orpha

    def _getCloseUMLS(self,mondoUri) : 
        sparql = SPARQLWrapper(MONDOQUERY)
        queryMondo = "PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
        SELECT distinct ?umls WHERE {{ <{s}> skos:closeMatch ?umls . FILTER regex(str(?umls),'umls','i')  }}".format(s=str(mondoUri))
        sparql.setQuery(queryMondo)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        umls = []
        for row in results["results"]["bindings"] : 
            umls.append(row["umls"]["value"])
        return umls

    def _getPhen_old(self,elt) : 
        #print("PHENO FOR : " + elt)
        omimId = elt.split("/")[-1]
        query = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT distinct ?phenotypeName WHERE {{ <{om}> <http://purl.bioontology.org/ontology/OMIM/has_manifestation> ?phen . \
        ?phen skos:prefLabel ?phenotypeName . }}".format(om = "http://purl.bioontology.org/ontology/OMIM/"+omimId)
        #print("QUERY : " + query)
        sparql = SPARQLWrapper(OMIMQUERY)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        #print(results)
        return results

    def _getPhen(self,elt,onto) : 
        omimId = elt.split("/")[-1]
        query = """
        SELECT distinct ?pheno ?phenoLabel
        WHERE {{ ?pheno <http://www.geneontology.org/formats/oboInOwl#hasDbXref> "{om}" . 
        ?pheno rdfs:label ?phenoLabel . FILTER NOT EXISTS {{ ?pheno rdfs:label ?phenoLabel .  FILTER(regex(?phenoLabel,"disease","i")) }}  }}""".format(om = onto+":"+omimId)
        #print("QUERY : " + query)
        sparql = SPARQLWrapper(HPOQUERY)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        return results
        
    def addPhenoToDisease(self,mondoUri) : 
        
        tempG = Graph()
        omim=self._getDisOMIM(mondoUri)       
        
        for elt in omim :
            #print("PHEN " + elt)
            
            resultPhen = self._getPhen(elt,"OMIM")   
            if resultPhen != None : 
                for row in resultPhen["results"]["bindings"]:
                    phenoUri = row["pheno"]["value"]
                    phenoLabel = row["phenoLabel"]["value"]
                    #exple : obo:HP_0001251
                    phenoId = phenoUri.split("/")[-1]
                    pt = ThesEntry(phenoId,"HP")
                    if not pt.exists() :
                        pt.createThesPheno(phenoUri,phenoLabel)
                    tempG.add([self._uri,AMMODEL.phenoRef,pt.uri])
                    
        orpha=self._getDisOrpha(mondoUri)
        for elt in orpha :
            resultPhen = self._getPhen(elt,"Orphanet")   
            if resultPhen != None : 
                for row in resultPhen["results"]["bindings"]:
                    phenoUri = row["pheno"]["value"]
                    phenoLabel = row["phenoLabel"]["value"]
                    #exple : obo:HP_0001251
                    phenoId = phenoUri.split("/")[-1]
                    pt = ThesEntry(phenoId,"HP")
                    if not pt.exists() :
                        pt.createThesPheno(phenoUri,phenoLabel)
                    tempG.add([self._uri,AMMODEL.phenoRef,pt.uri])        
                   
        self.addEntityGraphToStore(tempG) 

    def create(self) :

        mondoBaseUri = "http://purl.obolibrary.org/obo/"
        mondoUri = URIRef(mondoBaseUri+self._id)
        pref = self._getPrefLabel(mondoUri)
        print("dis. "+ self._id)     
        tempG = Graph(identifier=self._uri)
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)
        tempG.add([self._uri,RDF.type, self._type])
        tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])
        tempG.add([self._uri,RDFS.label,Literal(pref)])        
        dt=ThesEntry(self._id,"MONDO")
        if not dt.exists() :                  
            dt.createThesDisease(mondoUri,pref)
        tempG.add([self._uri,AMMODEL.thesRef,dt.uri])

        descr = self._getDescription(mondoUri)
        tempG.add([self._uri,AMMODEL.description,Literal(descr)])
        #phenos will be added in the pipeline
        #omim=self._getDisOMIM(mondoUri)        
        #for elt in omim :
            #print("PHEN " + elt)
            #resultPhen = self._getPhen(elt,"OMIM")   
            #if resultPhen != None : 
                #for row in resultPhen["results"]["bindings"]:
                    #phenoUri = row["pheno"]["value"]
                    #phenoLabel = row["phenoLabel"]["value"]
                    #exple : obo:HP_0001251
                    #phenoId = phenoUri.split("/")[-1]
                    #pt = ThesEntry(phenoId,"HP")
                    #if not pt.exists() :
                        #pt.createThesPheno(phenoUri,phenoLabel)
                    #tempG.add([self._uri,AMMODEL.phenoRef,pt.uri])


                      
        self.addEntityGraphToStore(tempG) 



class Vda(KbEntry): 

    def __init__(self,identifier,vdaScore,varUri,diseaseUri):
        super().__init__(identifier)
        self._type = AMMODEL.Vda
        self._score = vdaScore
        self._varUri = varUri
        self._disUri = diseaseUri

    def get_type(self):
        return self._type

    type = property(get_type,doc="Entry Type")

    def get_score(self):
        return self._score

    score = property(get_score,doc="Gene disease association score")

    def get_varUri(self):
        return self._varUri

    varUri = property(get_varUri,doc="Gene Uri")

    def get_disUri(self):
        return self._disUri

    disUri = property(get_disUri,doc="Disease Uri")


    def create(self,disVarUri,disDisUri) :
        #print("VDA "+ self._id)
        tempG = Graph(identifier=self._uri)
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)
        tempG.add([self._uri,RDF.type, self._type])
        tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])

        tempG.add([self._uri,AMMODEL.score, Literal(self._score)])
        tempG.add([self._uri,AMMODEL.referringTo,self._varUri])
        tempG.add([self._uri,AMMODEL.referringTo,self._disUri])
        tempG.add([self._varUri,AMMODEL.mentionedIn,self._uri])
        tempG.add([self._disUri,AMMODEL.mentionedIn,self._uri])
        tempG.add([self._uri,AMMODEL.source,Literal(Disgenet().name)])

        #?gda sio:SIO_000253 ?source . 
        queryPubmed= "SELECT  DISTINCT ?pubmed FROM <{g}> WHERE {{ ?vda a sio:SIO_000897 . ?vda sio:SIO_000628 <{disvUri}>, <{disdUri}> .   ?vda sio:SIO_000772 ?pubmed .    }}".format(g="http://rdf.disgenet.org",disvUri=str(disVarUri),disdUri=str(disDisUri))


        requestURL = DisgenetEndpoint().endpoint
        myparam = { 'query': queryPubmed }
        try : 
            r = requests.get(requestURL, params=myparam,headers={ "Content-type":"application/x-www-form-urlencoded","Accept" : "application/json"})

        except : 
            print("unable to retrieve publi list from disgenet")

        else : 
            try : 
                result = r.json()
            except : 
                print("unable to decode JSON for publi list from disgenet")
            else : 

                for row in result["results"]["bindings"] : 
                    tempG.add([self._uri,AMMODEL.publication,URIRef(row['pubmed']['value'])])

                self.addEntityGraphToStore(tempG)  



class Proda(KbEntry): 

    def __init__(self,identifier,protUri,diseaseUri,comm):
        super().__init__(identifier)
        self._type = AMMODEL.Proda
        self._proUri = protUri   #AMKB URI uniprot
        self._disUri = diseaseUri #AMKB URI Mondo
        self._comment = comm

    def get_type(self):
        return self._type

    type = property(get_type,doc="Entry Type")


    def get_proUri(self):
        return self._proUri

    proUri = property(get_proUri,doc="Protein Uri")

    def get_disUri(self):
        return self._disUri

    disUri = property(get_disUri,doc="Disease Uri")

    def get_comment(self):
        return self._comment
    comment = property(get_comment,doc="Comment about the entry")


    def create(self,protid,disUniUri) :
        print("Proda "+ self._id)
        tempG = Graph(identifier=self._uri)
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)
        tempG.add([self._uri,RDF.type, self._type])
        tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])
        tempG.add([self._uri,AMMODEL.disComment, Literal(self._comment)])
        tempG.add([self._uri,AMMODEL.referringTo,self._proUri])
        tempG.add([self._uri,AMMODEL.referringTo,self._disUri])
        tempG.add([self._proUri,AMMODEL.mentionedIn,self._uri])
        tempG.add([self._disUri,AMMODEL.mentionedIn,self._uri])
        sparql = SPARQLWrapper(UNIQUERY)
        queryEco = "PREFIX unicore:<http://purl.uniprot.org/core/> \
                PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
                PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> \
                PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
                SELECT distinct ?evidence \
                WHERE {{ \
                ?reif a rdf:Statement ; \
                rdf:subject     ?subj; \
                rdf:predicate   unicore:disease ; \
                rdf:object      <{dis}>  ; \
                unicore:attribution  ?attribution . \
                ?attribution unicore:evidence ?evidence . \
                FILTER regex(str(?subj), '{pid}') \
                 }}".format(pid = protid,dis=disUniUri)

        sparql.setQuery(queryEco)
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        for row in results["results"]["bindings"] : 
            supEvi = URIRef(self._stUri+"#SupportingEvidence_"+Uniprot().name+"_"+row['evidence']['value'].split("/")[-1])
            tempG.add([self._uri,AMMODEL.supportingEvidence,supEvi])

            sparql = SPARQLWrapper(UNIQUERY)
            queryPub = "PREFIX unicore:<http://purl.uniprot.org/core/> \
                PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
                PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> \
                PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
                SELECT distinct ?pubmed \
                WHERE {{ \
                ?reif a rdf:Statement ; \
                rdf:subject     ?subj; \
                rdf:predicate   unicore:disease ; \
                rdf:object      <{dis}>  ; \
                unicore:attribution  ?attribution . \
                ?attribution unicore:source ?citation . \
                ?citation skos:exactMatch ?pubmed . \
                ?attribution unicore:evidence <{evi}> . \
                FILTER regex(str(?subj), '{pid}') \
                }}".format(pid = protid,dis=disUniUri,evi = row['evidence']['value'])

            sparql.setQuery(queryPub)
            sparql.setReturnFormat(JSON)
            resultsEco = sparql.query().convert()

            for res in resultsEco["results"]["bindings"] : 
                tempG.add([supEvi,RDF.type,URIRef(STR_AMMODEL+"SupportingEvidence")])
                tempG.add([supEvi,AMMODEL.source,Literal(Uniprot().name)])
                tempG.add([supEvi,AMMODEL.evidenceType,URIRef(row['evidence']['value'])])
                tempG.add([supEvi,AMMODEL.publication,URIRef(res['pubmed']['value'])])

        self.addEntityGraphToStore(tempG)  






class Gda(KbEntry): 

    def __init__(self,identifier,gdaScore,geneUri,diseaseUri):
        super().__init__(identifier)
        self._type = AMMODEL.Gda
        self._score = gdaScore
        self._genUri = geneUri   #AMKB URI Ensembl
        self._disUri = diseaseUri #AMKB URI Mondo

    def get_type(self):
        return self._type

    type = property(get_type,doc="Entry Type")

    def get_score(self):
        return self._score

    score = property(get_score,doc="Gene disease association score")

    def get_genUri(self):
        return self._genUri

    genUri = property(get_genUri,doc="Gene Uri")

    def get_disUri(self):
        return self._disUri

    disUri = property(get_disUri,doc="Disease Uri")


    def create(self,disGenUri,disDisUri) :
        print("GDA "+ self._id)
        tempG = Graph(identifier=self._uri)
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)
        tempG.add([self._uri,RDF.type, self._type])
        tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])
        tempG.add([self._uri,AMMODEL.score, Literal(self._score)])
        tempG.add([self._uri,AMMODEL.referringTo,self._genUri])
        tempG.add([self._uri,AMMODEL.referringTo,self._disUri])
        tempG.add([self._genUri,AMMODEL.mentionedIn,self._uri])
        tempG.add([self._disUri,AMMODEL.mentionedIn,self._uri])

        #?gda sio:SIO_000253 ?source . 
        queryPubmed= "SELECT  DISTINCT ?pubmed FROM <{g}> WHERE {{ ?gda sio:SIO_000628 <{disgUri}>, <{disdUri}> .  ?gda sio:SIO_000772 ?pubmed . }}".format(g="http://rdf.disgenet.org",disgUri=str(disGenUri),disdUri=str(disDisUri))
        
        requestURL = DisgenetEndpoint().endpoint
        myparam = { 'query': queryPubmed }
        try : 
            r = requests.get(requestURL, params=myparam,headers={ "Content-type":"application/x-www-form-urlencoded","Accept" : "application/json"})

        except : 
            print("unable to retrieve publi list from Disgenet")
        else : 
            supEvi=URIRef(self._stUri+"SupportingEvidence_"+Disgenet().name+"_ECO_0000311")
            tempG.add([self._uri,AMMODEL.supportingEvidence,supEvi])
            tempG.add([supEvi,RDF.type,URIRef(STR_AMMODEL+"SupportingEvidence")])
            tempG.add([supEvi,AMMODEL.source,Literal(Disgenet().name)])
            tempG.add([supEvi,AMMODEL.evidenceType,URIRef("http://purl.obolibrary.org/obo/ECO_0000311")])
            if r.status_code == 200 or r.status_code == 204 :
                result = r.json()
                for row in result["results"]["bindings"] : 
                    tempG.add([supEvi,AMMODEL.publication,URIRef(row['pubmed']['value'])])

                self.addEntityGraphToStore(tempG)  


class Transcript(KbEntry): 

    def __init__(self,identifier):
        super().__init__(identifier)
        self._type = AMMODEL.Transcript
        self._prefLabel = ""

    def get_type(self):
        return self._type

    type = property(get_type,doc="Entry Type")

    def get_prefLabel(self):
        return self._prefLabel

    def set_prefLabel(self,value):
        self._prefLabel= value
 
    prefLabel= property(get_prefLabel,set_prefLabel,doc="Entry Type")
    
    """
    def create(self,uniPG) :
        print("creating transcr. "+ self._id)
        tempG = Graph(identifier=self._uri)
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)
        tempG.add([self._uri,RDF.type, self._type])
        tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])

        stEnsGuri = "http://rdf.ebi.ac.uk/resource/ensembl.transcript/"+self._id
        ensGuri=URIRef(stEnsGuri)

        dtr=ThesEntry(STR_ENSNAME+"_"+self._id,STR_ENSNAME)
        if not dtr.exists() :                  
            dtr.createThesPrefOnly(self._prefLabel)

        tempG.add([self._uri,AMMODEL.thesRef,dtr.uri])
        tempG.add([self._uri,RDFS.label,Literal(self._prefLabel)])



        if (ensGuri,UNICORE.translatedTo,None) in uniPG : 
            #a MODIFIER, use http://www.ensembl.org/biomart/martview/10104f87556083c44c41af16a95d3bba
            isoEns = uniPG.value(ensGuri,UNICORE.translatedTo,None)
            isoEnsId = str(isoEns).split("/")[-1]
            if (ensGuri,RDFS.seeAlso,None) in uniPG : 
                isoUni = uniPG.value(ensGuri,RDFS.seeAlso,None)
                isoId = str(isoUni).split("/")[-1]
                iso = Isoform(isoId)
                iso.addInfoTrans(isoUni,isoEns,self._uri)
                tempG.add([self._uri,AMMODEL.translatedTo,iso.uri])
            else : 
                url = 'https://www.uniprot.org/uploadlists/'
                params = {"from" :  "ENSEMBL_PRO_ID","to":"ACC","format": "list","query": isoEnsId}
                try :
                    rmap = requests.get(url,params)
                except : 
                    print("unable to get transc Ens-UniProt info")
                else : 
                    data = rmap.text
                    data = data.strip().split()
                    for elt in data :
                        isoId = elt
                        #It is an unreviewed prot with one isoform
                        if "-" not in elt : 
                            isoId = elt+"-1"
                        iso = Isoform(isoId)
                        isoUni = URIRef(STR_UNIKB+isoId)
                        iso.addInfoTrans(isoUni,isoEns,self._uri)
                        tempG.add([self._uri,AMMODEL.translatedTo,iso.uri])    

        self.addEntityGraphToStore(tempG)  
        """

    
    def create(self,isoUniId,isoEnsId) :
        print(self._id)
        tempG = Graph(identifier=self._uri)
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)
        tempG.add([self._uri,RDF.type, self._type])
        tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])

        stEnsGuri = "http://rdf.ebi.ac.uk/resource/ensembl.transcript/"+self._id
        ensGuri=URIRef(stEnsGuri)
        tempG.add([self._uri,RDFS.seeAlso,ensGuri])

        dtr=ThesEntry(STR_ENSNAME+"_"+self._id,STR_ENSNAME)
        if not dtr.exists() :                  
            dtr.createThesPrefOnly(self._id)
        tempG.add([self._uri,AMMODEL.thesRef,dtr.uri])
        tempG.add([self._uri,RDFS.label,Literal(self._id)])
        
        if isoUniId not in (None,"") : 
            iso = Isoform(isoUniId)
            isoUni = URIRef(STR_UNIKB+isoUniId)
            iso.addInfoTrans(isoUni,isoEnsId,self._uri)
            tempG.add([self._uri,AMMODEL.translatedTo,iso.uri])    

        self.addEntityGraphToStore(tempG)  
             


class ThesEntry(Entry):
    
    def __init__(self,identifier,thesDbName):
        super().__init__(identifier,STR_AMTHES,AMQUERY,AMUPDATE)        
        self._schemeUri = URIRef(STR_AMTHES+thesDbName+"/scheme")
        self._type = SKOS.Concept
        self._prefLabel = ""


    def get_type(self):
        return self._type

    type = property(get_type, doc="Entry Type")

    def get_schemeUri(self):
        return self._schemeUri
    schemeUri = property(get_schemeUri, doc="Entry Type")


    def get_prefLabel(self):
        return self._prefLabel

    def set_prefLabel(self,value):
        self._prefLabel= value
 
    prefLabel= property(get_prefLabel,set_prefLabel,doc="Entry Type")


    def createThesPrefOnly(self,prefLabel) :
        #thesG = Graph('SPARQLUpdateStore',identifier=URIRef(self._stUriBase))
        #thesG.open(self._queryEndpoint,self._updateEndpoint)
        thesG = Graph()
        thesG.add([self._uri,RDF.type,self._type])
        thesG.add([self._uri,SKOS.inScheme,self._schemeUri])
        thesG.add([self._uri,SKOS.topConceptOf,self._schemeUri])
        thesG.add([self._uri,AMMODEL.accession,Literal(self._id)])
        #thesG.add([self._uri,AMMODEL.refType,AMtype])
        thesG.add([self._uri,SKOS.prefLabel,Literal(prefLabel)])          
        self.addEntityGraphToStore(thesG)


    def createThesPrefAndMatch(self,prefLabel,seeAlso,interproId) :
        #thesG = Graph('SPARQLUpdateStore',identifier=URIRef(self._stUriBase))
        #thesG.open(self._queryEndpoint,self._updateEndpoint)
        thesG = Graph()

        thesG.add([self._uri,RDF.type,self._type])
        thesG.add([self._uri,SKOS.inScheme,self._schemeUri])
        thesG.add([self._uri,SKOS.topConceptOf,self._schemeUri])
        thesG.add([self._uri,AMMODEL.accession,Literal(self._id)])
        thesG.add([self._uri,SKOS.notation,Literal(interproId)])
        #thesG.add([self._uri,AMMODEL.refType,AMtype])
        thesG.add([self._uri,SKOS.prefLabel,Literal(prefLabel)])  
        thesG.add([self._uri,RDFS.seeAlso,seeAlso])     
        #thesG.add([self._uri,SKOS.exactMatch,exactMatch])       
        self.addEntityGraphToStore(thesG)

    def addAltLabel(self,litalt) :
        thesG = Graph()
        thesG.add([self._uri,SKOS.altLabel,litalt])
        self.addEntityGraphToStore(thesG)

    def addLiteral(self,propUri,label) : 
        thesG = Graph()
        thesG.add([self._uri,propUri,Literal(label)])
        self.addEntityGraphToStore(thesG)

    def addResource(self,propUri,uriRef) :
        thesG = Graph()
        thesG.add([self._uri,propUri,uriRef])
        self.addEntityGraphToStore(thesG)

    def createThesProt(self,uniPG,uniUri) :
        thesG = Graph()
        
        #print("CreateThesProt : adding statements to thesG")
        
        thesG.add([self._uri,RDF.type,self._type])
        thesG.add([self._uri,SKOS.inScheme,self._schemeUri])
        thesG.add([self._uri,SKOS.topConceptOf,self._schemeUri]) 
        thesG.add([self._uri,AMMODEL.accession,Literal(self._id)])
        #thesG.add([self._uri,AMMODEL.refType,modelType])
          
        #PrefLabel and abbr from recommandedName
        if (uniUri,UNICORE.recommendedName,None) in uniPG : 
            recName = uniPG.value(uniUri,UNICORE.recommendedName,None)
            if (recName, UNICORE.fullName, None) in uniPG : 
                lab = uniPG.value(recName,UNICORE.fullName,None)
                if isinstance(lab, Literal) :
                    thesG.add([self._uri,SKOS.prefLabel,uniPG.value(recName,UNICORE.fullName,None)])
                else : 
                    thesG.add([self._uri,SKOS.prefLabel,Literal("unnamed")])
            else:
                thesG.add([self._uri,SKOS.prefLabel,Literal("unnamed")])
            if (recName, UNICORE.shortName, None) in uniPG :
                if isinstance(uniPG.value(recName,UNICORE.shortName,None),Literal) :
                    thesG.add([self._uri,AMMODEL.abbr,uniPG.value(recName,UNICORE.shortName,None)])
            
        #Mnemonic
        if (uniUri,UNICORE.mnemonic,None) in uniPG : 
            thesG.add([self._uri,SKOS.notation,uniPG.value(uniUri,UNICORE.mnemonic,None)])
        #Alternative labels
        for altName in uniPG.objects(uniUri,UNICORE.alternativeName) :
            if (altName, UNICORE.fullName, None) in uniPG : 
                thesG.add([self._uri,SKOS.altLabel,uniPG.value(altName,UNICORE.fullName,None)])
            if (altName, UNICORE.shortName, None) in uniPG :
                thesG.add([self._uri,SKOS.altLabel,uniPG.value(altName,UNICORE.shortName,None)])  

        self.addEntityGraphToStore(thesG)
      

    """
    def createThesGene(self,ensGG,ensGuri) :
        #thesG = Graph('SPARQLUpdateStore',identifier=URIRef(self._stUriBase))
        #thesG.open(self._queryEndpoint,self._updateEndpoint)
        thesG = Graph()
        thesG.add([self._uri,RDF.type,self._type])
        thesG.add([self._uri,SKOS.inScheme,self._schemeUri])
        thesG.add([self._uri,SKOS.topConceptOf,self._schemeUri]) 
        thesG.add([self._uri,AMMODEL.accession,Literal(self._id)])
        #thesG.add([self._uri,AMMODEL.refType,AMMODEL.Gene])
        thesG.add([self._uri,OWL.sameAs,ensGuri])
        label=Literal("unnamed")
        if (ensGuri,RDFS.label,None) in ensGG : 
            label = ensGG.value(ensGuri,RDFS.label,None)
        thesG.add([self._uri,SKOS.prefLabel,label])    
            ## !!!!!! ebi error : skos:altlabel instead of SKOS.altLabel       
        for altName in ensGG.objects(ensGuri,SKOS.altlabel) :
            thesG.add([self._uri,SKOS.altLabel,altName])
            ## !!!!!! in case they correct the error
        for altName in ensGG.objects(ensGuri,SKOS.altLabel) :
            thesG.add([self._uri,SKOS.altLabel,altName])
        self.addEntityGraphToStore(thesG)
        #thesG.close()
    """
        
    def createThesGeneEns(self,label,ensId,geneSyn) :
        thesG = Graph()
        thesG.add([self._uri,RDF.type,self._type])
        thesG.add([self._uri,SKOS.inScheme,self._schemeUri])
        thesG.add([self._uri,SKOS.topConceptOf,self._schemeUri]) 
        thesG.add([self._uri,AMMODEL.accession,Literal(self._id)])
        thesG.add([self._uri,OWL.sameAs,URIRef(STR_ENS + self._id)])
        thesG.add([self._uri,SKOS.prefLabel,Literal(label)])
        if geneSyn not in (None,"") :
            if " " in geneSyn :
                geneList = geneSyn.split(" ")
                for syn in geneList :
                    thesG.add([self._uri,SKOS.altLabel,Literal(syn)])
            else :
                thesG.add([self._uri,SKOS.altLabel,Literal(geneSyn)])
            
        self.addEntityGraphToStore(thesG)
        #thesG.close()


    def createThesDisease(self,mondoUri,pref) :

        disG = Graph()
        disG.add([self._uri,RDF.type,self._type])
        disG.add([self._uri,SKOS.inScheme,self._schemeUri])
        disG.add([self._uri,AMMODEL.accession,Literal(self._id)])
        #disG.add([self._uri,AMMODEL.refType,AMMODEL.Disease])            
        disG.add([self._uri,OWL.sameAs,mondoUri])
        disG.add([self._uri,SKOS.prefLabel,Literal(pref)])
        self.addEntityGraphToStore(disG)


    def createThesGo(self,goUri,pref,altList) :

        thesG = Graph()
        thesG.add([self._uri,RDF.type,self._type])
        thesG.add([self._uri,SKOS.inScheme,self._schemeUri])
        thesG.add([self._uri,AMMODEL.accession,Literal(self._id)])           
        thesG.add([self._uri,OWL.sameAs,URIRef(goUri)])
        thesG.add([self._uri,SKOS.prefLabel,Literal(pref)])
        if len(altList) > 0 : 
            altList = altList.split("---")
            for alt in altList :
                thesG.add([self._uri,SKOS.altLabel,Literal(alt)])  

    
        self.addEntityGraphToStore(thesG)                

    def createThesPheno(self,phenoUri,pref) :

        thesG = Graph()
        thesG.add([self._uri,RDF.type,self._type])
        thesG.add([self._uri,SKOS.inScheme,self._schemeUri])
        thesG.add([self._uri,AMMODEL.accession,Literal(self._id)])           
        thesG.add([self._uri,OWL.sameAs,URIRef(phenoUri)])
        thesG.add([self._uri,SKOS.prefLabel,Literal(pref)])
     
        self.addEntityGraphToStore(thesG)                

 
 
    def createThesOrg(self) :

        #stOrg = "http://identifiers.org/taxonomy/"+self._id.replace("NCBI_","")
        #print("adding org to thesaurus")
        
        #orgUri = URIRef(stOrg)
        orgUri = URIRef("http://purl.uniprot.org/taxonomy/"+self._id.replace(Uniprot().name+"_taxonomy_",""))  
        uniOrg = "https://www.uniprot.org/taxonomy/"+self._id.replace(Uniprot().name+"_taxonomy_","")+".rdf" 


        #queryAll="DESCRIBE <{uri}> ".format(uri=stOrg)
        #result = EbiEndpoint().execQuery(queryAll,N3)
        thesG = Graph()
        thesG.add([self._uri,RDF.type,self._type])
        thesG.add([self._uri,SKOS.inScheme,self._schemeUri])
        thesG.add([self._uri,SKOS.topConceptOf,self._schemeUri]) 
        thesG.add([self._uri,AMMODEL.accession,Literal(self._id)])
        thesG.add([self._uri,AMMODEL.refType,AMMODEL.Organism])            
        thesG.add([self._uri,OWL.sameAs,orgUri])
            ##get from ebi
        taxLabel=Literal("Unnamed "  + self._id)
        taxG = Graph()
        try : 
                      
            result=requests.get(uniOrg)
            taxG.parse(data=result.content.decode(), format="xml")
            if (orgUri,UNICORE.scientificName,None) in taxG : 
                #taxLabel = taxG.value(orgUri,SKOS.prefLabel,None)
                taxLabel = taxG.value(orgUri,UNICORE.scientificName,None)
                thesG.add([self._uri,SKOS.prefLabel,taxLabel])   
            for obj in taxG.objects(orgUri,UNICORE.otherName) :
                thesG.add([self._uri,SKOS.altLabel,obj])  
            if (orgUri,UNICORE.mnemonic,None) in taxG : 
                mnemonic = taxG.value(orgUri,UNICORE.mnemonic,None)
                thesG.add([self._uri,SKOS.notation,mnemonic])
        except : 
            print("there was a problem with " + uniOrg)
        #print(taxG.serialize(format="n3"))
        finally : 
            self.addEntityGraphToStore(thesG)

       
            #if (orgUri,SKOS.prefLabel,None) in taxG :
       
        


    def createThesMacro(self,pref,altList) :
        thesG = Graph()

        thesG.add([self._uri,RDF.type,self._type])
        thesG.add([self._uri,SKOS.inScheme,self._schemeUri])
        thesG.add([self._uri,SKOS.topConceptOf,self._schemeUri]) 
        thesG.add([self._uri,AMMODEL.accession,Literal(self._id)])
        thesG.add([self._uri,AMMODEL.refType,AMMODEL.MaromolComplex])
        thesG.add([self._uri,SKOS.prefLabel,Literal(pref)])   
        for row in altList :
            thesG.add([self._uri,SKOS.altLabel,Literal(row)])  

        self.addEntityGraphToStore(thesG)


    def createThesCath(self,label,cathId) :

        thesG = Graph()

        thesG.add([self._uri,RDF.type,self._type])
        thesG.add([self._uri,SKOS.inScheme,self._schemeUri])
        thesG.add([self._uri,AMMODEL.accession,Literal(self._id)])
        thesG.add([self._uri,AMMODEL.refType,AMMODEL.ProteinFamily])            
        thesG.add([self._uri,SKOS.prefLabel,Literal(label)]) 
        lastIndex = cathId.rfind(".")
        broadId = cathId[:lastIndex]
        dtr=ThesEntry("CATH_"+broadId,"CATH")
        thesG.add([self._uri,SKOS.broader,dtr.uri])

        self.addEntityGraphToStore(thesG)




class Annotation(Entry) :

    def __init__(self,identifier,qualifier):
        super().__init__(identifier,STR_AMKB,AMQUERY,AMUPDATE)
        self._type=AMMODEL.Annotation
        self._targetUri = None
        self._indicatorUri = None
        self._qualifierUri = qualifier 

    def get_type(self):
        return self._type
    type = property(get_type, doc="Clue Type")

    def get_targetUri(self):
        return self._targetUri
    targetUri= property(get_targetUri, doc="target uri (referring to :) ")

    def get_indicatorUri(self):
        return self._indicatorUri
    indicatorUri = property(get_indicatorUri, doc="indic uri (base on :) ")

    def get_qualifierUri(self):
        return self._qualifierUri
    qualifierUri = property(get_qualifierUri, doc="Qualifier (annotation about ...) ")


    def addSupportingEvi(self,source,ecoId="ECO_0000311",publiList=None) :
        tempG = Graph()

        supEvi=URIRef(self._stUri+"SupportingEvidence_"+source+"_"+ecoId)
        tempG.add([self._uri,AMMODEL.supportingEvidence,supEvi])
        tempG.add([supEvi,RDF.type,URIRef(STR_AMMODEL+"SupportingEvidence")])
        tempG.add([supEvi,AMMODEL.source,Literal(source)])
        tempG.add([supEvi,AMMODEL.evidenceType,URIRef("http://purl.obolibrary.org/obo/"+ecoId)])
        if publiList != None : 
            for publi in publiList : 
                if publi.startswith("www") : 
                    publi = "http://"+publi
                #print(publi)
                tempG.add([supEvi,AMMODEL.publication,URIRef(publi)]) 

        self.addEntityGraphToStore(tempG)  



    def addQualifier(self,qualifierUri) :
        tempG = Graph()
        tempG.add([self._uri,AMMODEL.qualifier,qualifierUri])
        self.addEntityGraphToStore(tempG)  

    def create(self,ecoId,targetRef,source,description,indicatorRef=None):
        tempG = Graph()
        self._targetUri = str(targetRef)
        if indicatorRef != None : 
            self._indicatorUri = str(indicatorRef)
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)
        tempG.add([self._uri,RDF.type, self._type])
        tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])
        tempG.add([self._uri,AMMODEL.evidenceType, URIRef(STR_OBO+ecoId)])       
        tempG.add([self._uri,AMMODEL.referringTo,targetRef])
        tempG.add([targetRef,AMMODEL.mentionedIn,self._uri])
        tempG.add([self._uri,AMMODEL.qualifier,self._qualifierUri])
        tempG.add([self._uri,AMMODEL.source,Literal(source)])
        tempG.add([self._uri,AMMODEL.description,Literal(description)])
        if indicatorRef != None :
            tempG.add([self._uri,AMMODEL.basedOn,indicatorRef])
      

        self.addEntityGraphToStore(tempG)


if __name__ == '__main__':
    """
    p=Protein("P04925")
    p.getIsoforms()
    for item in p.getIsoforms() : 
        print(item)
        print(item.isCanonical())
    """

    """
    prot_file = "data/init_uniprot_id.txt"
    with open(prot_file) as f:
        id_list= f.read().splitlines()
    dict={}
    for elt in id_list[3960:] :
        p=Protein(elt)
        p.create()
    """

    query = 'PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
    SELECT ?protein ?obo \
    WHERE {{ \
    ?protein <http://purl.uniprot.org/core/classifiedWith> ?obo . \
    }} '

    sparql = SPARQLWrapper(UNIQUERY)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()
    mondoBaseUri = "http://purl.obolibrary.org/obo/"

    for row in results["results"]["bindings"] : 
        tempG = Graph()
        prot = row["protein"]["value"]
        protId = prot.split("/")[-1]
        protein = Protein(protId)
       
        if "obo/GO_" in row["obo"]["value"] :
            goId = row["obo"]["value"].split("/")[-1]

            sparql = SPARQLWrapper(GOQUERY)
            query = "PREFIX oboInOwl:<http://www.geneontology.org/formats/oboInOwl#> \
                PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> \
                SELECT distinct ?sub ?type ?label (group_concat(?altLabel;separator='---') as ?alt) \
                WHERE {{ VALUES ?sub {{ <{goterms}> }} \
                ?sub oboInOwl:hasOBONamespace ?type . ?sub rdfs:label ?label . OPTIONAL {{ ?sub oboInOwl:hasExactSynonym ?altLabel .}}  }} \
                GROUP BY ?type ?sub ?label ".format(goterms=row["obo"]["value"])
                #print(query)
                #print(GOQUERY)
            sparql.setQuery(query)
            sparql.setReturnFormat(JSON)
            results=sparql.query().convert()

            for row in results["results"]["bindings"] : 
                if "sub" in row :        
                    print("row type value" + row["type"]["value"] )
                    #print(row["sub"]["value"])
                    thes=ThesEntry(goId,Go().name)
                    if not thes.exists() : 
                        thes.createThesGo(row["sub"]["value"],row["label"]["value"],row["alt"]["value"])
                    if row["type"]["value"] == "molecular_function" : 
                        tempG.add([protein.uri,AMMODEL.function,thes.uri])
                    elif row["type"]["value"] == "biological_process" :
                        tempG.add([protein.uri,AMMODEL.bioProcess,thes.uri])
                    elif row["type"]["value"] == "cellular_component" :
                        tempG.add([protein.uri,AMMODEL.cellComponent,thes.uri])

        
            protein.addEntityGraphToStore(tempG) 


                           
      




"""    
    p = Protein("P05067")
    p.create()
   """ 


"""
    findAmyClues(Uniprot)
    checkReactome : part of amyloid fiber complex https://reactome.org/ContentService/#/entities/getComplexSubunitsUsingGET
    Amyloid fibril monomers https://reactome.org/content/detail/R-HSA-977175
    localized amyloid fibril monomers  https://reactome.org/content/detail/R-HSA-977181
    systemic amyloid fibril  monomers  https://reactome.org/content/detail/R-HSA-977105
    amyloid fibril main peptide chain https://reactome.org/content/detail/R-HSA-977144
    https://reactome.org/content/detail/R-HSA-976963
    https://reactome.org/content/detail/R-HSA-976760
    https://reactome.org/content/query?q=Amyloid+fibril&species=Homo+sapiens&species=Entries+without+species&types=Protein&cluster=true
    https://reactome.org/content/query?q=Amyloid+fibril&species=Homo+sapiens&species=Entries+without+species&types=Complex&cluster=true
    https://reactome.org/content/query?q=Amyloid+fibril&species=Homo+sapiens&species=Entries+without+species&types=Polymer&cluster=true
    https://reactome.org/content/query?q=Amyloid&types=Complex&cluster=true
    https://reactome.org/content/query?q=Amyloid&types=Protein&cluster=true
    """






 
