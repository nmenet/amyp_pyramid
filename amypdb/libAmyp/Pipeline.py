from amypdb.libAmyp.AmyConf import *
from amypdb.libAmyp.Thesaurus import *
from amypdb.libAmyp.Entry import Protein, Fragment, Annotation
import xml.etree.ElementTree as ET
import json
import requests
from rdflib import URIRef, Literal,Graph
from SPARQLWrapper import SPARQLWrapper,N3,JSON
from BioManager.bioSparqlService import *
from BioManager.bioEntity import *

#MONDO : http://purl.amypdb.org/model/Amyloidosis http://purl.amypdb.org/model/Amyloid_Related_Disease
#REACTOME : Amyloid_Pathway Amyloid_Complex
#GO : Amyloid_Process Amyloid_Function Amyloid_Complex
#PDB : Amyloid_Fibril Amyloid_Structure Amyloid_Like_Structure
#AMYLOAD : Amyloidogenic_Fragment


    # ECO:0000000 evidence
    #     ECO:0000006 experimental evidence
    #     ECO:0000041 similarity evidence
    #     ECO:0000088 biological system reconstruction evidence
    #     ECO:0000177 genomic context evidence
    #     ECO:0000204 author statement
    #     ECO:0000212 combinatorial evidence
    #     ECO:0000311 imported information
    #     ECO:0000352 evidence used in manual assertion
    #     ECO:0000361 inferential evidence
    #     ECO:0000501 evidence used in automatic assertion
    #     ECO:0006055 high throughput evidence
    #     ECO:0007672 computational evidence
    # ECO_0000366 inference automatic annotation automatic assertion
    #ECO:0005561 pairwise sequence alignment evidence used in automatic assertion
    #ECO:0007815 structure determination evidence used in automatic assertion

prot_file="data/init_uniprot_id.txt"
amyload_file = "data/Amyload/AmyLoad_all.xml"
ALBase_file = "data/ALBase/AL_sequences.csv"
isa_file_human = "data/ISA/ISA_human_vir.csv"
isa_file_animal = "data/ISA/ISA_animals_id.txt"
amypro_file = "data/AmyPro/amypro.json"
funct_amy_file = "data/Articles/Functional_prot_id.txt"
site_json_file = "../static/browse_count.json"

def createEntriesFromUniprot():
    #prot_file = "init_scripts/results/unique_ids_ok.txt"
    with open(prot_file) as f:
        id_list= f.read().splitlines()
    #dict={}

    #for i in range(0,9) :
    #    if i*440+440 > len(id_list) : 
    #        dict[i]=id_list[i*440:len(id_list)]
    #    else :
    #        dict[i]=id_list[i*440:i*440+440]
    
    # for elt in id_list[:100] :

    
    for elt in id_list :
        p=Protein(elt)
        p.create()

def createEntriesFromAmyload():
    tree = ET.parse(amyload_file)  
    root = tree.getroot()
    for fragment in root:
        fragId = fragment.find('id').text
        frag = Fragment(fragId)
        if not frag.exists() :
            label = fragment.find('name').text
            sequence = fragment.find('sequence').text
            frag.create(label,sequence,"Amyload")


def createEntriesFromALBase():
    with open(ALBase_file) as f:
        for line in f : 
            info = line.split(";")
            alName = info[2].strip()
            alId = alName.replace("*","_")
            alId = alId.replace("-","_")
            fragId = "ALBase_"+alId
            sequence = info[1].strip()
            frag = Fragment(fragId)
            if not frag.exists() :
                frag.create(alName,sequence,"ALBase")


def addAnnotForAmyload():
    tree = ET.parse(amyload_file)
    root = tree.getroot()
    for fragment in root:
        frag = Fragment(fragment.find('id').text)
        reference = frag.uri
        source="Amyload"
        evidence = "ECO_0000006"
        annotId = fragment.find('id').text+"_"+source+"_"+evidence
        annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloidogenic"))

        if not annot.exists() :
            description= "Fibrilation detected experimentally on a fragment"
            if fragment.find('solution').text != None : 
                description += " | Solution : " + fragment.find('solution').text
            description += " | Method : "
            for method in fragment.iter(tag='method') :
                description += method.find('name').text + ","
            description = description[:-1]
            if fragment.find('protein').find('name').text != None :
                description += " | Protein : " + fragment.find('protein').find('name').text
            annot.create(evidence,reference,source,description)

            pubList=[]
            for ref in fragment.iter(tag='reference') :
                pubList.append(ref.find('url').text) 

            annot.addSupportingEvi(source,"ECO_0000313",pubList)


def addAnnotForALBase():
    with open(ALBase_file) as f:
        for line in f : 
            info = line.split(";")
            alName = info[2].strip()
            alId = alName.replace("*","_")
            alId = alName.replace("-","_")
            fragId = "ALBase_"+alId
            sequence = info[1].strip()
            frag = Fragment(fragId)
            reference = frag.uri
            source="ALBase"
            evidence = "ECO_0000006"
            annotId = fragId+"_"+source+"_"+evidence
            annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloidogenic"))

            if not annot.exists() :
                description= "Fibrilation detected experimentally. Reported to form fibrillar deposits in AL patients."
                annot.create(evidence,reference,source,description)



def queryAndCreateAnnot(query,endpoint,annotType,evidence,description,source):
    sparql = SPARQLWrapper(endpoint)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()
    dict={}
    for row in results["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_"+row["indicatorId"]["value"] + "_" +source+"_"+evidence            
        annot = Annotation(annotId,annotType)
        if not annot.exists() :
            annot.create(evidence,URIRef(row["entity"]["value"]),source,description,URIRef(row["indicator"]["value"]))
            dict[annot] = row["indicator"]["value"]
    return dict


def inferFromFragment():

    sparql = SPARQLWrapper(AMQUERY)

    #for each sequence from polypeptide, check if it contains an amyloidogenic fragment
    #if if does, add a amyloid annot source amypdb, reference inferred, description 

    #PREFIX search: <http://www.openrdf.org/contrib/lucenesail#>
    queryFragSeq = """
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
        SELECT ?indicatorId ?indicator ?amyseq 
        WHERE { ?indicator rdf:type model:Fragment ; model:accession ?indicatorId ; model:primStruct ?primStructF . 
        ?primStructF model:sequence ?amyseq .
        }"""

    queryIsoPoly = """
        PREFIX search: <http://www.openrdf.org/contrib/lucenesail#> 
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
        SELECT ?entity ?entityId ?seq 
        WHERE { VALUES ?type { model:Polypeptide model:Isoform } . ?entity rdf:type ?type ; model:accession ?entityId ; model:primStruct ?primStruct . 
        ?primStruct model:sequence ?seq .
        } """

    sparql.setQuery(queryFragSeq)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    fragList = []
    for row in results["results"]["bindings"]:
        dict = {}
        dict["indicator"] = row["indicator"]["value"]
        dict["indicatorId"] = row["indicatorId"]["value"]
        dict["seq"] = row["amyseq"]["value"]
        fragList.append(dict)

    sparql.setQuery(queryIsoPoly)
    sparql.setReturnFormat(JSON)
    results2 = sparql.query().convert()

    entityList = []
    for row in results2["results"]["bindings"]:
        dict = {}
        dict["entity"] = row["entity"]["value"]
        dict["entityId"] = row["entityId"]["value"]
        dict["seq"] = row["seq"]["value"]
        entityList.append(dict)

    description = "Presence of an amyloidogenic fragment"
    evidence = "ECO_0005561"
    source = STR_AMNAME

    for entityDict in entityList : 
        for fragDict in fragList : 
            if fragDict["seq"] in entityDict["seq"] :
                annotId  = entityDict["entityId"]+"_"+ fragDict["indicatorId"]+"_"+source+"_"+evidence          
                annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"))
                if not annot.exists() :
                   
                    annot.create(evidence,URIRef(entityDict["entity"]),source,description,Literal("Amyloidogenic fragment :" + fragDict["seq"]))
         


def inferFromPDBWithQualifier(descriptorClue,descriptorResult,description,source,complEvi=None) : 
    pdbScheme = STR_AMTHES+PDB().name+"/scheme"

    queryQualif = """
    PREFIX model: <http://purl.amypdb.org/model/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    SELECT ?entity ?entityId ?indicator ?indicatorId
    WHERE {{
    ?entity rdf:type <{uri}> .
    ?entity model:accession ?entityId .
    ?entity model:terStruct ?terStruct.
    ?terStruct model:pdb ?indicator .
    ?pdbRef skos:broadMatch <{desc}> .
    ?pdbRef skos:inScheme <{scheme}> .
    ?pdbRef model:accession ?indicatorId . 
    ?pdbRef owl:sameAs ?indicator . }}""".format(uri=str(AMMODEL.Isoform),scheme=pdbScheme,desc=descriptorClue)

    dict = queryAndCreateAnnot(queryQualif,AMQUERY,descriptorResult,"ECO_0007815",description,source)

    for annot,indic in dict.items() : 
        print("add pub for PDB")
        sparql = SPARQLWrapper(PdbEndpoint().get_endpoint())
        query = "PREFIX dcterms:<http://purl.org/dc/terms/> \
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
        SELECT ?pubmed \
        WHERE {{ <{uri}> dcterms:references ?pubmed }} ".format(uri=indic)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        publiList = []
        for row in results["results"]["bindings"]:
            publiList.append(row["pubmed"]["value"])
        if len(publiList)<1 :
            publiList = None

        annot.addSupportingEvi(PDB().name,"ECO_0000313",publiList)

        if complEvi != None :
            source = complEvi[0]
            publiList = complEvi[1]
            annot.addSupportingEvi(source,"ECO_0000313",publiList)



def inferFromPDB():
    #for each isoform, check if it has a associated pdb structure annotaed with amyloid
    #if if does, add a amyloid annot source amypdb, reference inferred, description add pubref from pdb 
    print("for amyloid Fibril")
    inferFromPDBWithQualifier(STR_AMTHES+STR_AMNAME+"_Amyloid_Fibril",URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"),"This isoform is associated with a PDB structure described as an amyloid fibril",STR_AMNAME)
    print("for amyloid Structure")
    inferFromPDBWithQualifier(STR_AMTHES+STR_AMNAME+"_Amyloid_Structure",URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"), "This isoform is associated with a PDB structure described as an amyloid structure",STR_AMNAME)
    print("for amyloid like Structure")
    inferFromPDBWithQualifier(STR_AMTHES+STR_AMNAME+"_Amyloid_Like_Structure",URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Like"), "This isoform is associated with a PDB structure described as an amyloid-like structure",STR_AMNAME)
  
    print("for label match")
    pdbScheme = STR_AMTHES+PDB().name+"/scheme"
    queryLabel = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    SELECT ?entity ?entityId ?indicator ?indicatorId \
    WHERE {{ ?entity rdf:type <{uri}> ; model:accession ?entityId ; model:thesRef ?thesRef . ?thesRef skos:prefLabel ?label . \
    FILTER( strlen( ?label ) > 6 ) .\
    ?pdbRef skos:broadMatch <{desc}> . \
    ?pdbRef skos:prefLabel ?pdbLabel . FILTER regex(?pdbLabel,?label,'i') . \
    ?pdbRef skos:inScheme <{scheme}> . ?pdbRef model:accession ?indicatorId . \
    ?pdbRef owl:sameAs ?indicator . }} ".format(uri=str(AMMODEL.Polypeptide),scheme=pdbScheme,desc=STR_AMTHES+STR_AMNAME+"_Amyloid_Fibril")
    description = "The name of this polypeptide matches the name of a PDB structure described as an amyloid fibril"
    dict = queryAndCreateAnnot(queryLabel,AMQUERY,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"),"ECO_0000363",description,PDB().name)


def inferFromGo():

    goScheme = STR_AMTHES+Go().name+"/scheme"

    query = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    SELECT ?entity ?entityId ?indicator ?indicatorId \
    WHERE {{ ?entity  model:bioProcess  ?indicRef . \
    ?entity model:accession ?entityId . \
    ?indicRef skos:broadMatch <{desc}> . \
    ?indicRef skos:inScheme <{scheme}> .  \
    ?indicRef owl:sameAs ?indicator . \
    ?indicRef model:accession ?indicatorId . }} ".format(scheme=goScheme,desc=STR_AMTHES+STR_AMNAME+"_Amyloid_Process")

    evidence = "ECO_0000362"
    description = "This protein is involved in a biological process related to amyloidy"

    dict= queryAndCreateAnnot(query,AMQUERY,URIRef(STR_AMTHES+STR_AMNAME+"_Amylome_Interactor"),"ECO_0007815",description,Uniprot().name)

    for annot,indic in dict.items() : 
        sparql = SPARQLWrapper(UNIQUERY)
        queryPub = "PREFIX unicore:<http://purl.uniprot.org/core/> \
                PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
                PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> \
                PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
                SELECT distinct ?pubmed \
                WHERE {{ \
                ?reif a rdf:Statement ; \
                rdf:subject     <{pid}>; \
                rdf:predicate   unicore:annotation ; \
                rdf:object      <{go}>  ; \
                unicore:attribution  ?attribution . \
                ?attribution unicore:source ?citation . \
                ?citation skos:exactMatch ?pubmed . \
                }}".format(pid = annot.targetUri,go=indic)

        sparql.setQuery(queryPub)
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        publiList = []
        for row in results["results"]["bindings"]:
            publiList.append(row["pubmed"]["value"])
        if len(publiList)<1 :
            publiList = None

        annot.addSupportingEvi(Uniprot().name,"ECO_0000311",publiList)

def addHereditary(dict) : 

    for annot,indic in dict.items() : 

        query = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    ASK {{ ?disRef rdf:type skos:Concept .  \
    ?disRef owl:sameAs <{indic}> . \
    ?disRef skos:prefLabel|skos:altLabel ?label FILTER regex(?label,'hereditary|familial','i') \
    }} ".format(indic=indic)
        headers = {'content-type' : 'application/x-www-form-urlencoded'}
        myparam = { 'query': query }
        r=requests.get(AMQUERY,myparam,headers=headers)    
        labelExists = r.json()['boolean']
        if labelExists : 
            annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Hereditary"))


def inferFromMondo():

    #First, we annotate amyloidosis and amyloid-related disease

    MondoScheme = STR_AMTHES+Mondo().name+"/scheme"

    query = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    SELECT ?entity ?entityId ?indicator ?indicatorId \
    WHERE {{ ?entity  rdf:type  <{entityType}> . \
    ?entity model:thesRef ?disRef . \
    ?entity model:accession ?entityId . \
    ?disRef skos:broadMatch <{desc}> . \
    ?disRef skos:inScheme <{scheme}> .  \
    ?disRef model:accession ?indicatorId . \
    ?disRef owl:sameAs ?indicator . \
    }} ".format(scheme=MondoScheme,desc=STR_AMTHES+STR_AMNAME+"_Amyloidosis",entityType = AMMODEL.Disease)

    description = "This disease is classified as an amyloidosis in the MONDO ontology"

    dict1= queryAndCreateAnnot(query,AMQUERY,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloidosis"),"ECO_0000363",description,Mondo().name)

    print("query Amyloidosis ended, now trying to get hereditary type")

    addHereditary(dict1)

    print("now trying to get Amyloid related diseases")

    #Find amyloid related disease
    query2 = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    SELECT ?entity ?entityId ?indicator ?indicatorId \
    WHERE {{ ?entity  rdf:type  <{entityType}> . \
    ?entity model:accession ?entityId . \
    ?entity model:thesRef ?disRef . \
    ?disRef skos:broadMatch <{desc}> . \
    ?disRef skos:inScheme <{scheme}> .  \
    ?disRef model:accession ?indicatorId . \
    ?disRef owl:sameAs ?indicator . \
    }} ".format(scheme=MondoScheme,desc=STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease",entityType = AMMODEL.Disease)

    description2 = "This disease was listed as a disease involving an amyloid protein in a publication"

    dict2= queryAndCreateAnnot(query2,AMQUERY,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease"),"ECO_0007631",description2,STR_AMNAME)

    print("now trying to get hereditary type")


    addHereditary(dict2)

    print("adding article ref")

    publiList=[]
    publiList.append("https://www.ncbi.nlm.nih.gov/pubmed/28498720")

    for annot,indic in dict2.items() : 
        annot.addSupportingEvi(STR_AMNAME,"ECO_0000322",publiList)



    print("now trying to get Amyloid related diseases from phenotype")
    query3 = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    SELECT ?entity ?entityId \
    WHERE {{ ?entity  rdf:type  model:Disease . \
    ?entity model:accession ?entityId . \
    ?entity model:phenoRef ?phref . ?phref skos:prefLabel 'Amyloidosis' . \
    ?entity model:thesRef ?disRef . \
    FILTER NOT EXISTS {{  ?disRef skos:broadMatch <{desc}> . \
    ?disRef skos:inScheme <{scheme}> .  }} \
    }} ".format(scheme=MondoScheme,desc=STR_AMTHES+STR_AMNAME+"_Amyloidosis", pheno="Amyloidosis")

    description3 = "This disease has Amyloidosis listed in its phenotype in DisGenet"
    evidence3 = "ECO_0007631"
    source3 = Disgenet().name

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()
    #dict3={}
    for row in results["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_PhenoAmyloidosis_" +source3+"_"+evidence3           
        annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease"))
        if not annot.exists() :
            annot.create(evidence3,URIRef(row["entity"]["value"]),source3,description3)
            #dict3[annot] = row["indicator"]["value"]


    

def inferFromDiseaseAssociations():

    #First we annotate genes

    query = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    SELECT distinct ?entity ?entityId \
    WHERE {  ?gda rdf:type model:Gda . \
    ?gda model:referringTo ?entity . \
    ?entity rdf:type model:Gene . \
    ?entity model:accession ?entityId . \
    ?gda model:score ?score . FILTER (?score >= 0.3) } "

    description = "This gene is associated to at least one disease with a disgenet score superior or equal to 0.3"
    source = Disgenet().name
    evidence = "ECO_0000363"

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()

    for row in results["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_" +source+"_"+evidence 
        annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Pathogenic")       
        annot = Annotation(annotId,annotType)
        if not annot.exists() :
            annot.create(evidence,URIRef(row["entity"]["value"]),source,description)

    #Second we annotate proteins
    query2 = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    SELECT distinct ?entity ?entityId (COUNT(?pub) AS ?nb) (GROUP_CONCAT(DISTINCT ?pub; SEPARATOR='|') AS ?publiList) \
    WHERE {  ?proda rdf:type model:Proda . \
    ?proda model:referringTo ?entity . \
    ?entity rdf:type model:Protein . \
    ?entity model:accession ?entityId . \
    ?proda model:supportingEvidence ?evi . \
    ?evi model:publication ?pub . \
    } GROUP BY ?entity ?entityId "

    source2 = Uniprot().name
    evidence2 = "ECO_0000363"

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(query2)
    sparql.setReturnFormat(JSON)
    results2=sparql.query().convert()

    for row in results2["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_" +source2+"_"+evidence2
        description2 = "This protein is associated to at least one disease in Uniprot and there are " + row["nb"]["value"] + " publications supporting the different disease-gene associations"
        annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Pathogenic")       
        annot = Annotation(annotId,annotType)
        if not annot.exists() :
            annot.create(evidence2,URIRef(row["entity"]["value"]),source2,description2)
            publi = row["publiList"]["value"]
            publiList=publi.split("|")
            annot.addSupportingEvi(Uniprot().name,"ECO_0000323",publiList)




    #Second we annotate proteins from variant-disease annotations
    query3 = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    SELECT distinct ?entity ?entityId \
    WHERE {  ?vda rdf:type model:Vda . \
    ?vda model:referringTo ?var . \
    ?var rdf:type model:Variant . \
    ?entity model:isoform ?iso . ?iso model:variant ?var . \
    ?entity model:accession ?entityId . \
    ?vda model:score ?score . FILTER (?score >= 0.3) } "

    source3 = Disgenet().name
    evidence3= "ECO_0000363"
    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(query3)
    sparql.setReturnFormat(JSON)
    results3=sparql.query().convert()

    for row in results3["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_" +source3+"_"+evidence3
        description3 = "This protein has a variant which is associated to at least one disease with a disgenet score superior or equal to 0.3"
        annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Pathogenic")       
        annot = Annotation(annotId,annotType)
        if not annot.exists() :
            annot.create(evidence2,URIRef(row["entity"]["value"]),source2,description2)




def inferFromALBase():


    publiList = []
    publiList.append("https://www.ncbi.nlm.nih.gov/pubmed/19291508")
    complEvi=[ALBase().name,publiList]
    inferFromPDBWithQualifier(STR_AMTHES+STR_AMNAME+"_Amyloid_Light_Chain",URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"),"This isoform is associated with a PDB structure described as a amyloid light chain in ALBase",ALBase().name)

    uniprotList = ["P01834","Q6PJG0","P0DOY3","P01594","P01602","P01599","P01611","P01597","P01614","P01615","P01619","P01624","P06312","P01699","P01700","P01703","P01705","P01709","P01706","P01714","P01715","P01717","P01718","P80748","P01721","P01704","P01701","P01593"]

    evidence = "ECO_0000313"
    description = "This protein (or a protein variant) has an amyloidogenic sequence which is reported to form fibrillar deposits in AL patients"
    for protId in uniprotList :
        print("add ALBASE annot")
        p = Protein(protId)
        if p.exists() : 
            annotId  = protId+"_ALBase_"+evidence
            annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"))
            annot.create(evidence,p.uri,"ALBase",description)
            annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Pathogenic"))


def inferFromSignatures():

    #TO ANNOTATE : domains and conserved site => Amyloid
    #TO ANNOTATE : amyloid-like family
    amyloid = STR_AMTHES+STR_AMNAME+"_Amyloid"
    amyloid_like = STR_AMTHES+STR_AMNAME+"_Amyloid_Like"
    Amylome_Interactor =STR_AMTHES+STR_AMNAME+"_Amylome_Interactor"

    descList = [amyloid,amyloid_like,Amylome_Interactor]

    for desc in descList : 

        query = "PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX owl: <http://www.w3.org/2002/07/owl#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        SELECT ?entity ?entityId ?indicator ?indicatorId \
        WHERE {{ {{ ?entity model:signature ?disRef . \
        ?entity model:accession ?entityId . \
        ?disRef rdf:type skos:Concept . \
        ?disRef skos:notation ?indicatorId .  \
        ?disRef skos:broadMatch <{desc}> . \
        ?disRef model:accession ?id . \
        ?disRef rdfs:seeAlso ?indicator . \
        }} UNION {{ \
        ?entity model:signature ?disRef . \
        ?entity model:accession ?entityId . \
        ?disRef rdf:type skos:Concept . \
        ?disRef skos:notation ?indicatorId .  \
        ?indicator skos:notation ?indicatorId . \
        ?indicator skos:broadMatch <{desc}> . \
        }} \
        }} ".format(desc = desc)

        if "Amyloid_Like" in desc : 
            description = "The sequence of this isoform matches an Interpro signature related to an amyloid-like feature."
            evidence = "ECO_0000259"  #matach to interpro signature member

            dict= queryAndCreateAnnot(query,AMQUERY,URIRef(amyloid_like),evidence,description,STR_AMNAME)
            publiList=[]
            publiList.append("https://www.ncbi.nlm.nih.gov/pubmed/30614283")
            for annot,indic in dict.items() : 
                annot.addSupportingEvi("Interpro","ECO_0000322",publiList)

        elif "Amylome_Interactor" in desc :
            description = "The sequence of this isoform matches an Interpro signature related to an amyloid interactor"
            evidence = "ECO_0000259"  #matach to interpro signature member

            dict= queryAndCreateAnnot(query,AMQUERY,URIRef(Amylome_Interactor),evidence,description,STR_AMNAME)
            publiList=[]
            publiList.append("https://www.ncbi.nlm.nih.gov/pubmed/30614283")
            for annot,indic in dict.items() : 
                annot.addSupportingEvi("Interpro","ECO_0000322",publiList)

        else :
            description = "The sequence of this isoform matches an Interpro signature related to an amyloid entity"
            evidence = "ECO_0000259"  #matach to interpro signature member

            dict= queryAndCreateAnnot(query,AMQUERY,URIRef(amyloid),evidence,description,STR_AMNAME)
            publiList=[]
            publiList.append("https://www.ncbi.nlm.nih.gov/pubmed/30614283")
            for annot,indic in dict.items() : 
                annot.addSupportingEvi("Interpro","ECO_0000322",publiList)







def inferFromISA():
    with open(isa_file_human) as f:
        for line in f:
            info=line.split(";")
            uniId = info[0].strip()
            entityUri = STR_AMKB + uniId
            depositType = info[2]
            transmissionType = info[3]
            localisation = info[4]
            comment = info[5]
            fibril = info[6]
            description = "This protein belongs to a family listed as a protein precursor by the International Society of Amyloidosis (ISA). "
            if "variant" in comment : 
                description += "The amyloidy has only been detected on protein variants. "
            description += "The associated amyloid fibril is designated as "+fibril + ". "
            if "," in depositType : 
                description += "Its deposition type can be both systemic or localized. "
            else : 
                if "S" in depositType : description += "Its deposition type is systemic. "
                elif "L" in depositType : description += "Its deposition type is localized. "
            description += "Amyloid deposition can be found in : " + localisation + ". "
            if "," in transmissionType : 
                description += "Inheritance pattern : both acquired and hereditary transmissions are known ."
            else : 
                if "A" in transmissionType : " Inheritance pattern : the amyloid-related diseases for this protein are not hereditary (acquired form) ."
                elif "H" in transmissionType : " Inheritance pattern : the amyloid-related diseases caused by this protein are hereditary ."

            evidence = "ECO_0000322"
            annotId  = uniId+"_" +STR_AMNAME+"_"+evidence
            annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"))
            annot.create(evidence,URIRef(entityUri),STR_AMNAME,description)
            publiList=[]
            publiList.append("https://www.ncbi.nlm.nih.gov/pubmed/30614283")
            annot.addSupportingEvi("ISA","ECO_0000322",publiList)
            annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Pathogenic"))
            if "variant" in comment : 
                annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Variant_Only"))

                
    with open(isa_file_animal) as fa :    
            for line in fa :
                uniId = line.strip()
                description = "This protein belongs to a family listed as a protein precursor by the International Society of Amyloidosis (ISA). "
                evidence = "ECO_0000322"
                annotId  = uniId+"_" +STR_AMNAME+"_"+evidence
                annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"))
                annot.create(evidence,URIRef(entityUri),STR_AMNAME,description)
                publiList=[]
                publiList.append("https://www.ncbi.nlm.nih.gov/pubmed/30614283")
                annot.addSupportingEvi("ISA","ECO_0000322",publiList)


def inferFromAmyPro() : 

    with open(amypro_file) as f:
        data = json.load(f)
        for row in data : 
            uniprotId = row["uniprot_id"]
            patho_funct = row["class_name"]
            variantOnly = row["wild_type_amyloidogenic"]
            techniquesList = row["experimental_techniques"]

            p=Protein(uniprotId)
            if p.exists() : 
                evidence = "ECO_0000313"
                annotId  = uniprotId+"_Amypro_"+evidence
                annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"))
                if "patho" in patho_funct : 
                    patho_funct = "pathogenic amyloid"
                    annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Pathogenic"))
                if "functional" in patho_funct : 
                    annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Functional"))
                description = "This protein is described as a " + patho_funct + " protein in AmyPro. Information about experimental techniques used to support the claim  : "
                for elt in techniquesList : 
                    description += elt + ","
                description = description[:-1]
                annot.create(evidence,p.uri,"Amypro",description)
                if variantOnly == 0 : 
                    annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Variant_Only"))


def inferFromReactome() : 


    query = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    PREFIX unicore: <http://purl.uniprot.org/core/> \
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
    SELECT ?entity \
    WHERE { ?entity rdf:type unicore:Protein . \
    ?entity rdfs:seeAlso <http://identifiers.org/reactome/R-HSA-977225> . \
    }"

    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': query }
    r=requests.get(UNIQUERY,myparam,headers=headers)    
    data = r.json()

    evidence = "ECO_0000313"
    description = "This protein is annotated with the Reactome descriptor : 'Amyloid fiber formation' in Uniprot"

    for row in data["results"]["bindings"] : 
        uniUri = row["entity"]["value"]
        entityId = uniUri.split("/")[-1]
        p = Protein(entityId)
        annotId  = entityId+"_R-HSA-977225_Uniprot_"+evidence
        annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amylome_Interactor"))
        annot.create(evidence,p.uri,"Uniprot",description,URIRef("http://identifiers.org/reactome/R-HSA-977225"))


def inferFromArticles() : 

    #Functional Amyloid 
    #https://www.ncbi.nlm.nih.gov/pubmed/25131597

    with open(funct_amy_file) as f:

        for line in f :
            uniId = line.strip()
            p=Protein(uniId)
            description = "This protein is listed as a functional amyloid in a publication. "
            evidence = "ECO_0000322"
            annotId  = uniId+"_25131597_" +STR_AMNAME+"_"+evidence
            annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"))
            annot.create(evidence,p.uri,STR_AMNAME,description,URIRef("https://www.ncbi.nlm.nih.gov/pubmed/25131597"))
            annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Functional"))

    #RIP1/RIP3
    #https://www.ncbi.nlm.nih.gov/pubmed/22817896
    #idlist = ["P25445","Q13546"]
    #for elt in idlist :

        # uniId = elt
        # description = "This protein is listed as a precursor protein for a functional amyloid complex in a publication. "
        # evidence = "ECO_0000322"
        # annotId  = uniId+"_22817896_" +STR_AMNAME+"_"+evidence
        # annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"))
        # annot.create(evidence,URIRef(STR_AMKB+uniId),STR_AMNAME,description,URIRef("https://www.ncbi.nlm.nih.gov/pubmed/22817896"))
        # annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Functional"))


def inferFromPubliInUniprot() : 

    query = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    PREFIX unicore: <http://purl.uniprot.org/core/> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    SELECT distinct ?entity ?indicator 
    WHERE { ?entity rdf:type unicore:Protein . 
    ?entity unicore:citation ?citation . 
    ?citation skos:exactMatch ?indicator . 
    ?citation unicore:title ?title . FILTER regex(?title,'functional amyloid','i') 
    }"""

    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': query }
    r=requests.get(UNIQUERY,myparam,headers=headers)    
    data = r.json()

    evidence = "ECO_0000363"
    description = "This protein is associated with a publication mentioning the terms 'functional amyloid' in its title"

    for row in data["results"]["bindings"] : 
        uniUri = row["entity"]["value"]
        publiUri = row["indicator"]["value"]
        entityId = uniUri.split("/")[-1]
        pubId = publiUri.split("/")[-1]
        p = Protein(entityId)
        annotId  = entityId+"_"+pubId+"_"+STR_AMNAME+"_"+evidence
        annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Functional"))
        annot.create(evidence,p.uri,STR_AMNAME,description,URIRef(publiUri))
        annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"))


def inferFromUniprotKeywords() : 

    endpoint = "http://localhost:8080/rdf4j-server/repositories/amyp"

    query = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    PREFIX unicore: <http://purl.uniprot.org/core/> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    SELECT distinct ?entity ?indicator ?indicatorLabel ?tag
    WHERE {{ ?entity rdf:type unicore:Protein . 
    ?entity unicore:classifiedWith ?keyword . 
    SERVICE <{endpoint}> {{
    ?indicator owl:sameAs ?keyword .
    ?indicator skos:inScheme <{scheme}> .
    ?indicator skos:prefLabel ?indicatorLabel .
    ?indicator skos:broadMatch ?tag .
    }}
    }}""".format(endpoint=endpoint, scheme=STR_AMTHES+"Uniprot/scheme")

    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': query }
    r=requests.get(UNIQUERY,myparam,headers=headers)    
    data = r.json()
    evidence = "ECO_0000363"
    

    for row in data["results"]["bindings"] : 
        uniUri = row["entity"]["value"]
        keywordUri = row["indicator"]["value"]
        entityId = uniUri.split("/")[-1]
        keywordId = keywordUri.split("/")[-1]
        p = Protein(entityId)
        description = "This protein is tagged with the keyword " + row["indicatorLabel"]["value"] + " in Uniprot"
        annotId  = entityId+"_"+keywordId+"_Uniprot_"+evidence
        if row["tag"]["value"] == STR_AMTHES+STR_AMNAME+"_Amyloid"  : 
            annot = Annotation(annotId,URIRef(row["tag"]["value"]))
        else : 
            annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Involved_In_Amyloidosis"))
        annot.create(evidence,p.uri,"Uniprot",description,Literal("keyword : " + row["indicatorLabel"]["value"]))


def inferFromText() : 

    query="""
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    PREFIX unicore: <http://purl.uniprot.org/core/>  
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  
    SELECT distinct ?entity  ?comment 
    WHERE { ?entity rdf:type unicore:Protein .  
    ?entity unicore:annotation ?annot .  
    ?annot rdfs:comment ?comment . 
    FILTER regex(?comment,'(cleav|augment|degrad|induc|regulat|increas|decreas)(e|es|ing|ed)[A-Za-z ,]*(amyloid|prion)','i') } 
    """

    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': query }
    r=requests.get(UNIQUERY,myparam,headers=headers)    
    data = r.json()

    evidence = "ECO_0000363"
    description = "This protein is associated with a comment mentioning an interaction with an amyloid entity in Uniprot"

    for row in data["results"]["bindings"] : 
        uniUri = row["entity"]["value"]
        comment = row["comment"]["value"]
        entityId = uniUri.split("/")[-1]
        p = Protein(entityId)
        annotId  = entityId+"_comment_"+Uniprot().name+"_"+evidence
        annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amylome_Interactor"))
        annot.create(evidence,p.uri,Uniprot().name,description,Literal(comment))



def addTagToProtein() : 

    query="""
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    CONSTRUCT { ?prot model:tag ?qualifier }
        WHERE { ?annot rdf:type model:Annotation .
    {?annot model:referringTo ?prot .  ?annot model:qualifier ?qualifier . ?prot rdf:type model:Protein .  }
    UNION
    {?annot model:referringTo ?iso .  ?annot model:qualifier ?qualifier . ?iso rdf:type model:Isoform .  ?prot model:isoform ?iso  . ?prot rdf:type model:Protein .}
    UNION
    {?annot model:referringTo ?poly . ?annot model:qualifier ?qualifier . ?poly rdf:type model:Polypeptide . ?iso model:cleavedInto ?poly . ?iso rdf:type model:Isoform .  ?prot model:isoform ?iso . ?prot rdf:type model:Protein . }
    } 
    """

    myparam = { 'query': query}
    header = {'Content-Type' : 'application/rdf+n3' }
    results=requests.get(AMQUERY,params=myparam,headers=header)
    graph = Graph()
    graph.parse(data=results.content, format="n3")

    update = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=graph.serialize(format='n3'),g=STR_AMKB)
    headers = {'content-type' : 'application/x-www-form-urlencoded'}
    cont = URIRef(STR_AMKB)
    myparam = { 'context' : cont,'update': update}
        #print("update endpoint" + self._updateEndpoint)
        #print("query" + query)
    try : 
        r=requests.post(AMUPDATE,myparam,headers=headers) 
    except :
        print("There was a problem trying to add  "  + self._stUri + " to the triple store")

    query2="""
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    CONSTRUCT {{ ?prot model:tag <{amylome}> }}
    WHERE {{ ?prot rdf:type model:Protein . ?prot model:tag <{amyloid}> . }}
    """.format(amyloid=STR_AMTHES+STR_AMNAME+"_Amyloid", amylome=STR_AMTHES+STR_AMNAME+"_Amylome_Interactor")

    myparam = { 'query': query2}
    header = {'Content-Type' : 'application/rdf+n3' }
    results=requests.get(AMQUERY,params=myparam,headers=header)
    graph = Graph()
    graph.parse(data=results.content, format="n3")

    update = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=graph.serialize(format='n3').decode(),g=STR_AMKB)
    headers = {'content-type' : 'application/x-www-form-urlencoded'}
        #headers = {'content-type' : 'application/sparql-update'}
        #cont = "<"+self._stUriBase+">"
    cont = URIRef(STR_AMKB)
    myparam = { 'context' : cont,'update': update}
        #print("update endpoint" + self._updateEndpoint)
        #print("query" + query)
    try : 
        r=requests.post(AMUPDATE,myparam,headers=headers) 
    except :
        print("There was a problem trying to add  "  + self._stUri + " to the triple store")
    

def addTagToDisease() : 

    query="""
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    CONSTRUCT { ?disease model:tag ?qualifier }
    WHERE { 
   { ?annot model:referringTo ?disease . ?annot model:qualifier ?qualifier . ?disease rdf:type model:Disease . } 
   UNION 
   { ?disease rdf:type model:Disease . ?disease model:thesRef ?thes . ?thes skos:broadMatch ?qualifier }
       }
    """

    myparam = { 'query': query}
    header = {'Content-Type' : 'application/rdf+n3' }
    results=requests.get(AMQUERY,params=myparam,headers=header)
    graph= Graph()
    graph.parse(data=results.content, format="n3")

    update = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=graph.serialize(format='nt').decode(),g=STR_AMKB)
    headers = {'content-type' : 'application/x-www-form-urlencoded'}
        #headers = {'content-type' : 'application/sparql-update'}
        #cont = "<"+self._stUriBase+">"
    cont = URIRef(STR_AMKB)
    myparam = { 'context' : cont,'update': update}
        #print("update endpoint" + self._updateEndpoint)
        #print("query" + query)
    try : 
        r=requests.post(AMUPDATE,myparam,headers=headers) 
    except :
        print("There was a problem trying to add  "  + self._stUri + " to the triple store")


   
def addTagToProtFromDisease() : 
    queryAnnot1 = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    SELECT ?entity ?entityId ?diseaseId ?diseaseLabel ?tagLabel
    WHERE {{
    ?proda rdf:type model:Proda .
    ?proda model:referringTo ?disease . 
    ?disease rdf:type model:Disease .
    ?disease model:tag ?tag .
    ?disease rdfs:label ?diseaseLabel .
    ?tag skos:prefLabel ?tagLabel .
    ?disease model:accession ?diseaseId .
    VALUES ?tag {{ <{amyloidosis}>  <{amyloidrel}> }} 
    ?proda model:referringTo ?entity .
    ?entity rdf:type model:Protein .
    ?entity model:accession ?entityId . 
     }}""".format(amyloidosis=STR_AMTHES+STR_AMNAME+"_Amyloidosis",amyloidrel=STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease")

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(queryAnnot1)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()

    source = STR_AMNAME
    evidence = "ECO_0000363"

    for row in results["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_Involved_In_"+row["diseaseId"]["value"]+source+"_"+evidence 
        annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Involved_In_Amyloidosis")       
        annot = Annotation(annotId,annotType)
        if not annot.exists() :
            description = "This protein is associated to " + row["diseaseLabel"]["value"] + " which is tagged as a " + row["tagLabel"]["value"]
            annot.create(evidence,URIRef(row["entity"]["value"]),source,description, Literal("Association to " + row["diseaseLabel"]["value"]))
            prot = Protein(row["entityId"]["value"])
            tempG = Graph()
            tempG.add([prot.uri,AMMODEL.tag,annotType])
            prot.addEntityGraphToStore(tempG)


    # query1="""
    # PREFIX model: <http://purl.amypdb.org/model/> 
    # PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    # PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    # PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    # CONSTRUCT {{ ?prot model:tag <{quali}> }}
    #     WHERE {{
    # ?proda rdf:type model:Proda . ?proda model:referringTo ?disease . 
    # ?disease rdf:type model:Disease . ?disease model:tag ?tag .
    # VALUES ?tag {{ <{amyloidosis}>  <{amyloidrel}> }} 
    # ?proda model:referringTo ?prot . ?prot rdf:type model:Protein . 
    #  }} """.format(quali=STR_AMTHES+STR_AMNAME+"_Involved_In_Amyloidosis", amyloidosis=STR_AMTHES+STR_AMNAME+"_Amyloidosis",amyloidrel=STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease")

    # myparam = { 'query': query1}
    # header = {'Content-Type' : 'application/rdf+n3' }
    # print("first query")
    # results=requests.get(AMQUERY,params=myparam,headers=header)
    print("queryAnnot2")

    queryAnnot2 = """
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX owl: <http://www.w3.org/2002/07/owl#>  
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
SELECT ?entity ?entityId ?diseaseId ?diseaseLabel ?tagLabel
WHERE {{
?vda rdf:type model:Vda .
?vda model:referringTo ?disease . 
?disease rdf:type model:Disease .
?disease rdfs:label ?diseaseLabel .
?disease model:accession ?diseaseId .
?disease model:tag <{amyloidosis}> .
<{amyloidosis}> skos:prefLabel ?tagLabel .
?vda model:referringTo ?variant .
?variant rdf:type model:VariantAa . 
?iso model:variant ?variant . 
?iso rdf:type model:Isoform .
?entity model:isoform ?iso . 
?entity rdf:type model:Protein .
?entity model:accession ?entityId . 
      }}""".format(amyloidosis=STR_AMTHES+STR_AMNAME+"_Amyloidosis")

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(queryAnnot2)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()

    source = STR_AMNAME
    evidence = "ECO_0000363"

    for row in results["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_Involved_In_"+row["diseaseId"]["value"]+source+"_"+evidence 
        annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Involved_In_Amyloidosis")       
        annot = Annotation(annotId,annotType)
        
        description = "This protein has a variant which is associated to " + row["diseaseLabel"]["value"] + " which is tagged as a " + row["tagLabel"]["value"]
        annot.create(evidence,URIRef(row["entity"]["value"]),source,description, Literal("Association to " + row["diseaseLabel"]["value"]))
        prot = Protein(row["entityId"]["value"])
        tempG = Graph()
        tempG.add([prot.uri,AMMODEL.tag,annotType])
        prot.addEntityGraphToStore(tempG)


    print("queryAnnot3")
    queryAnnot3 = """
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX owl: <http://www.w3.org/2002/07/owl#>  
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
SELECT ?entity ?entityId ?diseaseId ?diseaseLabel ?tagLabel
WHERE {{
?vda rdf:type model:Vda .
?vda model:referringTo ?disease . 
?disease rdf:type model:Disease .
?disease rdfs:label ?diseaseLabel .
?disease model:accession ?diseaseId .
?disease model:tag <{amyloidrel}> .
<{amyloidrel}> skos:prefLabel ?tagLabel .
?vda model:referringTo ?variant .
?variant rdf:type model:VariantAa . 
?iso model:variant ?variant . 
?iso rdf:type model:Isoform .
?entity model:isoform ?iso . 
?entity rdf:type model:Protein .
?entity model:accession ?entityId . 
     }}""".format(amyloidrel=STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease")

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(queryAnnot3)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()

    source = STR_AMNAME
    evidence = "ECO_0000363"

    for row in results["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_Involved_In_"+row["diseaseId"]["value"]+source+"_"+evidence 
        annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Involved_In_Amyloidosis")       
        annot = Annotation(annotId,annotType)
        description = "This protein has a variant which is associated to " + row["diseaseLabel"]["value"] + " which is tagged as a " + row["tagLabel"]["value"]
        annot.create(evidence,URIRef(row["entity"]["value"]),source,description, Literal("Association to " + row["diseaseLabel"]["value"]))
        prot = Protein(row["entityId"]["value"])
        tempG = Graph()
        tempG.add([prot.uri,AMMODEL.tag,annotType])
        prot.addEntityGraphToStore(tempG)

    # query2="""
    # PREFIX model: <http://purl.amypdb.org/model/> 
    # PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    # PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    # PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    # CONSTRUCT {{ ?prot model:tag <{quali}> }}
    #     WHERE {{
    # ?vda rdf:type model:Vda . ?vda model:referringTo ?disease . 
    # ?disease rdf:type model:Disease . ?disease model:tag ?tag .
    # VALUES ?tag {{ <{amyloidosis}>  <{amyloidrel}> }} 
    # ?vda model:referringTo ?variant . ?variant rdf:type model:VariantAa . 
    # ?iso model:variant ?variant . ?iso rdf:type model:Isoform . ?prot model:isoform ?isoform . ?prot rdf:type model:Protein .  
    #  }} """.format(quali=STR_AMTHES+STR_AMNAME+"_Involved_In_Amyloidosis", amyloidosis=STR_AMTHES+STR_AMNAME+"_Amyloidosis",amyloidrel=STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease")

    # myparam = { 'query': query2}
    # print("second query")
    # results2=requests.get(AMQUERY,params=myparam,headers=header)

    # graph= Graph()
    # graph.parse(data=results.content, format="n3")
    # graph.parse(data=results2.content, format="n3")

    # update = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=graph.serialize(format='nt').decode(),g=STR_AMKB)
    # headers = {'content-type' : 'application/x-www-form-urlencoded'}
    #     #headers = {'content-type' : 'application/sparql-update'}
    #     #cont = "<"+self._stUriBase+">"
    # cont = URIRef(STR_AMKB)
    # myparam = { 'context' : cont,'update': update}
    #     #print("update endpoint" + self._updateEndpoint)
    #     #print("query" + query)
    # try : 
    #     r=requests.post(AMUPDATE,myparam,headers=headers) 
    # except :
    #     print("There was a problem trying to add  "  + self._stUri + " to the triple store")    

def createBasicThesaurus():
    print("Creating Thesaurus...")
    Thes = Thesaurus()
    Thes.createAmyloidTerms()
    Thes.createCath()
    Thes.createFromExtDescriptors(PDB())
    Thes.createFromExtDescriptors(Go())
    Thes.createFromExtDescriptors(Reactome())
    Thes.createFromExtDescriptors(Mondo())
    Thes.createFromExtDescriptors(Uniprot())
    Thes.createInterpro()


def createEntries():
    print("###################### CREATING ENTRIES #######################")
    print("create Proteins")
    createEntriesFromUniprot()
    ##print("add info for isoforms that are not from the initial pool of data")
    ##createLabelForIsoforms()
    print("create Entrie from Amyload")
    createEntriesFromAmyload()
    print("create Entries from ALBase")
    createEntriesFromALBase()
    ##createEntriesFromAmyp(amyp_pept_file)

def addAnnotations():
    print("################## ADD ANNOTATIONS ##########################")
    print("add annotation for Amyload")
    addAnnotForAmyload()
    print("add annotations for ALBAse")
    addAnnotForALBase()
    
    print("infer annotations from ALBase")
    inferFromALBase()
    ##addAnnotForAmyp()
    print("infer annotations from fragments")
    inferFromFragment()

    print("infer annotations from PDB")
    inferFromPDB() 
    print("infer annotations from Go")
    inferFromGo()  

    print("infer annotations from ISA")
    inferFromISA() 
    print("infer annotation from Signatures")
    inferFromSignatures()  
    print("infer annotations from Amypro")
    inferFromAmyPro()
    print("infer annotations from Articles")
    inferFromArticles() # https://www.ncbi.nlm.nih.gov/pubmed/25131597
    print("infer annotations from Reactome")    
    inferFromReactome()
    print("infer annotations from Uniprot Publications")
    inferFromPubliInUniprot()

    print("infer annotations from Uniprot Keywords")
    inferFromUniprotKeywords()

    print("infer from text")
    inferFromText()


    print("infer annotation from Mondo")
    inferFromMondo()

    print("infer annotation from Disease associations")
    inferFromDiseaseAssociations()


def defineStatus():

    print("add Amyloid / Amyloid-like / Amyloid-interactor to prot")
    addTagToProtein()
    print("add tag to diseases")
    addTagToDisease()
    print("add tag involved in from disease tag")
    addTagToProtFromDisease()


    #inferFromDisease()
    #inferFromKeywords()    
    #inferFromAggrescan()
    #inferFromMesh()     "functional amyloid"
    #inferFromText()     "forms amyloid formation" "amyloid-like"




    #addAnnotationsFromPDB()
    #addAnnotationsFromMondo()


def createDict(query,dict) :
    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    data=sparql.query().convert()
    
    for row in data["results"]["bindings"] : 
        cl = row["cl"]["value"]
        clcount = row["nbcl"]["value"]
        dict[cl] = clcount

    return dict


def createSiteInfo():

    print("FOR DISEASES")
    print("Dict pheno 1")


    dictAll = {}
    print("Dict pheno")
    queryPheno2="""
PREFIX model: <http://purl.amypdb.org/model/>
PREFIX owl: <http://www.w3.org/2002/07/owl#> 
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX thes: <http://purl.amypdb.org/thesaurus/>
SELECT ?cl (count (distinct ?disease) as ?nbcl) 
WHERE {
?disease rdf:type model:Disease .
?disease model:tag ?tag . 
VALUES ?tag { thes:Amyp_Amyloid_Related_Disease thes:Amyp_Amyloidosis } .
?disease model:phenoRef ?phRef . ?phRef skos:prefLabel ?cl . 
} GROUP BY ?cl ORDER BY DESC(?nbcl) """


    dictPhenoAmyloidRelated = {}
    dictPhenoAmyloidRelated = createDict(queryPheno2,dictPhenoAmyloidRelated)
    dictAll["dictDiseasePhenoAmyRelated"]=dictPhenoAmyloidRelated
    
    print("Dict fam 3")

    amypscheme = STR_AMTHES+STR_AMNAME+"/scheme"

    queryDisFam ="PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  \
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  \
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
    SELECT ?cl (count (distinct ?disease) as ?nbcl) \
    WHERE {{  \
    ?disease rdf:type model:Disease . \
    ?disease model:tag ?tag . \
    VALUES  ?tag {{ thes:{am}_Amyloidosis thes:{am}_Amyloid_Related_Disease  }} . \
    {{  ?annot rdf:type model:Proda . ?annot model:referringTo ?disease . \
    ?annot model:referringTo ?prot . }} \
    UNION {{ \
    ?annot rdf:type model:Vda . ?annot model:referringTo ?disease . \
    ?annot model:referringTo ?var . ?var rdf:type model:VariantAa . ?iso model:variant ?var . ?iso rdf:type model:Isoform . ?prot model:isoform ?iso . \
    }} \
    ?prot rdf:type model:Protein . ?prot model:family ?fam . ?fam skos:prefLabel ?cl . ?fam skos:inScheme  <{scheme}> \
       }} GROUP BY ?cl ORDER BY DESC(?nbcl) ".format(am=STR_AMNAME,scheme = amypscheme)
  
    dictDisFam = {}
    dictDisFam = createDict(queryDisFam,dictDisFam)
    dictAll["dictDiseaseFamily"]=dictDisFam

    print("Dict CATH")
    cathScheme = STR_AMTHES+"CATH/scheme"
    queryDisCath ="PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  \
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  \
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
    SELECT ?cl (count (distinct ?disease) as ?nbcl) \
    WHERE {{  \
    ?disease rdf:type model:Disease . \
    ?disease model:tag ?tag . \
    VALUES  ?tag {{ thes:{am}_Amyloidosis thes:{am}_Amyloid_Related_Disease  }} . \
    {{  ?annot rdf:type model:Proda . ?annot model:referringTo ?disease . \
    ?annot model:referringTo ?prot . }} \
    UNION {{ \
    ?annot rdf:type model:Vda . ?annot model:referringTo ?disease . \
    ?annot model:referringTo ?var . ?var rdf:type model:VariantAa . ?iso model:variant ?var . ?iso rdf:type model:Isoform . ?prot model:isoform ?iso . \
    }} \
    ?prot rdf:type model:Protein . ?prot model:family ?fam . ?fam skos:broader ?supf . ?supf skos:prefLabel ?cl . ?fam skos:inScheme  <{scheme}> \
       }} GROUP BY ?cl ORDER BY DESC(?nbcl) ".format(am=STR_AMNAME,scheme = cathScheme)

    dictDisCath = {}
    #dictDisCath = createDict(queryDisCath,dictDisCath)
    #dictAll["dictDiseaseCath"]=dictDisFam

    print("FOR PROTEINS")
    print("Dict ")

    queryProtType="PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  \
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  \
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
    SELECT ?cl (count (distinct ?protein) as ?nbcl) \
    WHERE {{  \
    ?protein rdf:type model:Protein . \
    ?protein model:tag ?tag . \
    VALUES ?tag {{ thes:{am}_Amyloid thes:{am}_Amyloid_Like thes:{am}_Amylome_Interactor }} . \
    ?tag skos:prefLabel ?cl . \
       }} GROUP BY ?cl ORDER BY DESC(?nbcl) ".format(am=STR_AMNAME)

    dictProtType = {}
    dictProtType = createDict(queryProtType,dictProtType)
    dictAll["dictProteinType"]=dictProtType

    queryProtFunct="PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  \
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  \
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
    SELECT ?cl (count (distinct ?protein) as ?nbcl) \
    WHERE {{  \
    ?protein rdf:type model:Protein . \
    ?protein model:tag ?tag . \
    VALUES ?tag {{ thes:{am}_Functional thes:{am}_Pathogenic thes:{am}_Variant_Only }} . \
    ?tag skos:prefLabel ?cl . \
       }} GROUP BY ?cl ORDER BY DESC(?nbcl) ".format(am=STR_AMNAME)

    dictProtFunct = {}
    dictProtFunct = createDict(queryProtFunct,dictProtFunct)
    dictAll["dictProteinFunct"]=dictProtFunct

    queryProtTaxo="PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  \
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  \
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
    SELECT ?cl (count (distinct ?protein) as ?nbcl) \
    WHERE {{  \
    ?protein rdf:type model:Protein . \
    ?protein model:tag ?tag . \
    VALUES ?tag {{ thes:{am}_Amyloid thes:{am}_Amyloid_Like thes:{am}_Amylome_Interactor  }} . \
    ?protein model:organism ?org . \
    ?org skos:prefLabel ?cl . \
       }} GROUP BY ?cl ORDER BY DESC(?nbcl) ".format(am=STR_AMNAME)

    dictProtTaxo = {}
    dictProtTaxo = createDict(queryProtTaxo,dictProtTaxo)
    dictAll["dictProteinTaxo"]=dictProtTaxo



    queryAmylome="PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  \
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  \
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
    SELECT ?cl (count (distinct ?entity) as ?nbcl) \
    WHERE {{  \
    {{ \
    ?entity rdf:type ?type . \
    VALUES ?cl {{  model:Protein }} . \
    ?entity model:tag ?tag . \
    VALUES ?tag {{ thes:{am}_Amyloid thes:{am}_Amyloid_Like thes:{am}_Amylome_Interactor  }} . \
    }} UNION {{ \
    ?entity rdf:type ?type . \
    VALUES ?cl {{  model:Disease }} . \
    ?entity model:tag ?tag . \
    VALUES ?tag {{ thes:{am}_Amyloidosis thes:{am}_Amyloid_Related_Disease  }} . \
    }} UNION {{ \
    ?entity rdf:type ?type . \
    VALUES ?cl {{  model:Gene }} . \
    ?entity model:encodes ?prot . \
    ?prot model:tag ?tag . \
    VALUES ?tag {{ thes:{am}_Amyloid thes:{am}_Amyloid_Like thes:{am}_Amylome_Interactor  }} . \
    }} \
    BIND( \
    IF(regex(str(?type),'Protein'), 'Protein', \
    IF(regex(str(?type),'Disease'), 'Disease', \
    IF(regex(str(?type),'Gene'), 'Gene', \
    'Unknown')))  AS ?cl ) . \
    }} GROUP BY ?cl ".format(am=STR_AMNAME)

    dictAmylome = {}
    dictAmylome = createDict(queryAmylome,dictAmylome)
    dictAll["dictAmylome"]=dictAmylome

    
    with open(site_json_file, 'w') as outfile:  
        json.dump(dictAll, outfile)



if __name__ == '__main__':
    createBasicThesaurus()
    createEntries()
    addAnnotations()
    defineStatus()
    createSiteInfo()
