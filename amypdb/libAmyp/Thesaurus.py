
#!/usr/bin/python
# -*- coding: UTF-8 -*-
from BioManager import *
from rdflib import *
from rdflib.namespace import SKOS,RDF,RDFS,OWL
from SPARQLWrapper import JSON,N3,XML,TURTLE
import requests
from amypdb.libAmyp.AmyConf import *
import csv

class Thesaurus(object):
    """
        Manage descriptors specific to Amypdb from external resources
    """

    def __init__(self):
        print("Thesaurus")

    def createFromExtDescriptors(self,bioEntity) :
        print("Descriptor extraction from ", bioEntity.name)
        resultGraph=self._extractDescriptors(bioEntity) 
        print("Creation of our own descriptors from ", bioEntity.name)
        self._transformToAmyDesc(resultGraph,bioEntity)

    def _createId(self,uriSource,bioEntity) :
        uriId = uriSource.split(bioEntity.idSeparator)[-1]
        return uriId


    def _createDescUri(self,uriSource,bioEntity) :
        uriId = uriSource.split(bioEntity.idSeparator)[-1]
        link = STR_AMTHES + bioEntity.name + "_"+uriId
        if bioEntity.baseUri == Orphanet().baseUri or bioEntity.baseUri == Go().baseUri or bioEntity.baseUri == Mondo().baseUri :
            link = STR_AMTHES + uriId
        elif bioEntity.baseUri == Uniprot().baseUri :
            link = STR_AMTHES + bioEntity.name + "_" + uriSource.split(bioEntity.idSeparator)[-2] + "_"+ uriSource.split(bioEntity.idSeparator)[-1]
        return URIRef(link)

    def _transformToAmyDesc(self,resultGraph,bioEntity) : 
        
        g=Graph()
        g.bind('',URIRef(STR_AMTHES))
        g.bind('skos',URIRef(u'http://www.w3.org/2004/02/skos/core#'))
        g.bind('rdf',URIRef(u'http://www.w3.org/1999/02/22-rdf-syntax-ns#'))
        g.bind('rdfs',URIRef(u'http://www.w3.org/2000/01/rdf-schema#'))
        g.bind('owl',URIRef(u'http://www.w3.org/2002/07/owl#'))
        #create conceptScheme
        conceptScheme = URIRef(STR_AMTHES+bioEntity.name+"/scheme")
        g.add([conceptScheme, RDF.type, SKOS.ConceptScheme])
        g.add([conceptScheme, RDFS.label, Literal("Descripteurs issus de "+ bioEntity.name)])
        g.add([conceptScheme, DCTERMS.title, Literal("Descripteurs issus de "+bioEntity.name)])

        for sub in resultGraph.subjects(None,None) :
            if not isinstance(sub,BNode) : 
            
                #Subject creation + in scheme
                subA = self._createDescUri(sub,bioEntity)
                #print("descSubj :", subA)
                
                g.add([subA, RDF.type, SKOS.Concept])
                g.add([subA, SKOS.inScheme, conceptScheme])      
                g.add([subA, OWL.sameAs,sub])
                if "rdf.wwpdb.org/pdb" in str(sub) :
                  httpSub = str(sub).replace("https","http")
                  g.add([subA, OWL.sameAs,URIRef(httpSub)])
                g.add([subA, AMMODEL.accession, Literal(self._createId(sub,bioEntity))])


                #ON AJOUTE LA PROPRIETE ISAMYLOID
                #g.add([subA,URIRef(STR_AMMODEL+"amyloidMention"), Literal(True, datatype=XSD.boolean)])   

                for pred,obj in resultGraph.predicate_objects(sub) :
                    #print("predicate :", pred)
                    
            
                    if pred == SKOS.prefLabel or pred == URIRef(u'http://www.biopax.org/release/biopax-level3.owl#displayName'):
                        g.add([subA, SKOS.prefLabel, obj])
                    elif pred == RDFS.label : 
                        if len(g.preferredLabel(subA)) < 1 :
                            g.add([subA, SKOS.prefLabel, obj])
                    elif pred == URIRef(u'http://www.ebi.ac.uk/efo/alternative_term') or pred == SKOS.altLabel or pred == URIRef(u'http://www.geneontology.org/formats/oboInOwl#hasNarrowSynonym') or pred == URIRef(u'http://www.geneontology.org/formats/oboInOwl#hasExactSynonym') or pred == URIRef(u'http://www.geneontology.org/formats/oboInOwl#hasRelatedSynonym') :
                        g.add([subA, SKOS.altLabel, obj])
                
                #Description generation
                    elif pred == URIRef(u'http://www.ebi.ac.uk/efo/definition') or pred == URIRef(u'http://purl.obolibrary.org/obo/IAO_0000115') :
                        g.add([subA, SKOS.definition, obj])           

                    elif pred == SKOS.scopeNote :
                        if isinstance (obj,Literal) : 
                            g.add([subA,SKOS.scopeNote, obj])

                    elif pred == SKOS.note :
                        if isinstance (obj,Literal) :
                            g.add([subA,SKOS.note, obj])

                    elif pred == RDFS.comment : 
                        g.add([subA,SKOS.note, obj])

                #Intra-source relationships
                    elif pred == RDFS.subClassOf or pred == URIRef(u'http://id.nlm.nih.gov/mesh/vocab#broaderDescriptor') or pred == SKOS.broader :
                        if ( not isinstance(obj, BNode) ) and ( obj != URIRef(u'http://www.orpha.net/ORDO/ObsoleteClass') ) and  ( obj != URIRef(u'http://www.semanticweb.org/ontology/HOOM#Orpha_Num')) :
                            g.add([subA, SKOS.broader,self._createDescUri(obj,bioEntity)]) 

                    elif pred == URIRef(u'http://data.bioontology.org/metadata/obo/part_of') :
                        if not isinstance(obj, BNode):
                            g.add([subA, URIRef(u'http://www.w3.org/2004/02/skos/extensions#broaderPartitive'), self._createDescUri(obj,bioEntity)])
                    elif pred == SKOS.narrower :
                        g.add([subA,SKOS.narrower,self._createDescUri(obj,bioEntity)])

                
                #Inter-sources relationships
                    elif pred == URIRef(u'http://www.geneontology.org/formats/oboInOwl#hasDbXref'): 
                        strval=obj.value
                        if (strval.startswith("ICD-10")) or (strval.startswith("MeSH")) or (strval.startswith("OMIM")) or (strval.startswith("UMLS")) :
                            link=STR_AMTHES+strval.split(":")[0]+"_"+strval.split(":")[-1]
                            g.add([subA,SKOS.relatedMatch,URIRef(link)])

                    elif pred == SKOS.exactMatch:
                        strval=str(obj)
                        if "umls" in strval :
                            link=STR_AMTHES+UMLS().name+"_"+strval.split("/")[-1]
                            g.add([subA,SKOS.exactMatch,URIRef(link)])
                        elif "mesh" in strval : 
                            link=STR_AMTHES+Mesh().name+"_"+str(obj).split("/")[-1]
                            g.add([subA,SKOS.exactMatch,URIRef(link)])
                        elif "ORDO" in strval : 
                            link=STR_AMTHES+str(obj).split("/")[-1]
                            g.add([subA,SKOS.exactMatch,URIRef(link)])



                    elif pred == RDFS.seeAlso :
                        if "mesh" in str(obj) or "/mim/" in str(obj) : 
                            link=STR_AMTHES+Mesh().name+"_"+str(obj).split("/")[-1]
                            if "/mim/" in str(obj) :
                                link=STR_AMTHES+Omim().name+"_"+str(obj).split("/")[-1]
                            g.add([subA,SKOS.relatedMatch,URIRef(link)])

                     # Type : Disease/Protein/
                    #elif pred == SKOS.changeNote or pred == URIRef(u'http://www.geneontology.org/formats/oboInOwl#hasOBONamespace') :
                        #g.add([subA,AMMODEL.refType,URIRef(STR_AMMODEL+obj)])

                    # DESCRIPTOR : amyloid / amyloid_like / Amylose... 
                    elif pred == SKOS.broadMatch : 
                        g.add([subA,SKOS.broadMatch, obj])
                        

                    #elif pred == RDF.type and "http://www.biopax.org/" in str(obj) :
                        #print(str(obj))
                        #g.add([subA,RDF.type,URIRef(STR_AMMODEL+str(obj).split("#")[-1])])

                    #elif pred == RDF.type and Uniprot().baseUri in str(obj) : 
                        #g.add([subA,RDF.type,URIRef(STR_AMMODEL+str(obj).split("/")[-1])])



    
        for s,o in g.subject_objects(SKOS.broader) :
            #print("removing triples")
            if (o, SKOS.prefLabel, None) not in g : 
                g.remove([s,SKOS.broader,o])
        

        for s in g.subjects(None,None) :           
            if (s, SKOS.broader, None) not in g and not isinstance(s,BNode) and (s,RDF.type,SKOS.conceptScheme) not in g :
                g.add([s, SKOS.topConceptOf,conceptScheme])
        

        g.serialize(destination="data/results"+bioEntity.name+".ttl", format="nt")

        self.addEntityGraphToStore(g)


    def addEntityGraphToStore(self,graph) :
        #print("adding statements to endpoint " + self._updateEndpoint)
        #print(self._stUriBase)
        query = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=graph.serialize(format='nt').decode(),g=STR_AMTHES)
        headers = {'content-type' : 'application/x-www-form-urlencoded'}
        cont = URIRef(STR_AMTHES)
        myparam = { 'context' : cont,'update': query}
        r=requests.post(AMUPDATE,myparam,headers=headers)       
        print("Adding graph " + str(r.status_code))



    def createTopConcept(self,g,conceptSchemeUri,topConceptUri,prefLabel) :
        g.add([topConceptUri, SKOS.topConceptOf,conceptSchemeUri])
        g.add([topConceptUri, RDF.type, SKOS.Concept])
        g.add([topConceptUri, SKOS.inScheme, conceptSchemeUri])
        g.add([topConceptUri, SKOS.prefLabel, Literal(prefLabel)])
        g.add([topConceptUri, AMMODEL.accession, Literal(str(topConceptUri.split("/")[-1]))])
        return g

    def createSubConcept(self,g,conceptSchemeUri,topConceptUri,subConceptUri,prefLabel) :
        g.add([subConceptUri, SKOS.broader,topConceptUri])
        g.add([subConceptUri, RDF.type, SKOS.Concept])
        g.add([subConceptUri, SKOS.inScheme, conceptSchemeUri])
        g.add([subConceptUri, SKOS.prefLabel, Literal(prefLabel)])
        g.add([subConceptUri, AMMODEL.accession, Literal(str(subConceptUri.split("/")[-1]))])
        return g


    def createAmyloidTerms(self) :
        g=Graph()
        g.bind('',URIRef(STR_AMTHES))
        g.bind('skos',URIRef(u'http://www.w3.org/2004/02/skos/core#'))
        g.bind('rdf',URIRef(u'http://www.w3.org/1999/02/22-rdf-syntax-ns#'))
        g.bind('rdfs',URIRef(u'http://www.w3.org/2000/01/rdf-schema#'))
        g.bind('owl',URIRef(u'http://www.w3.org/2002/07/owl#'))
        #create conceptScheme
        conceptSchemeUri = URIRef(STR_AMTHES+STR_AMNAME+"/scheme")
        g.add([conceptSchemeUri, RDF.type, SKOS.ConceptScheme])
        g.add([conceptSchemeUri, RDFS.label, Literal("Descripteurs issus de : "+ STR_AMNAME)])
        g.add([conceptSchemeUri, DCTERMS.title, Literal("Descripteurs issus de : "+ STR_AMNAME)])

        #MONDO : http://purl.amypdb.org/model/Amyloidosis http://purl.amypdb.org/model/Amyloid_Related_Disease
#REACTOME : Amyloid_Pathway Amyloid_Complex
#GO : Amyloid_Process Amyloid_Function Amyloid_Complex
#PDB : Amyloid_Fibril Amyloid_Structure Amyloid_Like_Structure
#AMYLOAD : Amyloidogenic_Fragment
        topConceptUri = URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Descriptor")
        g = self.createTopConcept(g,conceptSchemeUri,topConceptUri,"Amyloid descriptor") 
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloidosis"),"Amyloidosis")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease"),"Amyloid-related disease")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Process"),"Process related to amyloid formation")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Pathway"),"Pathway related to amyloid formation")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Function"),"Molecular function related to amyloid process")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Complex"),"Amyloid complex")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Fibril"),"Amyloid fibril")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Structure"),"Amyloid structure")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Like_Structure"),"Amyloid-like structure")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloidogenic"),"Amyloidogenic")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"),"Amyloid")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Like"),"Amyloid-like")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Amylome_Interactor"),"Amylome Interactor")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Pathogenic"),"Pathogenic")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Functional"),"Functional")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Hereditary"),"Hereditary")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Acquired"),"Acquired")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Variant_Only"),"Variants only")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Light_Chain"),"Amyloid Light Chain")
        g = self.createSubConcept(g,conceptSchemeUri,topConceptUri,URIRef(STR_AMTHES+STR_AMNAME+"_Involved_In_Amyloidosis"),"Involved in amyloidosis")

        
        g.serialize(destination="data/results_descr.ttl", format="nt")
        self.addEntityGraphToStore(g)



    def getNarrowerFrom(self,entity,type,g) :
        
        query = "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
          prefix owl: <http://www.w3.org/2002/07/owl#> \
          PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
          prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#> \
          prefix rdfs:<http://www.w3.org/2000/01/rdf-schema#> \
          CONSTRUCT {{ ?s skos:broadMatch <{type}> . \
          ?s ?p ?o . \
          ?s1 ?subClassOf ?s . \
          ?s1 ?p1 ?o1 . \
          ?s1 skos:broadMatch <{type}> . \
          }} \
          WHERE {{  \
          VALUES ?s {{ <{entity}> }} \
          VALUES ?p {{ rdfs:label oboInOwl:hasExactSynonym  oboInOwl:hasNarrowSynonym skos:exactMatch skos:closeMatch oboInOwl:IAO_0000115 }} \
          VALUES ?p1 {{ rdfs:label oboInOwl:hasExactSynonym  oboInOwl:hasNarrowSynonym skos:exactMatch skos:closeMatch oboInOwl:IAO_0000115 }} \
          ?s ?p ?o . \
          ?s1 ?subClassOf ?s . \
          ?s1 ?p1 ?o1 . \
          }} ".format(entity=entity,type=type)
        myparam = { 'query': query}
        header = {'Content-Type' : 'application/rdf+n3' }
        results1=requests.get(MONDOQUERY,params=myparam,headers=header)
        g.parse(data=results1.content, format="n3")
        #print(g.serialize(format="n3"))

        for sub in g.subjects(RDFS.subClassOf,URIRef(entity)) :
            print("entering loop for "+ str(sub))
            h=Graph()
            g+=self.getNarrowerFrom(str(sub),type,h)

        
        return g 


    def _extractDescriptors(self, bioEntity):



        amyloid = "amylo(id|gen|se|sis)|prion"
        amlike = "amyloid-like"
        prot = "Transthyretin|Beta(-| )*2(-| )*microglobulin|Apolipoprotein A(-| )*I|Apolipoprotein A(-| )*II|Apolipoprotein A(-| )*(IV|4)|Apolipoprotein C(-| )*(II|2)|Gelsolin|Lysozyme C|Leukocyte cell-derived chemotaxin-2|Fibrinogen alpha chain|Cystatin(-| )*C|Integral membrane protein 2B|Amyloid-beta precursor protein|Alpha-synuclein|Major prion protein|^Islet amyloid polypeptide|Natriuretic peptides A|^Prolactin$|^Insulin$|Lactadherin|Transforming growth factor-beta-induced protein ig-h3|Lactotransferrin$"
        relDiseases1 = "Alzheimer|Parkinson|Dementia(.)*Lewy|Multiple system atrophy|Creutzfeldt-Jacob|Fatal insomnia|Huntington disease-like 1|spongiform encephalopathy|^Kuru|tauopath|Pick(.)*disease|Progressive supranuclear palsy|Corticobasal degeneration|Argyrophilic grain|Tangle predominant dementia|Frontotemporal lobar degeneration|Chronic traumatic encephalopathy|Ganglioglioma|Meningioangiomatosis|Subacute sclerosing|Panencephalitis|Lead encephalopathy|Tuberous sclerosis|Hallervorden-Spatz|Lipofuscinosis|Huntington|Familial British dementia|Familial Danish dementia"
        relDiseases2 = "Type II diabetes|Insulinoma|Medullary carcinoma of the thyroid|Pituitary prolactinoma|corneal distrophy|Calcifying epithelial odontogenic|Pulmonary alveolar proteinosis|Hypotrichosis simplex of the scalp|CADASIL|Heavy chain deposition disease|Light chain deposition|myeloma cast nephropathy|Fanconi syndrome|Fibronectin glomerulopathy|Amyotrophic lateral sclerosis|C1q nephropathy|IgA nephropathy|Henoch(.)*purpura|Primary hyperoxaluria type 1|plasmacytoma|Medullary cystic kidney disease 2|Familial juvenile hyperuricemic nephropathy|Glomerulocystic kidney|Spinocerebellar ataxia 1|Sickle cell anemia|Heinz body anemia|Inclusion body(.)*thalassemia|alpha1-antitrypsin deficiency|Hereditary hyperferritinaemia cataract syndrom"





        ###############################################################################
        #                           MONDO                                 
        ###############################################################################
        if bioEntity.name == Mondo().name : 

            #On recherche toutes les entits qui sont des sous-classes de Amylose
            #On les annote en tant qu'amylose
            g = Graph()
            g += self.getNarrowerFrom("http://purl.obolibrary.org/obo/MONDO_0019065",STR_AMTHES+STR_AMNAME+"_Amyloidosis",g)
            #print("exit")
            #On recherche toutes les entit qui sont soit dans la liste des maladies associes ou contenant le nom dune prot amyloide et qui ne sont pas sous-classe de amylose
            #On les annote en tant qu'amyloid_related_disease

            query = "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
          prefix owl: <http://www.w3.org/2002/07/owl#> \
          PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
          prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#> \
          prefix rdfs:<http://www.w3.org/2000/01/rdf-schema#> \
          CONSTRUCT {{ ?s skos:broadMatch <{type}> . \
          ?s ?pLabel ?label . ?s ?p ?o . \
          }} \
          WHERE {{ \
          VALUES ?pLabel {{ rdfs:label oboInOwl:hasExactSynonym  oboInOwl:hasNarrowSynonym }} \
          VALUES ?p {{ rdfs:subClassOf oboInOwl:IAO_0000115 skos:exactMatch skos:closeMatch }} \
          ?s ?pLabel ?label . \
          ?s ?p ?o . \
          FILTER regex(?label,'{regex}','i') \
          FILTER NOT EXISTS {{ ?s rdfs:subClassOf* <http://purl.obolibrary.org/obo/MONDO_0019065>  }} \
          }} ".format(regex=relDiseases1,type=STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease")

            myparam = { 'query': query}
            header = {'Content-Type' : 'application/rdf+n3' }
            print("preparing for query")
            results1=requests.get(MONDOQUERY,params=myparam,headers=header)
            print("query")
            g.parse(data=results1.content, format="n3")
            print("parse")
            
            query2 = "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
          prefix owl: <http://www.w3.org/2002/07/owl#> \
          PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
          prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#> \
          prefix rdfs:<http://www.w3.org/2000/01/rdf-schema#> \
          CONSTRUCT {{ ?s skos:broadMatch <{type}> . \
          ?s ?pLabel ?label . ?s ?p ?o . \
          }} \
          WHERE {{ \
          VALUES ?pLabel {{ rdfs:label oboInOwl:hasExactSynonym  oboInOwl:hasNarrowSynonym }} \
          VALUES ?p {{ rdfs:subClassOf oboInOwl:IAO_0000115 skos:exactMatch skos:closeMatch }} \
          ?s ?pLabel ?label . \
          ?s ?p ?o . \
          FILTER regex(?label,'{regex}','i') \
          FILTER NOT EXISTS {{ ?s rdfs:subClassOf* <http://purl.obolibrary.org/obo/MONDO_0019065>  }} \
          }} ".format(regex=relDiseases2,type=STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease")

            myparam = { 'query': query2}
            header = {'Content-Type' : 'application/rdf+n3' }
            print("preparing for query2")
            results2=requests.get(MONDOQUERY,params=myparam,headers=header)
            print("query2")
            g.parse(data=results2.content, format="n3")
            print("parse")
            for sub in g.subjects(SKOS.broadMatch,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease")) :
                print("loop for broadMatch")
                h = Graph()
                g += self.getNarrowerFrom(str(sub),STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease",h)
            
            return g


        ###############################################################################
        #                           UNIPROT                                 
        ###############################################################################



        if bioEntity.name == Uniprot().name :


            queryInfo1 = """
            PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 
            CONSTRUCT {{ ?s ?p ?o . ?s skos:prefLabel ?label . ?s skos:broadMatch <{type}> . }}
            WHERE
            {{ ?s rdf:type <http://purl.uniprot.org/core/Concept> . ?s ?p ?o .
            ?s skos:prefLabel ?label .
            FILTER regex(?label,'amyloid$','i')
            }}""".format(type=STR_AMTHES+STR_AMNAME+"_Amyloid")   

            queryInfo2 = """
            PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 
            CONSTRUCT {{ ?s ?p ?o . ?s skos:prefLabel ?label . ?s skos:broadMatch <{type}> . }}
            WHERE
            {{?s rdf:type <http://purl.uniprot.org/core/Concept> . ?s ?p ?o .
            ?s skos:prefLabel ?label .
            FILTER regex(?label,'amyloidosis','i')
            }}""".format(type=STR_AMTHES+STR_AMNAME+"_Amyloidosis")  


            queryInfo3 = """
            PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 
            CONSTRUCT {{ ?s ?p ?o . ?s skos:prefLabel ?label . ?s skos:broadMatch <{type}> . }}
            WHERE
            {{ ?s rdf:type <http://purl.uniprot.org/core/Concept> . ?s ?p ?o .
            ?s skos:prefLabel ?label .
            FILTER regex(?label,'prion','i')
            }}""".format(type=STR_AMTHES+STR_AMNAME+"_Amyloid")  


            queryInfo4 = """
            PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 
            CONSTRUCT {{ ?s ?p ?o . ?s skos:prefLabel ?label . ?s skos:broadMatch <{type}> . }}
            WHERE
            {{?s rdf:type <http://purl.uniprot.org/core/Concept> . ?s ?p ?o .
            ?s skos:prefLabel ?label .
            FILTER regex(?label,'{regex}','i')
            }}""".format(regex = relDiseases1,type=STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease") 

            queryInfo5 = """
            PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 
            CONSTRUCT {{ ?s ?p ?o . ?s skos:prefLabel ?label . ?s skos:broadMatch <{type}> . }}
            WHERE
            {{ ?s rdf:type <http://purl.uniprot.org/core/Concept> . ?s ?p ?o .
            ?s skos:prefLabel ?label .
            FILTER regex(?label,'{regex}','i')
            }}""".format(regex = relDiseases2,type=STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease") 

            endpoint = UNIQUERY
            result1 = requests.get(endpoint, params={'query':queryInfo1},headers={ "Content-type":"application/x-www-form-urlencoded","Accept" : "application/rdf+xml" })
            result2 = requests.get(endpoint, params={'query':queryInfo2},headers={ "Content-type":"application/x-www-form-urlencoded","Accept" : "application/rdf+xml" })
            result3 = requests.get(endpoint, params={'query':queryInfo3},headers={ "Content-type":"application/x-www-form-urlencoded","Accept" : "application/rdf+xml" })
            result4 = requests.get(endpoint, params={'query':queryInfo4},headers={ "Content-type":"application/x-www-form-urlencoded","Accept" : "application/rdf+xml" })
            result5 = requests.get(endpoint, params={'query':queryInfo5},headers={ "Content-type":"application/x-www-form-urlencoded","Accept" : "application/rdf+xml" })
           
            g = Graph()
            g.parse(data=result1.content.decode(), format="xml")
            g.parse(data=result2.content.decode(), format="xml")
            g.parse(data=result3.content.decode(), format="xml")
            g.parse(data=result4.content.decode(), format="xml")
            g.parse(data=result5.content.decode(), format="xml")
        


            return g



        ###############################################################################
        #                           REACTOME                                  
        ###############################################################################



        if bioEntity.name == Reactome().name :

        #On recherche toutes les entits contenant amyloid de type pathway
        #On les annote en tant qu'amyloid_pathway

            queryInfo1 = "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
            prefix owl: <http://www.w3.org/2002/07/owl#> \
            PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
            prefix biopax3: <http://www.biopax.org/release/biopax-level3.owl#> \
            CONSTRUCT {{ ?s skos:broadMatch <{type}> . \
                       ?s ?p ?o .  ?s biopax3:displayName ?label .  ?s rdf:type ?type . }} \
            WHERE {{ ?s ?p ?o . ?s biopax3:displayName ?label .   ?s rdf:type biopax3:Pathway  . \
            FILTER (regex(?label, '{regex}','i')) }} ".format(regex=amyloid,type=STR_AMTHES+STR_AMNAME+"_Amyloid_Pathway")    


        #On recherche toutes les entits contenant amyloid de type complex
        #On les annote en tant qu'amyloid_complex 

            queryInfo2 = "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
            prefix owl: <http://www.w3.org/2002/07/owl#> \
            PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
            prefix biopax3: <http://www.biopax.org/release/biopax-level3.owl#> \
            CONSTRUCT {{ ?s skos:broadMatch <{type}> . \
                       ?s ?p ?o .  ?s biopax3:displayName ?label .  ?s rdf:type ?type . }} \
            WHERE {{ ?s ?p ?o . ?s biopax3:displayName ?label .   ?s rdf:type biopax3:Complex  . \
            FILTER (regex(?label, '{regex}','i')) }} ".format(regex=amyloid,type=STR_AMTHES+STR_AMNAME+"_Amyloid_Complex")    

            results1=bioEntity.getBioSparqlService().execQuery(queryInfo1,N3)
            results2=bioEntity.getBioSparqlService().execQuery(queryInfo2,N3)
            g = Graph()
            g.parse(data=results1, format="n3")
            g.parse(data=results2, format="n3")
            return g


        ###############################################################################
        #                           GO                               
        ###############################################################################

        if bioEntity.name == Go().name :

            #On recherche toutes les entits qui contiennent la regex amyloid|prion...
            #On les annote en tant qu'amyloid_process

            queryInfo1 = "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
      prefix owl: <http://www.w3.org/2002/07/owl#> \
      prefix xsd: <http://www.w3.org/2001/XMLSchema#> \
       PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
      PREFIX oboInOwl:<http://www.geneontology.org/formats/oboInOwl#> \
      CONSTRUCT {{ ?s ?p ?o . ?s rdfs:label ?label. ?s skos:broadMatch <{type}> . }} \
      WHERE {{  \
      ?s ?p ?o . \
      ?s rdfs:label ?label . \
      ?s oboInOwl:hasOBONamespace 'biological_process'^^xsd:string . \
      FILTER regex(?label, '{regex}','i') \
      FILTER NOT EXISTS {{ ?s oboInOwl:id ?id .  FILTER regex(?id,'0040030|0045857|0045858','i')  }}  \
      FILTER NOT EXISTS {{ ?s owl:deprecated 'true'^^xsd:boolean }} \
      }}".format(regex=amyloid,type=STR_AMTHES+STR_AMNAME+"_Amyloid_Process")

            queryInfo2 = "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
      prefix owl: <http://www.w3.org/2002/07/owl#> \
      PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
      prefix xsd: <http://www.w3.org/2001/XMLSchema#> \
      PREFIX oboInOwl:<http://www.geneontology.org/formats/oboInOwl#> \
      CONSTRUCT {{ ?s ?p ?o . ?s rdfs:label ?label. ?s skos:broadMatch <{type}> . }} \
      WHERE {{  \
      ?s ?p ?o . \
      ?s rdfs:label ?label . \
      ?s oboInOwl:hasOBONamespace 'molecular_function'^^xsd:string \
      FILTER regex(?label, '{regex}','i') \
      FILTER NOT EXISTS {{ ?s owl:deprecated 'true'^^xsd:boolean }} \
      }}".format(regex=amyloid,type=STR_AMTHES+STR_AMNAME+"_Amyloid_Function")

            queryInfo3 = "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
      prefix owl: <http://www.w3.org/2002/07/owl#> \
         PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
      prefix xsd: <http://www.w3.org/2001/XMLSchema#> \
      PREFIX oboInOwl:<http://www.geneontology.org/formats/oboInOwl#> \
      CONSTRUCT {{ ?s ?p ?o . ?s rdfs:label ?label. ?s skos:broadMatch <{type}> . }} \
      WHERE {{  \
      ?s ?p ?o . \
      ?s rdfs:label ?label . \
      ?s oboInOwl:hasOBONamespace 'cellular_component'^^xsd:string \
      FILTER regex(?label, '{regex}','i') \
      FILTER NOT EXISTS {{ ?s owl:deprecated 'true'^^xsd:boolean }} \
      }}".format(regex=amyloid,type=STR_AMTHES+STR_AMNAME+"_Amyloid_Complex")
            
            endpoint = GOQUERY
            result1 = requests.get(endpoint, params={'query':queryInfo1},headers={ "Content-type":"application/x-www-form-urlencoded","Accept" : "application/rdf+xml" })
            result2 = requests.get(endpoint, params={'query':queryInfo2},headers={ "Content-type":"application/x-www-form-urlencoded","Accept" : "application/rdf+xml" })
            result3 = requests.get(endpoint, params={'query':queryInfo3},headers={ "Content-type":"application/x-www-form-urlencoded","Accept" : "application/rdf+xml" })
           
            g = Graph()
            g.parse(data=result1.content.decode(), format="xml")
            g.parse(data=result2.content.decode(), format="xml")
            g.parse(data=result3.content.decode(), format="xml")
        
            return g

        
        ###############################################################################
        #                           PDB                      
        ###############################################################################


        if bioEntity.name == PDB().name :
          # On recherche toutes les structures qui ont "amyloid fibril" en keyword ou ("protein fibril" en keyword et "amyloid..." dans le libell mais pas "amyloid-like")
          # On les annote en "amyloid fibril"
            queryInfo1 ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
          PREFIX PDBo: <https://rdf.wwpdb.org/schema/pdbx-v50.owl#> \
          PREFIX dcterms: <http://purl.org/dc/terms/> \
          PREFIX dc: <http://purl.org/dc/elements/1.1/> \
          PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
          CONSTRUCT {{ ?pdb rdfs:label ?pref . ?pdb dcterms:identifier ?id . ?pdb skos:altLabel ?alt . \
          ?pdb skos:altLabel ?alt . ?pdb skos:note ?text . \
          ?pdb skos:broadMatch <{type}> . \
          }} \
          FROM <http://rdf.integbio.jp/dataset/pdbj> \
          WHERE {{  {{  \
          ?pdb dc:title ?pref . \
          ?pdb skos:altLabel ?label . \
          ?struct_keywords_ref PDBo:struct_keywords.text ?text . \
          ?struct_keywords_ref PDBo:of_datablock ?pdb . \
          FILTER regex(?text, '{amfibril}', 'i') \
          }} UNION {{   \
          ?pdb dc:title ?pref . \
          ?pdb skos:altLabel ?label . \
          ?struct_keywords_ref PDBo:struct_keywords.pdbx_keywords ?keyword . \
          ?struct_keywords_ref PDBo:struct_keywords.text ?text . \
          ?struct_keywords_ref PDBo:of_datablock ?pdb . \
          FILTER regex(?pref, '{regex}', 'i') \
          FILTER regex(?keyword, '{profib}', 'i') \
          FILTER NOT EXISTS {{ ?pdb dc:title ?pref .  FILTER regex(?pref, 'amyloid-like', 'i') }}  }} \
          }}".format(amfibril = "amyloid fibril",profib="PROTEIN FIBRIL",regex=amyloid+"|"+prot,type=STR_AMTHES+STR_AMNAME+"_Amyloid_Fibril")

          # On recherche toutes les structures qui ont "amyloid" en keyword ou libell mais pas amyloid-like
          # On les annote en "amyloid"         
            queryInfo2 ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
          PREFIX PDBo: <https://rdf.wwpdb.org/schema/pdbx-v50.owl#> \
          PREFIX dcterms: <http://purl.org/dc/terms/> \
          PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
          PREFIX dc: <http://purl.org/dc/elements/1.1/> \
          CONSTRUCT {{ ?pdb rdfs:label ?pref . ?pdb dcterms:identifier ?id . ?pdb skos:altLabel ?alt . \
          ?pdb skos:altLabel ?alt . ?pdb skos:note ?text . \
          ?pdb skos:broadMatch <{type}>  . \
          }} \
          FROM <http://rdf.integbio.jp/dataset/pdbj> \
          WHERE {{  {{  \
          ?pdb dc:title ?pref . \
          ?pdb skos:altLabel ?label . \
          ?struct_keywords_ref PDBo:struct_keywords.text ?text . \
          ?struct_keywords_ref PDBo:of_datablock ?pdb . \
          FILTER regex(?text, '{regex}', 'i') \
          FILTER NOT EXISTS {{ ?struct_keywords_ref PDBo:struct_keywords.text ?text .  FILTER regex(?text, 'amyloid-like', 'i') }} \
          }} UNION {{ \
          ?pdb dc:title ?pref . \
          ?pdb skos:altLabel ?label . \
          ?struct_keywords_ref PDBo:struct_keywords.text ?text . \
          ?struct_keywords_ref PDBo:of_datablock ?pdb . \
          FILTER regex(?pref, '{regex}', 'i') \
          FILTER NOT EXISTS {{ ?pdb dc:title ?pref .  FILTER regex(?pref,'amyloid-like', 'i') }} \
          }} \
          }}".format(regex=amyloid,type=STR_AMTHES+STR_AMNAME+"_Amyloid_Structure")

          # On recherche toutes les structures qui ont "amyloid-like" en keyword ou libell
          # On les annote en "amyloid-like"         
            queryInfo3 ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
          PREFIX PDBo: <https://rdf.wwpdb.org/schema/pdbx-v50.owl#> \
          PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
          PREFIX dcterms: <http://purl.org/dc/terms/> \
          PREFIX dc: <http://purl.org/dc/elements/1.1/> \
          CONSTRUCT {{ ?pdb rdfs:label ?pref . ?pdb dcterms:identifier ?id . ?pdb skos:altLabel ?alt . \
          ?pdb skos:altLabel ?alt . ?pdb skos:note ?text . \
          ?pdb skos:broadMatch <{type}> . \
          }} \
          FROM <http://rdf.integbio.jp/dataset/pdbj> \
          WHERE {{  {{  \
          ?pdb dc:title ?pref . \
          ?pdb skos:altLabel ?label . \
          ?struct_keywords_ref PDBo:struct_keywords.text ?text . \
          ?struct_keywords_ref PDBo:of_datablock ?pdb . \
          FILTER regex(?text, '{regex}', 'i') \
          }} UNION {{ \
          ?pdb dc:title ?pref . \
          ?pdb skos:altLabel ?label . \
          ?struct_keywords_ref PDBo:struct_keywords.text ?text . \
          ?struct_keywords_ref PDBo:of_datablock ?pdb . \
          FILTER regex(?pref, '{regex}', 'i') \
          }} \
          }}".format(regex="amyloid-like",type=STR_AMTHES+STR_AMNAME+"_Amyloid_Like_Structure")


            queryInfo4 ="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
          PREFIX PDBo: <https://rdf.wwpdb.org/schema/pdbx-v50.owl#> \
          PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
          PREFIX dcterms: <http://purl.org/dc/terms/> \
          PREFIX dc: <http://purl.org/dc/elements/1.1/> \
          CONSTRUCT {{ ?pdb rdfs:label ?pref . ?pdb dcterms:identifier ?id . ?pdb skos:altLabel ?alt . \
          ?pdb skos:altLabel ?alt . ?pdb skos:note ?text . \
          ?pdb skos:broadMatch <{type}> . \
          }} \
          FROM <http://rdf.integbio.jp/dataset/pdbj> \
          WHERE {{   \
          ?pdb dc:title ?pref . \
          ?pdb skos:altLabel ?label . \
          ?struct_keywords_ref PDBo:struct_keywords.text ?text . \
          ?struct_keywords_ref PDBo:of_datablock ?pdb . \
          FILTER regex(str(?pdb), '{regex}', 'i') \
          }}".format(regex="1B0W|1B6D|1BJM|1CD0|1DCL|1EK3|1HEZ|1LGV|1LIL|1QP1|1REI|1WTL|2CD0|2MCG|2OMB|2RHE|3BJL|3MCG|4BJL",type=STR_AMTHES+STR_AMNAME+"_Amyloid_Light_Chain")
           

            results1 = bioEntity.getBioSparqlService().execQuery(queryInfo1,N3)
            results2 = bioEntity.getBioSparqlService().execQuery(queryInfo2,N3)
            results3 = bioEntity.getBioSparqlService().execQuery(queryInfo3,N3)
            results4 = bioEntity.getBioSparqlService().execQuery(queryInfo4,N3)
            g = Graph()
            g.parse(data=results1, format="n3")
            g.parse(data=results2, format="n3")
            g.parse(data=results3, format="n3")
            g.parse(data=results4, format="n3")

            return g


    def createConceptSchemes(self,dbList) : 
        g=Graph()
        g.bind('',URIRef(STR_AMTHES))
        g.bind('skos',URIRef(u'http://www.w3.org/2004/02/skos/core#'))
        g.bind('rdf',URIRef(u'http://www.w3.org/1999/02/22-rdf-syntax-ns#'))
        g.bind('rdfs',URIRef(u'http://www.w3.org/2000/01/rdf-schema#'))
        g.bind('owl',URIRef(u'http://www.w3.org/2002/07/owl#'))

        #create conceptScheme
        for elt in dbList : 
            conceptSchemeUri = URIRef(STR_AMTHES+elt+"/scheme")
            g.add([conceptSchemeUri, RDF.type, SKOS.ConceptScheme])
            g.add([conceptSchemeUri, RDFS.label, Literal("Descripteurs issus de : "+ elt)])
            g.add([conceptSchemeUri, DCTERMS.title, Literal("Descripteurs issus de : "+ elt)])

        return g


    def createInterproConcept(self,g,conceptSchemeUri,topConceptUri,prefLabel,descriptor,description,type) :
        #on ne connait pas l'identifiant interpro, on l'ajoutera plus tard lors de la creation des entrees (SKOS.notation, RDFS,seeAlso)
        g.add([topConceptUri, SKOS.topConceptOf,conceptSchemeUri])
        g.add([topConceptUri, RDF.type, SKOS.Concept])
        g.add([topConceptUri, SKOS.inScheme, conceptSchemeUri])
        g.add([topConceptUri, SKOS.prefLabel, Literal(prefLabel)])
        g.add([topConceptUri, AMMODEL.accession, Literal(str(topConceptUri.split("/")[-1]))])
        if description != "" and description != " " and description != None and description != "''" : 
            g.add([topConceptUri, SKOS.definition, Literal(description)])
        g.add([topConceptUri, SKOS.broadMatch, URIRef(STR_AMTHES+STR_AMNAME+"_"+descriptor)])
        g.add([topConceptUri, SKOS.scopeNote, Literal(type)])
        return g


    def createCath(self) :
        g = Graph()
        file_name = "data/CATH/cath.ttl"
        g.parse(file=open(file_name, "r"),format="n3")
        self.addEntityGraphToStore(g)


    def createInterpro(self) :

        interpro_file = "data/Interpro/Interpro_annotated_ok.csv"

        #On cree les conceptScheme

        dbList = []
        line_count = 0
        with open('data/Interpro/Interpro_annotated_ok.csv',mode='r') as csv_file:
            csv_reader = csv.reader(csv_file,delimiter=";")
            for row in csv_reader:
                if line_count == 0:
                    print(', '.join(row))                    
                    line_count += 1
                else : 
                    dbname = row[4].strip()
                    if "PROSITE" in dbname : dbname = "PROSITE"
                    if "CATH" in dbname : dbname = "CATH"
                    if dbname not in dbList  : 
                        dbList.append(dbname)

        g = self.createConceptSchemes(dbList)

        #On cree les concept : on les ajoute tous en topConcept car pas les infos pour recreer une hierarchie
        line_count = 0
        with open('data/Interpro/Interpro_annotated_ok.csv',mode='r') as csv_file:
            csv_reader = csv.reader(csv_file,delimiter=";")   
            for row in csv_reader:
                if line_count == 0:
                    
                    line_count += 1
                else : 
                    dbId = row[0].strip()
                    dbId = dbId.replace(".","_")
                    dbId = dbId.replace("G3DSA:","")
                    type = row[1].strip()
                    firstIndex = type.find("_")
                    type = type[firstIndex+1:]
                    description = row[2].strip()
                    name = row[3].strip()
                    database = row[4].strip()
                    if "PROSITE" in database : database = "PROSITE"
                    if "CATH" in database : database = "CATH"
                    descriptor = row[5].strip()


                    topConceptUri = URIRef(STR_AMTHES+database+"_"+dbId)
                    conceptSchemeUri = URIRef(STR_AMTHES+database+"/scheme")
                    g = self.createInterproConcept(g,conceptSchemeUri,topConceptUri,name,descriptor,description,type)

        self.addEntityGraphToStore(g)
       


if __name__ == '__main__':

    Thes = Thesaurus()
    #Thes.createAmyloidTerms()
    Thes.createCath()
    #Thes.createFromExtDescriptors(PDB())
    #Thes.createFromExtDescriptors(Go())
    #Thes.createFromExtDescriptors(Reactome())
    #Thes.createFromExtDescriptors(Mondo())
    #Thes.createInterpro() #Il n'existe pas de enpoint / onto permettant d'interroger interpro

    #Thes.createQualifiers()
    #descM.createFromExtDescriptors(UMLS())
    #descM.createFromExtDescriptors(Orphanet()) 
    #descM.createFromExtDescriptors(Mesh())
    
    
    #descM.createFromExtDescriptors(Uniprot())
    
    
