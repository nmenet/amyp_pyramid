>2CD0_A
NFLLTQPHSVSESPGKTVTISCTRSSGSIANNYVHWYQQRPGSSPTTVIFEDDHRPSGVPDRFSGSVDTSSNSASLTISGLKTEDEADYYCQSYDHNNQVFGGGTKLTVLG
>1EK3_A
DIVMTQSPDSLAVSPGERATINCKSSQNLLDSSFDTNTLAWYQQKPGQPPKLLIYWASSRESGVPDRFSGSGSGTDFTLTISSLQAEDVAVYYCQQYYSTPPTFGGGTKVEIKR
>1LGV_A
ETALTQPASVSGSPGQSITVSCTGVSSIVGSYNLVSWYQQHPGKAPKLLTYEVNKRPSGVSDRFSGSKSGNSASLTISGLQAEDEADYYCSSYDGSSTSVVFGGGTKLTVLGQPKAAPSVTLFPPSSEELQANKATLVCLISDFYPGAVTVAWKADSSPVKAGVETTKPSKQSNNKYAASSYLSLTPEQWKSHRSYSCQVTHEGSTVEKTVAPTAC
>P01617
DIVMTQSPLSLPVTPGEPASISCRSSQSLLHSDGFDYLNWYLQKPGQSPZLLIYALSNRASGVPDRFSGSGSGTDFTLKISRVEAEDVGVYYCMZALQAPITFGQGTRLEIKR
>P01612
DVQMTQSPSSLSASVGDRVIITCRASQSSVDYLNWYQQKPGKAPKLLIFDTSNLQSGVPSRFSGGRSGTDFTLTISSLQPDDFATYYCQQSYTNPEVTFGGGTTVDIKR
>A42193
QSVLTQPPSASGTPGQRVTISCSGSSSNIGSNVVTWYQQLPGTAPKLLIYTNNQRPSGVPGRFSGSKSGTSASLAVSGLQSEDEADYYCATWDDSVNGWVFGGGTKLTVLGQPKAAPSVTLFPPSSEELQANKATLVCLISDFYPGAVTVAWKADSSPVKAGVETTTPSKQSNNKYAASSYLSLTPEQWKSHKSYSCQVTHEGSTVEKTVAPTECS
>0610195A
ESVLTQPPSASGVPGQSVIISCSGSSSNIGRNTVNWYQQVPGAAPKLLVYSNNQWPSGVPDRFSGSKSGTSASLAISGLHSEDEADYFCATWDDSLDGPVFGGGTKLTVLGQPKAAPSVTLFPPSSEELQANKATLVCLISDFYPGAVTVAWKADSSPVKAGVETTTPSKZSBBKYAASSYLSLTPEQWKSHRSYSCZVTHZGSTVZKTVAPTECS
>P01721
DFMLTQPHSVSESPGKTVTFSCTGSGGSIADSFVQWYQQRPGSAPTTVIYDDNQRPSGVPDRFSGSIDDSANSASLTISGLKTEDEADYYCQSYNSNHHVVFGGGTKVTVLG
>1B0W_A
DIQMTQSPSSLSASVGDRVTITCQASQDISDYLIWYQQKLGKAPNLLIYDASTLETGVPSRFSGSGSGTEYTFTISSLQPEDIATYYCQQYDDLPYTFGQGTKVEIKR
>1QP1_A
DIQMTQSPSSLSASVGDRVTITCQASQDISDYLIWYQQKLGKAPNLLIYDASTLETGVPSRFSGSGSGTEYTFTISSLQPEDIATYYCQQYDDLPYTFGQGTKVEIK
>P01709
QSALTQPPSASGSLGQSVTISCTGTSSDVGGYNYVSWYQQHAGKAPKVIIYEVNKRPSGVPDRFSGSKSGNTASLTVSGLQAEDEADYYCSSYEGSDNFVFGTGTKVTVLG
>3MCG_1
XSALTQPPSASGSLGQSVTISCTGTSSDVGGYNYVSWYQQHAGKAPKVIIYEVNKRPSGVPDRFSGSKSGNTASLTVSGLQAEDEADYYCSSYEGSDNFVFGTGTKVTVLGQPKANPTVTLFPPSSEELQANKATLVCLISDFYPGAVTVAWKADGSPVKAGVETTKPSKQSNNKYAASSYLSLTPEQWKSHRSYSCQVTHEGSTVEKTVAPTECS
>2MCG_2
XSALTQPPSASGSLGQSVTISCTGTSSDVGGYNYVSWYQQHAGKAPKVIIYEVNKRPSGVPDRFSGSKSGNTASLTVSGLQAEDEADYYCSSYEGSDNFVFGTGTKVTVLGQPKANPTVTLFPPSSEELQANKATLVCLISDFYPGAVTVAWKADGSPVKAGVETTKPSKQSNNKYAASSYLSLTPEQWKSHRSYSCQVTHEGSTVEKTVAPTECS
>2MCG_1
XSALTQPPSASGSLGQSVTISCTGTSSDVGGYNYVSWYQQHAGKAPKVIIYEVNKRPSGVPDRFSGSKSGNTASLTVSGLQAEDEADYYCSSYEGSDNFVFGTGTKVTVLGQPKANPTVTLFPPSSEELQANKATLVCLISDFYPGAVTVAWKADGSPVKAGVETTKPSKQSNNKYAASSYLSLTPEQWKSHRSYSCQVTHEGSTVEKTVAPTECS
>1DCL_B
PSALTQPPSASGSLGQSVTISCTGTSSNVGGYNYVSWYQQHAGKAPKVIIYEVNKRPSGVPDRFSGSKSGNTASLTVSGLQAEDEADYYCSSYEGSDNFVFGTGTKVTVLGQPKANPTVTLFPPSSEELQANKATLVCLISDFYPGAVTVAWKADGSPVKAGVETTKPSKQSNNKYAASSYLSLTPEQWKSHRSYSCQVTHEGSTVEKTVAPTECS
>1DCL_A
PSALTQPPSASGSLGQSVTISCTGTSSNVGGYNYVSWYQQHAGKAPKVIIYEVNKRPSGVPDRFSGSKSGNTASLTVSGLQAEDEADYYCSSYEGSDNFVFGTGTKVTVLGQPKANPTVTLFPPSSEELQANKATLVCLISDFYPGAVTVAWKADGSPVKAGVETTKPSKQSNNKYAASSYLSLTPEQWKSHRSYSCQVTHEGSTVEKTVAPTECS
>1203309A
NFMLTQPHSVSESPGKTVTISCTRSSGSIASYYVQWYQLRPGSAPTTVIYEDNQRPSGVPDRFSGSIDRSSNSASLTVSELKTEDEADYYCQSYDSNNWVFGGGTKLTVLGQPKAAPSVTLFPPSSEELQANKATLVCLISDFYPGAVTVAWKADSSPVKAGVETTTPSKQSNNKYAASSYLSLTPEQWKSHKSYSCQVTHEGSTVEKTVAPTECS
>1002833A
DFMLTQPHSVSESPGKTVIISCTRSDGTIAGYYVQWYQQRPGRAPTTVIFEDTQRPSGVPDRFSGSIDRSSNSASLTISGLQTEDEADYYCQSYDRDHWVFGGGTKLTVLGQPK
>P06888
QSVLTQPPSLSAAPGQRVSISCSGSSSNIGKNYVDWYQQLPGTAPKLLIFNNNKRPSGIPDRFSGSKSGTSATLGITGLQTGDEAIYYCGTWDNRRSVFGGGTNVTVVG
>A29700
SDASVTLGITGLQTGDEADYYCGTWDSSLSAAVFGGGTKLTVLGQPK
>S02083
SGNTASLTITGARAEDEADYYCNSRNSSGNYHVIFGGGTKLTVLSQPKAAPSVTLFPPSSEELQANKATLVCLISDFYPGAV
>S03876
DIVLTQSPLSLPVTPGEPASISCKSSQSLMHSSGDNYLDWYLQKPGQSPQIVIYLGSNRASGVPDTFSGSGSGTDFTLLISSVGAEDVGVYYCMQALQTPWTFGQGTKVGIKRTVAAPSVFIFP
