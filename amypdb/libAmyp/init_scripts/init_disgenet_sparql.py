
from SPARQLWrapper import SPARQLWrapper, JSON
#code à refactoriser....utilisation one-shot pour le moment
sparql = SPARQLWrapper("http://rdf.disgenet.org/sparql/")
sparql.setQuery("""
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        PREFIX void: <http://rdfs.org/ns/void#>
        PREFIX sio: <http://semanticscience.org/resource/>
        PREFIX ncit: <http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#>
        PREFIX up: <http://purl.uniprot.org/core/>
        PREFIX dcat: <http://www.w3.org/ns/dcat#>
        PREFIX dctypes: <http://purl.org/dc/dcmitype/>
        PREFIX wi: <http://http://purl.org/ontology/wi/core#>
        PREFIX eco: <http://http://purl.obolibrary.org/obo/eco.owl#>
        PREFIX prov: <http://http://http://www.w3.org/ns/prov#>
        PREFIX pav: <http://http://http://purl.org/pav/>
        PREFIX obo: <http://purl.obolibrary.org/obo/> 
        SELECT DISTINCT ?protein
        WHERE {
        <http://linkedlifedata.com/resource/umls/id/C0002726> skos:exactMatch ?diseaseMain .
        ?diseaseG rdfs:subClassOf* ?diseaseMain .  
        ?disease skos:exactMatch ?diseaseG .
        ?gda sio:SIO_000628 ?gene,?disease .
        ?gene rdf:type ncit:C16612 ;
        sio:SIO_010078 ?protein .
        ?protein skos:exactMatch ?uniprot .
        FILTER regex(?uniprot, "http://purl.uniprot.org/uniprot/")  
        }
        """)
sparql.setReturnFormat(JSON)
print("Executing first Query")
results = sparql.query().convert()

resultStr=""
for result in results["results"]["bindings"]:
        uri=result["protein"]["value"]
        uri=uri.replace("http://identifiers.org/uniprot/","")
        resultStr+=uri+"\n"

print("Starting query 2")
sparql.setQuery("""
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        PREFIX void: <http://rdfs.org/ns/void#>
        PREFIX sio: <http://semanticscience.org/resource/>
        PREFIX ncit: <http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#>
        PREFIX up: <http://purl.uniprot.org/core/>
        PREFIX dcat: <http://www.w3.org/ns/dcat#>
        PREFIX dctypes: <http://purl.org/dc/dcmitype/>
        PREFIX wi: <http://http://purl.org/ontology/wi/core#>
        PREFIX eco: <http://http://purl.obolibrary.org/obo/eco.owl#>
        PREFIX prov: <http://http://http://www.w3.org/ns/prov#>
        PREFIX pav: <http://http://http://purl.org/pav/>
        PREFIX obo: <http://purl.obolibrary.org/obo/> 
        SELECT DISTINCT ?protein
        WHERE {
        <http://linkedlifedata.com/resource/umls/id/C0002726> skos:exactMatch ?diseaseMain .
        ?diseaseG rdfs:subClassOf* ?diseaseMain .  
        ?disease skos:exactMatch ?diseaseG .
        ?vda sio:SIO_000628 ?variant,?disease .
        ?variant sio:SIO_000216 ?spe,?pleio .
        ?variant so:associated_with ?gene .
        ?gene rdf:type ncit:C16612 ;
        sio:SIO_010078 ?protein .
        ?protein skos:exactMatch ?uniprot .
        FILTER regex(?uniprot, "http://purl.uniprot.org/uniprot/")  
        }
        """)
sparql.setReturnFormat(JSON)
results2 = sparql.query().convert()

for result in results2["results"]["bindings"]:
        uri=result["protein"]["value"]
        uri=uri.replace("http://identifiers.org/uniprot/","")
        resultStr+=uri+"\n"

print("Writing results into file")

with open("results/disgenet_idlist.txt", "w") as f: 
        f.write(resultStr)


