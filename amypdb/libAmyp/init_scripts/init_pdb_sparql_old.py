
from SPARQLWrapper import SPARQLWrapper, JSON
import urllib.request
#import xml.etree.ElementTree as ET

sparql = SPARQLWrapper("https://integbio.jp/rdf/sparql")

sparql.setQuery("""
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX PDBo: <https://rdf.wwpdb.org/schema/pdbx-v50.owl#>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
SELECT ?protein
FROM <http://rdf.integbio.jp/dataset/pdbj>
WHERE { {
?pdb dc:title ?text .
} UNION {  
?struct_keywords_ref PDBo:struct_keywords.text ?text .
?struct_keywords_ref PDBo:of_datablock ?pdb . 
}
FILTER regex(?text, "amylo(id|gen|do|se)|fibril", "i")
?pdb PDBo:has_entityCategory ?entity_category .
?entity_category PDBo:has_entity ?entity .
?entity PDBo:referenced_by_struct_ref ?struct_ref .
?struct_ref PDBo:link_to_uniprot ?protein .
}
 """)

#print(sparql.supportsReturnFormat(JSON))
#sparql.setReturnFormat(JSON)
#sparql.addCustomHttpHeader("Content-Type","application/sparql-results+json")
#POUR MEMO : le endpoint de pdb refuse de retourner du json malgré les infos passés dans la request
# A priori, il faut que le mime type soit passé de manière unique

#sparql.setReturnFormat(XML)

request = sparql._createRequest()
request.add_header('Accept', 'application/sparql-results+json')
response = urllib.request.urlopen(request)
res = SPARQLWrapper.Wrapper.QueryResult((response, sparql.returnFormat))
result = res.convert()

print("Executing Query")
results = sparql.query().convert()
resultStr=""

#tree = ET.parse(results)
#root = tree.getroot()
#for urielt in root.findall("./results/result/binding/uri"):
 #   uri = urielt.text

for result in results["results"]["bindings"]:
    uri=result["protein"]["value"]
    uri=uri.replace("http://purl.uniprot.org/uniprot/","")
    resultStr+=uri+"\n"

with open("results/pdb_idlist.txt", "w") as f:
    f.write(resultStr)

