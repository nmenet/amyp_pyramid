import requests, sys

requestURL = "https://www.uniprot.org/uniprot/"
query = "amyloidogenic+OR+amyloid+OR+amylogenic+OR+amylogenesis+OR+amyloidosis+OR+prion"
myparam = { 'query': query, 'format':'tab', 'sort':'score', 'columns':'id', 'fil':'reviewed:yes'} 
headers = {'User-Agent' : 'Python laurence.r.noel@gmail.com'}
r = requests.get(requestURL,params=myparam, headers=headers)
responseBody = r.text
print(r.status_code)
with open("results/uniprot_idlist.txt", "w") as f: 
    f.write(responseBody)


