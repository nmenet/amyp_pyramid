import requests

endpoint = "http://localhost:80/rdf4j-server/repositories/uniprot/statements"
uniprot = "http://purl.uniprot.org/uniprot/"
prot_file = "results/unique_ids_ok.txt"


queryinit = 'LOAD <https://www.uniprot.org/uniprot/PO2766.rdf> INTO GRAPH <http://purl.uniprot.org/uniprot/>'
headers = {'content-type' : 'application/x-www-form-urlencoded'}
myparam = { 'update': queryinit }
resultsinit=requests.post(endpoint,params=myparam, headers=headers)



with open(prot_file) as f:
    uniprot_list= f.read().splitlines()

with open("results/log.txt","w") as log :
    for id_uniprot in uniprot_list :
        query = 'LOAD <https://www.uniprot.org/uniprot/{idu}.rdf> INTO GRAPH <{uni}>  '.format(uni=uniprot, idu=str(id_uniprot))
        log.write("POSTing SPARQL query with " + str(id_uniprot)+ "\n")
        myparam = { 'update': query }
        results1=requests.post(endpoint,params=myparam, headers=headers)
        print(str(id_uniprot) + " : " + str(results1.status_code))
        if results1.status_code !=200 :
            log.write("ERREUR : "+str(results1.status_code))
            
