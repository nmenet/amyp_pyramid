from SPARQLWrapper import SPARQLWrapper, JSON
#code à refactoriser....utilisation one-shot pour le moment
repository = "http://localhost:8080/rdf4j-server/repositories/amyp"
sparql = SPARQLWrapper(repository)
sparql.setQuery("""
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX model:<http://purl.amypdb.org/model/>
SELECT DISTINCT ?phenoUri ?phenoLabel
WHERE {?maladie model:phenoRef ?phenoUri .
?phenoUri skos:prefLabel ?phenoLabel .
}
""")
sparql.setReturnFormat(JSON)
print(sparql)
print("Executing first Query")
results = sparql.query().convert()
print("result query")
print(results)

resultStr = list()
for result in results["results"]["bindings"]:
        uri = str(result["phenoUri"]["value"])
        print("uri :")
        print(uri)
        symptom = result["phenoLabel"]["value"]
        print("maladie :")
        print(symptom)
        json_symptom = """{{\"label\": \"{symptom}\",\"value\": \"{uri}\"}}""".format(symptom=symptom, uri=uri)
        resultStr.append(json_symptom)
resultStr=str(resultStr).replace("\\"," ")
print("result "+ str(resultStr).replace("'"," "))
print("Writing results into file")

with open("../../static/symptom_list.json", "w") as f:
        f.write(str(resultStr).replace("'"," "))




