from SPARQLWrapper import SPARQLWrapper, JSON
from amypdb.libAmyp.utils import *
import traceback
#code à refactoriser....utilisation one-shot pour le moment
repository = "http://localhost:8080/rdf4j-server/repositories/amyp"
repometamyl="http://localhost:8080/rdf4j-server/repositories/metamyl"
sparql = SPARQLWrapper(repository)
sparql.setReturnFormat(JSON)


sparql.setQuery("""
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX model:<http://purl.amypdb.org/model/>
SELECT DISTINCT ?id ?seq
WHERE {?truc rdf:type model:Protein .
  ?truc model:accession ?id .
  ?truc model:isoform ?isoform .
  ?isoform model:isCanonical true.
  ?isoform model:primStruct ?seqref .
  ?seqref model:sequence ?seq .
}
LIMIT 100
""")
print(sparql)
print("Executing first Query")
results_seq = anti_verbose_json(sparql.query().convert())
print("result query")



sparql.setQuery("""
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX model:<http://purl.amypdb.org/model/>
SELECT DISTINCT ?id ?variant ?new ?pos ?variantId
WHERE {?truc rdf:type model:Protein .
  ?truc model:accession ?id .
  ?truc model:isoform ?isoform .
  ?isoform model:variant ?variant .
  ?variant model:accession ?variantId .
  ?variant model:substitution ?new . 
  ?variant model:positionRef ?posref.
  ?posref model:position ?pos.
}
LIMIT 100
""")
print("Executing 2 Query")
results_var = anti_verbose_json(sparql.query().convert())
print("result query")
print(results_var)


dict_res={}
for item in results_seq:
        try :
                res_metamyl=metamyl_info(item["seq"],repometamyl)
                dict_res[item["id"]]={"seq": item["seq"],"metamyl_prot":{"main_tab":res_metamyl["main_tab"],"tab_HS":res_metamyl["tab_HS"]}}
        except KeyError:
                dict_res[item["id"]] = {"seq": item["seq"], "metamyl_prot": None}
                traceback.print_exc()
print(dict_res)
for item in results_var:
        if item["id"] in dict_res:
                print('var find',item["id"])
                print(item["pos"])
                pos=item["pos"].split("-")
                pos =int(pos[1])
                print(dict_res[item["id"]])
                sequence =dict_res[item["id"]]["seq"]
                seq_var=sequence[:pos]+item["new"]+sequence[pos+1:]
                try:
                        res_metamyl=metamyl_info(seq_var,repometamyl)
                        dict_res[item["id"]][item["variantId"]] = {"main_tab":res_metamyl["main_tab"],"tab_HS":res_metamyl["tab_HS"]}
                        print("add metamyl var")
                except KeyError:
                        dict_res[item["id"]][item["variantId"]]= None
                        traceback.print_exc()


print("Writing results into file")
#print(dict_res)
#with open("../../static/metamyl_info.py", "w") as f:
#        f.write(str(resultStr).replace("'"," "))




