# from rdflib import URIRef, Literal,Graph
# from SPARQLWrapper import SPARQLWrapper,N3,JSON
# import requests
# from rdflib.plugins.stores import sparqlstore
# from rdflib.namespace import SKOS,RDF,RDFS,OWL,DC,XSD
from amypdb.libAmyp.AmyConf import *
from amypdb.libAmyp.Entry import Fragment
import xml.etree.ElementTree as ET


    # ECO:0000000 evidence
    #     ECO:0000006 experimental evidence
    #     ECO:0000041 similarity evidence
    #     ECO:0000088 biological system reconstruction evidence
    #     ECO:0000177 genomic context evidence
    #     ECO:0000204 author statement
    #     ECO:0000212 combinatorial evidence
    #     ECO:0000311 imported information
    #     ECO:0000352 evidence used in manual assertion
    #     ECO:0000361 inferential evidence
    #     ECO:0000501 evidence used in automatic assertion
    #     ECO:0006055 high throughput evidence
    #     ECO:0007672 computational evidence




class Clue(Entry) :

    def __init__(self,identifier):
    	super().__init__(identifier,STR_AMKB,AMQUERY,AMUPDATE)

    def createClueId(entityId,source,ecoId):
    	return entityId+"_"+source+"_"+ecoId

    # def get_source(self):
    #     return self._source

    # source = property(get_source, doc="Source")

    # def get_reference(self):
    #     return self._reference

    # reference = property(get_reference, doc="Reference to the entity concerned by the clue")

    # def get_description():
    #     return self._description

    # description = property(get_description, doc="Description about the clue")

    # def get_ecoId():
    #     return self._ecoId

    # ecoId = property(get_ecoId, doc="Evidence id for the clue")

    def addPubRef(self,publi) :
    	tempG = Graph()
    	pubRef=URIRef(self._stUri+"#PublicationRef")
    	if not self.exists(pubRef) :	
    	  	tempG.add([self._uri,AMMODEL.publicationRef,pubRef])
        	tempG.add([pubRef,RDF.type,URIRef(STR_AMMODEL+"PublicationRef")])
    	tempG.add([pubRef,AMMODEL.publication,publi])	


class AmyloidogenicClue(Clue) :

    def __init__(self,identifier):
    	super().__init__(identifier,STR_AMKB,AMQUERY,AMUPDATE)
    	self._type = AMMODEL.AmyloidogenicClue

	def createClueId(entityId,source,ecoId):
    	return entityId+"_"+source+"_"+ecoId

	def create(ecoId,reference,source,description):
        tempG = Graph()
        tempG.bind('skos',SKOS)
        tempG.bind('rdf',RDF)
        tempG.bind('rdfs',RDFS)
        tempG.bind('amkb',AMKB)
        tempG.bind('ammodel',AMMODEL)
        tempG.bind('amthes',AMTHES)
        tempG.add([self._uri,RDF.type, self._type])
        tempG.add([self._uri,AMMODEL.accession, Literal(self._id)])
        tempG.add([self._uri,AMMODEL.evidence, URIRef(STR_OBO+ecoId)])
        tempG.add([self._uri,AMMODEL.referringTo,reference])
        tempG.add([self._uri,AMMODEL.source,source])
        tempG.add([self._uri,AMMODEL.description,description])

        self.addEntityGraphToStore(tempG)  


if __name__ == '__main__':

    tree = ET.parse('data/Amyload/Amyload_all.xml')  
	root = tree.getroot()

	for fragment in root:  
		frag = Fragment(fragment[tag='id'].text)
        if not frag.exists() :
        	label = fragment[tag='name'].text
        	sequence = fragment[tag='sequence'].text
            frag.create(label,sequence)
                
        reference = frag.uri
        evidence = "ECO_0000006"
        clueId = createClueId(fragment[tag='id'].text,source,evidence)
        clue = AmyloidogenicClue(clueId)

        if not clue.exists() : 
        	source="Amyload"
        	description= ""
        	 #Experimental
        	if fragment[tag='solution'].text != None : 
        		description += " | Solution : " + fragment[tag='solution'].text
        	description += " | Method : "
        	for method in frag.iter(tag='method') :
        		description += method.text + ","
        	description = description[:-1]
        	if fragment[tag='protein'][tag='name'].text != None : 
        		description += " | Protein : " + fragment[tag='protein'][tag='name'].text

        	clue.create(evidence,reference,source,description)
        	for ref in frag.iter(tag='reference') : 
        		clue.addPubRef(ref[tag='url'].text)




