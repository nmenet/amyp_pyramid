from Bio.SeqUtils.ProtParam import ProteinAnalysis

https://biopython.org/wiki/ProtParam

my_seq = "MAEGEITTFTALTEKFNLPPGNYKKPKLLYCSNGGHFLRILPDGTVDGTRDRSDQHIQLQLSAESVGEVYIKSTETGQYLAMDTSGLLYGSQTPSEECLFLERLEENHYNTYTSKKHAEKNWFVGLKKNGSCKRGPRTHYGQKAILFLPLPV"
analysed_seq = ProteinAnalysis(my_seq)
analysed_seq.molecular_weight() #17103.1617
analysed_seq.gravy() #-0.597368421052632
analysed_seq.count_amino_acids() 
"""
{'A': 6, 'C': 3, 'E': 12, 'D': 5, 'G': 14, 'F': 6, 'I': 5, 'H': 5, 'K': 12, 'M':
 2, 'L': 18, 'N': 7, 'Q': 6, 'P': 8, 'S': 10, 'R': 6, 'T': 13, 'W': 1, 'V': 5,
 'Y': 8}
"""

