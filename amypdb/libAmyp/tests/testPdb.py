#import requests
from SPARQLWrapper import SPARQLWrapper,N3,JSON
from amypdb.libAmyp.AmyConf import *
from rdflib import URIRef, Literal,Graph

sparql = SPARQLWrapper(UNIQUERY)
query = "PREFIX unicore:<http://purl.uniprot.org/core/> \
CONSTRUCT {{ ?sub unicore:component ?comp .  ?comp  ?prop ?nameRef . ?nameRef ?propL ?label }} \
WHERE {{ VALUES ?sub {{ <{ent1}> }} ?sub unicore:component ?comp . \
?comp  ?prop ?nameRef .  ?nameRef ?propL ?label }} ".format(ent1="http://purl.uniprot.org/uniprot/P05067")
sparql.setQuery(query)
sparql.setReturnFormat(N3)
results = sparql.query().convert()

queryAnnot = "PREFIX unicore:<http://purl.uniprot.org/core/> \
CONSTRUCT {{ ?sub unicore:annotation ?annot . ?annot ?prop ?r . ?annot a ?type . ?annot <http://www.w3.org/2000/01/rdf-schema#comment> ?com . ?r ?propos ?pos . ?pos ?att ?value }} \
WHERE {{ VALUES ?sub {{ <{ent1}> }} VALUES ?type {{ unicore:Chain_Annotation unicore:Peptide_Annotation unicore:Propeptide_Annotation  }}  ?sub unicore:annotation ?annot . \
?annot ?prop ?r . ?annot a ?type . ?annot <http://www.w3.org/2000/01/rdf-schema#comment> ?com . ?r ?propos ?pos . ?pos ?att ?value }} ".format(ent1="http://purl.uniprot.org/uniprot/P05067")
sparql.setQuery(queryAnnot)
sparql.setReturnFormat(N3)
resultsAnnot = sparql.query().convert()

rg = Graph()
rg.parse(data=results, format="n3")
rg.parse(data=resultsAnnot, format="n3")
print(rg.serialize(format="n3"))


"""
r2 = requests.get("https://www.ebi.ac.uk/pdbe/api/mappings/cath/1b8i")
if r2.status_code == 200 :
	data2 = r2.json()
	items2 = data2['1b8i']
	cath = items2['CATH']
	print(cath.get(1))
	for elt in cath : 
		print(elt)
		print(cath[elt]["topology"])
		for item in cath[elt] :
			if item == "topology" :
				print(cath[elt][item])
		

	

r = requests.get("https://www.ebi.ac.uk/pdbe/api/mappings/all_isoforms/1b8i")
if r.status_code == 200 :
    data = r.json()
    items = data['1b8i']
    isoforms = items['UniProt']
    for elt in isoforms : 
    	print(elt)


r = requests.get("https://www.ebi.ac.uk/pdbe/api/mappings/best_structures/P05067")
if r.status_code == 200 :
    data = r.json()
    items = data['P05067']
    bestPdb = items[0]['pdb_id']
    print(bestPdb)

"""