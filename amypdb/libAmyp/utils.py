import requests
import json

def anti_verbose_json(result_search):
    data=[]
    for row in result_search["results"]["bindings"]:  # permet d'enlever le formatage
        dict = {}
        for elt in result_search["head"]["vars"]:
            if elt in row:
                dict[elt] = row[elt]["value"]
            else:
                dict[elt] = ""
        data.append(dict)
    return data

def metamyl_info(sequence,endpointseq):
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    endpoint = endpointseq

    list_hexa = list()  # list of each hexapeptide in this sequence
    for aa in range(len(sequence)):  # fill in list_hexa
        if aa < len(sequence) - 5:
            list_hexa.append(sequence[aa:aa + 6])

    to_search = ""

    for i in list_hexa:
        if i not in to_search:
            to_search += "\"" + i + "\" "  # sparql format for search
    querystr1 = """PREFIX ammodel: <http://purl.amypdb.org/model/>
                        SELECT DISTINCT ?hexa ?score
                        WHERE {{ ?id ammodel:accession ?hexa .
                        ?id ammodel:score ?score .}}
                        VALUES ?hexa{{ {searched} }}""".format(
        searched=to_search)  # value prend la valeur de la liste d'hexa

    response = requests.post(endpoint, data={"query": querystr1}, headers=headers)

    result_search = response.json()

    data = anti_verbose_json(result_search)

    results = json.loads(json.dumps(data))
    # creation d'un dictionnaire python  de la forme dict{'AAAAAA'}= 0,5... permet de chercher plus facilement
    # a partir de la liste des hexapeptides
    dict_hexa = {}
    for i in results:
        dict_hexa[i['hexa']] = float(i['score'].strip(";\n, "))

    HST = 0.606  # valeur seuil des hotspot

    HS = [0] * len(sequence)  # list des hotspot de la seq
    HSA = [0] * len(sequence)  # avec la moyenne des hostspot
    TA = 0

    list_score = []  # nested list for data display

    for i in range(len(list_hexa)):
        #print(list_hexa[i])
        #print(dict_hexa[list_hexa[i]])
        list_score.append([list_hexa[i], dict_hexa[list_hexa[i]]])  # liste [AA,score]
        TA += dict_hexa[list_hexa[i]]  # somme des scores de tous les hexa de la seq
        if dict_hexa[list_hexa[i]] > HST:  # pour chaque hexa de la liste si le score de
            # l'hexa est > a la valeur seuil
            j = i
            while j < i + 6:  # si les 6 AA du peptide prennent la valeur 1
                HS[j] = 1
                j += 1

    HS_hex = list()

    for i in range(len(HS) - 5):  # on parcours HS
        j = i
        sumHS = 0
        while j < (i + 6):  # on fais la somme des 6 residue suivant
            sumHS += HS[j]
            j += 1
        if sumHS == 6:  # si ont tous la valeur 1
            HS_hex.append(1)  # alors l'hexapeptide prend la valeur 1
        else:
            HS_hex.append(0)  # sinon 0

    TSA = 0  # total surface amyloide
    start_end = list()  # position de debut et fin +max value de chaque hotspot [[start1,end1,max1],[start1,end2,max2]]
    surface = 0  # somme de l'amylogenicité suppérieure au seuil pour l'ensemble des residue du hotspot
    max = 0  # val la plus elevée d'amylogenicité pour chaque hotspot
    cpt = 0  # longueur du hot spot
    for i in range(len(HS_hex)):
        if HS_hex[i] > 0:
            cpt += 1
            if dict_hexa[list_hexa[i]] > HST:
                surface += dict_hexa[list_hexa[i]] - HST
            if dict_hexa[list_hexa[i]] > max:
                max = dict_hexa[list_hexa[i]]
        else:
            if cpt != 0:
                TSA += surface
                j = i - cpt
                while j < i + 5:
                    HSA[j] = surface / cpt
                    j += 1
                start_end.append([i - cpt, i + 5, round(HSA[i], 5), round(max, 5), sequence[i - cpt:i + 5]])
                cpt = 0
                surface = 0
                max = 0
    if cpt > 0:
        TSA += surface
        j = len(HS_hex) - cpt
        print("cpt =", cpt, " j=", j, " stop=", len(HS_hex) + 5)
        while j < len(HS_hex) + 5:
            HSA[j] = surface / cpt
            j += 1
        start_end.append([len(HS_hex) - cpt, len(HS_hex) + 5, round(HSA[len(HS_hex)], 5), round(max, 5),
                          sequence[len(HS_hex) - cpt:len(HS_hex) + 5]])

    # change format of HSA for a good graph and CSV display (start at 1 : [1 , value HSA])
    for index in range(len(HSA)):
        val = HSA[index]
        HSA[index] = [index + 1, val]
    # list for the tab containing : protein length, number of hotspot, total area, total amyloid
    general = [len(sequence), len(start_end), round(TA, 5), round(TSA, 5)]

    return {'HS': HS, 'hexa_scores': list_score, 'hsa': HSA, 'hst': HST, 'tab_HS': start_end, 'main_tab': general}