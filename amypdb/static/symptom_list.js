var customIcon = document.createElement('img');
customIcon.src = "{{request.static_url('amypdb:static/icon.svg')}}";
var customIconMulti = new SelectPure(".autocomplete-select", {
options: [
{
label: "symptome A",
value: "hhtp://fakepath/uriA",
},
{
label: "symptome B",
value: "hhtp://fakepath/uriB",
},
{
label: "symptome C",
value: "hhtp://fakepath/uriC",
},
{
label: "symptome D",
value: "hhtp://fakepath/uriD",
},
],
multiple: true,
autocomplete: true,
inlineIcon: customIcon,
onChange: value => { console.log(value); },
classNames: {
select: "select-pure__select",
dropdownShown: "select-pure__select--opened",
multiselect: "select-pure__select--multiple",
label: "select-pure__label",
placeholder: "select-pure__placeholder",
dropdown: "select-pure__options",
option: "select-pure__option",
autocompleteInput: "select-pure__autocomplete",
selectedLabel: "select-pure__selected-label",
selectedOption: "select-pure__option--selected",
placeholderHidden: "select-pure__placeholder--hidden",
optionHidden: "select-pure__option--hidden",
}
});