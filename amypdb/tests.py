import unittest

from pyramid import testing


class ViewTests(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()
#
 #   def test_my_view(self):
  #      from .views.default import my_view
   #     request = testing.DummyRequest()
    #    info = my_view(request)
     #   self.assertEqual(info['project'], 'amyp') */

    def test_home(self):
        from .views.default import home
        request = testing.DummyRequest()
        response = home(request)
        self.assertEqual('amyp',response['project'])
    

    def test_prot(self):
        from .views.default import prot
        request = testing.DummyRequest()
        response = prot(request)
        # Our view now returns data
        self.assertEqual('P0DJI8', response['idprot'])


class FunctionalTests(unittest.TestCase):
    def setUp(self):
        from amypdb import main
        app = main({})
        from webtest import TestApp
        self.testapp = TestApp(app)

    def test_root(self):
        res = self.testapp.get('/', status=200)
        self.assertTrue(b'Amy' in res.body)
