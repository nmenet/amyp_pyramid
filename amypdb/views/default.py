from pyramid.view import view_config, view_defaults
from pyramid.response import Response
from pyramid.httpexceptions import HTTPFound, HTTPServiceUnavailable
import requests
import json
import re
from amypdb.libAmyp.Entry import *
from amypdb.libAmyp.utils import *
from rdflib import URIRef, Literal, Graph
from rdflib.namespace import SKOS, RDF, RDFS, OWL, DC, XSD
from operator import itemgetter
from pyramid.path import AssetResolver
from collections import Counter
from SPARQLWrapper import SPARQLWrapper, JSON
import csv


@view_config(route_name='home', renderer='../templates/home.jinja2')
def home(request):
    settings = request.registry.settings
    if settings['maintenance'] == True:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)

    endpoint = settings['repo_amyp_query']
    # data = []
    # resolver = AssetResolver()
    # static_path = resolver.resolve('amypdb:static').abspath()
    # json_url = static_path+"/browse_count.json"
    # with open(json_url,'r') as json_file:  
    #     data = json.load(json_file)
    # print(dictCathJson)

    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    resultCount = {}

    queryCountGenes = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
SELECT (count(distinct ?gene) AS ?nbGene)
WHERE { 
?gene rdf:type model:Gene .
?gene model:encodes ?protein .
?protein model:tag ?tag .
?protein model:organism <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606> .
VALUES ?tag { <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Like> 
<http://purl.amypdb.org/thesaurus/Amyp_Involved_In_Amyloidosis> <http://purl.amypdb.org/thesaurus/Amyp_Amylome_Interactor>  }
}"""

    myparam = {'query': queryCountGenes}
    # r=requests.get(endpoint,myparam,headers=headers)
    # results = r.json()
    # for row in results["results"]["bindings"] :
    # resultCount["nbGene"] = row["nbGene"]["value"]
    resultCount["nbGene"] = 542

    queryCountAmylomeProteins = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
SELECT (count(distinct ?protein) AS ?nbProtein) 
WHERE {
?protein rdf:type model:Protein .
?protein model:tag ?tag .
?protein model:organism <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606> .
VALUES ?tag { <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Like> 
<http://purl.amypdb.org/thesaurus/Amyp_Involved_In_Amyloidosis> <http://purl.amypdb.org/thesaurus/Amyp_Amylome_Interactor>  }
} """

    myparam = {'query': queryCountAmylomeProteins}
    # #r=requests.get(endpoint,myparam,headers=headers)    
    # results = r.json()
    # for row in results["results"]["bindings"] :
    #     resultCount["nbAmylomeProtein"] = row["nbProtein"]["value"]
    resultCount["nbAmylomeProtein"] = 598

    queryCountAmyloidProteins = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
SELECT (count(distinct ?protein) AS ?nbProtein) 
WHERE { 
?protein rdf:type model:Protein .
?protein model:tag <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> .
?protein model:organism <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606> . } """

    myparam = {'query': queryCountAmyloidProteins}
    # r=requests.get(endpoint,myparam,headers=headers)    
    # results = r.json()
    # for row in results["results"]["bindings"] :
    #     resultCount["nbAmyloidProtein"] = row["nbProtein"]["value"]
    resultCount["nbAmyloidProtein"] = 265

    queryCountDiseases = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
SELECT (count(distinct ?disease) AS ?nbDisease) 
WHERE {?disease rdf:type model:Disease . ?disease model:tag ?tag .
VALUES ?tag {<http://purl.amypdb.org/thesaurus/Amyp_Amyloidosis> <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Related_Disease>}
}"""

    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = {'query': queryCountDiseases}
    r = requests.get(endpoint, myparam, headers=headers)
    print(endpoint)
    print(myparam)
    print(r.status_code)
    results = r.json()
    for row in results["results"]["bindings"]:
        resultCount["nbDisease"] = row["nbDisease"]["value"]

    return {'project': 'amyp', 'humanCount': resultCount}


@view_config(route_name='contact', renderer='../templates/contact.jinja2')
def contact(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    return {'contact': 'Page contact'}


@view_config(route_name='docu', renderer='../templates/docu.jinja2')
def docu(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    return {'docu': 'page docu'}

@view_config(route_name='docu_start', renderer='../templates/docu_start.jinja2')
def docu_start(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    return {'docu': 'page docu'}
@view_config(route_name='docu_search', renderer='../templates/docu_search.jinja2')
def docu_search(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    return {'docu': 'page docu'}

@view_config(route_name='docu_dtools', renderer='../templates/docu_dtools.jinja2')
def docu_dtools(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    return {'docu': 'page docu'}

@view_config(route_name='docu_where', renderer='../templates/docu_where.jinja2')
def docu_where(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    return {'docu': 'page docu'}

@view_config(route_name='docu_explo', renderer='../templates/docu_explo.jinja2')
def docu_explo(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    return {'docu': 'page docu'}


@view_config(route_name='about_data', renderer='../templates/about_data.jinja2')
def about_data(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    return {'data': 'Page data'}

@view_config(route_name='stats', renderer='../templates/stats.jinja2')
def stats(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)

    endpoint = settings['repo_amyp_query']
    queryAvg = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
        SELECT (AVG(?aromaticity) AS ?avgAro) 
        (AVG(?isoelectricPoint) AS ?avgIso) 
        (AVG(?molWeight) AS ?avgMol) 
        (AVG(?instability) AS ?avgInst) 
        WHERE {
        ?prot rdf:type model:Protein .
        ?prot model:tag <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> .
        ?prot model:isoform ?iso .
        ?prot model:family ?family .
        FILTER regex(str(?family),"Amyp_")
        ?iso model:isCanonical ?boolean .
        ?iso model:aromaticity ?aromaticity .
        ?iso model:isoelectricPoint ?isoelectricPoint .
        OPTIONAL { ?iso model:molWeight ?molWeight . }
        OPTIONAL { ?iso model:instabilityIndex ?instability . }
              } GROUP BY ?family """
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = {'query': queryAvg}
    r = requests.get(endpoint, myparam, headers=headers)
    results = r.json()

    dictAvg = []
    for row in results["results"]["bindings"]:
        dict = {}
        for elt in results["head"]["vars"]:
            if elt in row:
                dict[elt] = row[elt]["value"]
            else:
                dict[elt] = ""
        dictAvg.append(dict)

    dictAvgFinal = []
    counter = 0
    avgAro = 0.0
    avgIso = 0.0
    avgInst = 0.0
    avgMol = 0.0
    for item in dictAvg:
        counter = counter + 1
        avgAro = avgAro + float(item["avgAro"])
        avgIso = avgIso + float(item["avgIso"])
        avgInst = avgInst + float(item["avgInst"])
        avgMol = avgMol + float(item["avgMol"])
    dictAvgFinal = {"avgAro": avgAro / counter, "avgIso": avgIso / counter, "avgInst": avgInst / counter,
                    "avgMol": avgMol / counter}

    queryAmino = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        SELECT ?amino ?family
        WHERE {
        ?prot rdf:type model:Protein .
        ?prot model:tag <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> .
        ?prot model:isoform ?iso .
        ?prot model:family ?family .
        FILTER regex(str(?family),"Amyp_")
        ?iso model:isCanonical ?boolean .
        ?iso model:aminoPercent ?amino .
              }  """
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = {'query': queryAmino}
    r = requests.get(endpoint, myparam, headers=headers)
    results = r.json()
    aminoData = {}

    for item in results["results"]["bindings"]:
        if item["family"]["value"] in aminoData:
            tab = aminoData[item["family"]["value"]]
            tab.append(item["amino"]["value"])
            aminoData[item["family"]["value"]] = tab
        else:
            aminoData[item["family"]["value"]] = [item["amino"]["value"]]

    # first average by family
    aminoAvgFam = []
    for key, value in aminoData.items():
        sums = Counter()
        counters = Counter()
        for itemset in value:
            itemset = json.loads(itemset)
            sums.update(itemset)
            counters.update(itemset.keys())
        ret = {x: float(sums[x]) / counters[x] for x in sums.keys()}
        aminoAvgFam.append(ret)

    # then average

    sums = Counter()
    counters = Counter()
    for itemset in aminoAvgFam:
        sums.update(itemset)
        counters.update(itemset.keys())
    aminoFinal = {x: float(sums[x]) / counters[x] for x in sums.keys()}

    diagram = []
    unidata = {"A": 0.0916, "Q": 0.0377, "L": 0.0990, "S": 0.0665, "R": 0.0575, "E": 0.0618, "K": 0.0494, "T": 0.0554,
               "N": 0.0383, "G": 0.0733, "M": 0.0238, "W": 0.0130, "D": 0.0547, "H": 0.0219, "F": 0.0392, "Y": 0.0291,
               "C": 0.0120, "I": 0.0567, "P": 0.0486, "V": 0.0690}
    for key, value in aminoFinal.items():
        for key2, value2 in unidata.items():
            if key == key2:
                diagram.append([key, value * 100, value2 * 100])

    # print(aminoFinal)

    queryCATH = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        SELECT ?cathTopLabel ?cathLabel (COUNT(distinct ?family2) as ?nb)
        WHERE {
        ?prot rdf:type model:Protein .
        ?prot model:tag <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> .
        ?prot model:isoform ?iso .
        ?prot model:family ?family .
        FILTER regex(str(?family),"CATH","i")
        ?prot model:family ?family2 .
        FILTER regex(str(?family2),"Amyp","i")
        ?family skos:broader ?broadCath .        
        ?broadCath skos:prefLabel ?cathLabel .
        ?broadCath skos:broader ?cathTop . 
        ?cathTop skos:prefLabel ?cathTopLabel .
        }
        GROUP BY ?cathTopLabel ?cathLabel
     """
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = {'query': queryCATH}
    r = requests.get(endpoint, myparam, headers=headers)
    results = r.json()

    dictCath = []
    for row in results["results"]["bindings"]:
        dict = {}
        dict["name"] = row["cathLabel"]["value"]
        dict["value"] = row["nb"]["value"]
        dict["group"] = row["cathTopLabel"]["value"]
        dictCath.append(dict)

    dictCath2 = sorted(dictCath, key=itemgetter('group'))

    dictCathJson = json.loads(json.dumps(dictCath2))
    # print(dictCathJson)

    return {'project': 'amyp', 'dictAvg': dictAvgFinal, 'aminoAvg': diagram, 'dictCath': dictCathJson}


@view_config(route_name='protein_amy', renderer='../templates/protein.jinja2')
def protein_amy(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    mainInfo = Protein(itemId).getMainInfo()
    annotDict = Protein(itemId).getAmyloidAnnotations()
    annotDictSort = sorted(annotDict, key=itemgetter('qualiLabel'))
    resultAnnot = json.loads(json.dumps(annotDictSort))

    return {'itemId': itemId, 'mainInfo': mainInfo, 'annotDict': resultAnnot}


@view_config(route_name='protein_bio', renderer='../templates/protein_bio.jinja2')
def protein_bio(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    print("identifiant : " + itemId)
    mainInfo = Protein(itemId).getMainInfo()
    sankey = Protein(itemId).getBioThread()
    data = json.loads(json.dumps(sankey["data"]))
    colors = json.loads(json.dumps(sankey["node_colors"]))
    legend = json.loads(json.dumps(sankey["legend"]))
    return {'itemId': itemId, 'mainInfo': mainInfo, 'data': data, 'colors': colors, 'legend': legend}


@view_config(route_name='protein_seq', renderer='../templates/protein_seq.jinja2')
def protein_seq(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    print("identifiant : " + itemId)
    mainInfo = Protein(itemId).getMainInfo()
    isoCanoLabel = ""
    bestPdbId = ""

    isoDict = Protein(itemId).getIsoformInfo()
    isoDictSort = sorted(isoDict, key=itemgetter('isoAcc'))

    for elt in isoDictSort:
        if "isoSeq" in elt:
            seqSt = elt["isoSeq"]
            seqArr = re.findall('.{1,10}', seqSt)
            seqSt2 = '  '.join(seqArr)
            elt["isoSeq"] = seqSt2
        if "signInfoList" in elt:
            seqSign = elt["signInfoList"]
            tab = seqSign.split("|")
            seqStr2 = ""
            if len(tab) > 0:
                sign = []
                for item in tab:
                    id_name = item.split("::")
                    if len(id_name) > 1:
                        sign.append(
                            "<li><a target='_blank' href='https://www.ebi.ac.uk/interpro/entry/" + id_name[0] + "'>" +
                            id_name[1] + "</a><li>")
                seqStr2 = "<ul>" + ' '.join(sign) + "</ul>"
            elt['signInfoList'] = seqStr2
        if "isoCano" in elt:
            if elt["isoCano"] == "True" or elt["isoCano"] == "true":
                isoCanoLabel = elt["isoAcc"]
        if "bestPdbId" in elt:
            bestPdbId = elt["bestPdbId"]

    resultIso = json.loads(json.dumps(isoDictSort))

    return {'itemId': itemId, 'mainInfo': mainInfo, 'isoDict': resultIso, 'isoCanoLabel': isoCanoLabel,
            'bestPdbId': bestPdbId}


@view_config(route_name='protein_patho', renderer='../templates/protein_patho.jinja2')
def protein_patho(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    print("identifiant : " + itemId)
    mainInfo = Protein(itemId).getMainInfo()
    if "geneList" in mainInfo:
        seqGenes = mainInfo["geneList"]
        tabGenes = seqGenes.split("|")
        genes = []
        for item in tabGenes:
            elt = item.split("::")
            if len(elt) > 1:
                genes.append({"geneId": elt[0], "geneLabel": elt[1]})
        mainInfo['geneList'] = genes

    print("starting getDiseaseInfo")
    disDict = Protein(itemId).getDiseaseInfo()
    # print("starting sort")
    disDictSort = sorted(disDict, key=itemgetter('diseaseLabel'))
    # print("modifiying values")

    for elt in disDictSort:
        if "pubList" in elt:
            seqSign = elt["pubList"]
            tab = seqSign.split("|")
            sign = []
            counter = 0
            for item in tab:
                if counter < 4:
                    sign.append("<a target='_blank' href='" + item + "'>Pubmed:" + item.split('/')[-1] + "</a>")
                    counter = counter + 1
                elif counter == 4:
                    sign.append("...")
                    counter = counter + 1
                else:
                    break
            seqStr2 = ', '.join(sign)
            elt['pubList'] = seqStr2

    resultDis = json.loads(json.dumps(disDictSort))

    disComment = Protein(itemId).getDisComment()
    print(disComment)

    variantDict = Protein(itemId).getVariantInfo()
    variantDictSort = sorted(variantDict, key=itemgetter('score'))

    return {'itemId': itemId, 'mainInfo': mainInfo, 'disDict': resultDis, 'variantDict': variantDictSort,
            'disComment': disComment}

@view_config(route_name='protein_var', renderer='../templates/protein_var.jinja2')
def protein_var(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    print("identifiant : " + itemId)
    mainInfo = Protein(itemId).getMainInfo()

    print("starting getDiseaseInfo")
   # disDict = Protein(itemId).getDiseaseInfo()
    # print("starting sort")
    #disDictSort = sorted(disDict, key=itemgetter('diseaseLabel'))
    # print("modifying values")

    # for elt in disDictSort:
    #     if "pubList" in elt:
    #         seqSign = elt["pubList"]
    #         tab = seqSign.split("|")
    #         sign = []
    #         counter = 0
    #         for item in tab:
    #             if counter < 4:
    #                 sign.append("<a target='_blank' href='" + item + "'>Pubmed:" + item.split('/')[-1] + "</a>")
    #                 counter = counter + 1
    #             elif counter == 4:
    #                 sign.append("...")
    #                 counter = counter + 1
    #             else:
    #                 break
    #         seqStr2 = ', '.join(sign)
    #         elt['pubList'] = seqStr2

    #resultDis = json.loads(json.dumps(disDictSort))

    #disComment = Protein(itemId).getDisComment()
    #print(disComment)

    variantDict = Protein(itemId).getVariantInfo()
    variantDictSort = sorted(variantDict, key=itemgetter('score'))

    return {'itemId': itemId, 'mainInfo': mainInfo, 'variantDict': variantDictSort}

@view_config(route_name='protein_pub', renderer='../templates/protein_pub.jinja2')
def protein_pub(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']

    pubDict = Protein(itemId).getPublications()
    resultPub = json.loads(json.dumps(pubDict))
    mainInfo = Protein(itemId).getMainInfo()
    return {'itemId': itemId, 'mainInfo': mainInfo, 'pubDict': resultPub}


@view_config(route_name='gene', renderer='../templates/gene.jinja2')
def gene(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    mainInfo = Gene(itemId).getMainInfo()
    protList = Gene(itemId).getProteins()
    sankey = Gene(itemId).getBioThread()
    data = json.loads(json.dumps(sankey["data"]))
    colors = json.loads(json.dumps(sankey["node_colors"]))
    legend = json.loads(json.dumps(sankey["legend"]))
    return {'itemId': itemId, 'mainInfo': mainInfo, 'data': data, 'colors': colors, 'legend': legend,
            'protList': protList}


@view_config(route_name='gene_diseases', renderer='../templates/gene_diseases.jinja2')
def gene_diseases(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    mainInfo = Gene(itemId).getMainInfo()
    disDict = Gene(itemId).getDiseases()
    disDictSort = sorted(disDict, key=itemgetter('score'), reverse=True)

    phenoList = Gene(itemId).getPheno()
    mainPheno = []
    disPheno = []
    for item in phenoList:
        if item not in disPheno:
            disPheno.append(item)
            if phenoList.count(item) > 1:
                mainPheno.append({"phenoLabel": item, "nb": phenoList.count(item)})
    mainPhenoSorted = sorted(mainPheno, key=itemgetter('nb'), reverse=True)
    print(mainPhenoSorted)
    return {'itemId': itemId, 'mainInfo': mainInfo, 'disDict': disDictSort, 'pheno': mainPhenoSorted}


@view_config(route_name='disease', renderer='../templates/disease.jinja2')
def disease(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    mainInfo = Disease(itemId).getMainInfo()

    if "phenoList" in mainInfo:
        seqPheno = mainInfo["phenoList"]
        tabPheno = seqPheno.split("|")
        pheno = []
        for item in tabPheno:
            pheno.append(item)
        mainInfo['phenoList'] = pheno

    return {'itemId': itemId, 'mainInfo': mainInfo}


@view_config(route_name='disease_genes', renderer='../templates/disease_genes.jinja2')
def disease_genes(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']

    mainInfo = Disease(itemId).getMainInfo()
    geneDict = Disease(itemId).getGenes()
    resultGene = json.loads(json.dumps(geneDict))

    genes = ""

    for elt in geneDict:
        if "geneId" in elt:
            if len(geneDict) > 10 and len(geneDict) < 30:
                if "score" in elt:
                    if float(elt["score"]) > 0.3:
                        genes = genes + "<" + STR_AMKB + elt["geneId"] + ">"

            elif len(geneDict) >= 30:
                if "score" in elt:
                    if float(elt["score"]) >= 0.4:
                        genes = genes + "<" + STR_AMKB + elt["geneId"] + ">"
            else:
                genes = genes + "<" + STR_AMKB + elt["geneId"] + ">"

    # disFromGeneList = Disease(itemId).getDiseaseFromGenes(genes)
    # mainDisFG = []
    # disFG = []
    # for item in disFromGeneList : 
    #     if item not in disFG : 
    #         disFG.append(item)
    #         mainDisFG.append({"diseaseLabel":item,"nb":disFromGeneList.count(item)})
    # mainDisFGSorted = sorted(mainDisFG, key=itemgetter('nb'), reverse=True)

    if len(genes) > 0:
        sankey = Disease(itemId).getDiseaseFromGenes(genes)
        data = json.loads(json.dumps(sankey["data"]))
        colors = json.loads(json.dumps(sankey["node_colors"]))
        legend = json.loads(json.dumps(sankey["legend"]))
    else:
        data = []
        colors = []
        legend = []

    phenoList = Disease(itemId).getPhenoFromGenes(genes)
    mainPheno = []
    disPheno = []
    for item in phenoList:
        if item not in disPheno:
            disPheno.append(item)
            if phenoList.count(item) > 1:
                mainPheno.append({"phenoLabel": item, "nb": phenoList.count(item)})
    mainPhenoSorted = sorted(mainPheno, key=itemgetter('nb'), reverse=True)

    return {'itemId': itemId, 'mainInfo': mainInfo, 'resultGene': resultGene, 'data': data, 'colors': colors,
            'legend': legend, 'pheno': mainPhenoSorted}


@view_config(route_name='disease_proteins', renderer='../templates/disease_proteins.jinja2')
def disease_proteins(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']

    mainInfo = Disease(itemId).getMainInfo()
    resultProt = Disease(itemId).getProteins()
    resultProtSorted = sorted(resultProt, key=itemgetter('nb'), reverse=True)
    resultVariant = Disease(itemId).getVariants()

    return {'itemId': itemId, 'mainInfo': mainInfo, 'resultProt': resultProtSorted, 'resultVariant': resultVariant}

@view_config(route_name='disease_related', renderer='../templates/disease_related.jinja2')
def disease_related(request):

    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']

    mainInfo = Disease(itemId).getMainInfo()
    resultParent = Disease(itemId).getParent()
    resultChildren = Disease(itemId).getChildren()
    resultAssoc = Disease(itemId).getAssociated()
    return {'itemId': itemId, 'mainInfo' : mainInfo, 'resultParent' : resultParent, 'resultChildren' : resultChildren, 'resultAssoc' : resultAssoc}



@view_config(route_name='amylome_protein_list', renderer='../templates/amylome_protein_list.jinja2')
def amylome_protein_list(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)

    endpoint = settings['repo_amyp_query']
    querystr = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    SELECT distinct ?entityId ?entityLabel ?qualifier ?description  
    WHERE { 
    ?entity rdf:type model:Protein .
    ?entity model:accession ?entityId .
    ?entity rdfs:label ?entityLabel .
    ?entity model:description ?description .
    ?entity model:tag ?tag .
    ?tag skos:prefLabel ?qualifier .
    ?entity model:organism <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606> .
    VALUES ?tag {
    <http://purl.amypdb.org/thesaurus/Amyp_Amyloid>
    <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Like> 
    <http://purl.amypdb.org/thesaurus/Amyp_Involved_In_Amyloidosis>
    <http://purl.amypdb.org/thesaurus/Amyp_Amylome_Interactor>  }
    }
    ORDER BY ?entityLabel
     """
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = {'query': querystr}
    r = requests.get(endpoint, myparam, headers=headers)
    results = r.json()

    data = []
    for row in results["results"]["bindings"]:
        dict = {}
        for elt in results["head"]["vars"]:
            if elt in row:
                dict[elt] = row[elt]["value"]
            else:
                dict[elt] = ""
        data.append(dict)

    return {'resultList': data,
            'title': 'Proteins belonging to the Amylome : amyloid proteins, proteins involved in amyloidosis and theirs interactors'}


@view_config(route_name='amyloid_protein_list', renderer='../templates/amylome_protein_list.jinja2')
def amyloid_protein_list(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    endpoint = settings['repo_amyp_query']

    querystr = """
    PREFIX search: <http://www.openrdf.org/contrib/lucenesail#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
        SELECT distinct ?entityId ?entityLabel ?qualifier ?description  
        WHERE { 
        ?entity rdf:type model:Protein .
        ?entity model:accession ?entityId .
         ?entity rdfs:label ?entityLabel .
         ?entity model:description ?description .
?entity model:tag ?tag .
?tag skos:prefLabel ?qualifier .
?entity model:organism <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606> .
VALUES ?tag { <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Like> 
} }    """
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = {'query': querystr}
    r = requests.get(endpoint, myparam, headers=headers)
    results = r.json()

    data = []
    for row in results["results"]["bindings"]:
        dict = {}
        for elt in results["head"]["vars"]:
            if elt in row:
                dict[elt] = row[elt]["value"]
            else:
                dict[elt] = ""
        data.append(dict)

    return {'resultList': data, 'title': 'Amyloid proteins'}


@view_config(route_name='disease_list', renderer='../templates/disease_list.jinja2')
def disease_list(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    endpoint = settings['repo_amyp_query']

    querystr = "PREFIX search: <http://www.openrdf.org/contrib/lucenesail#>\
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
        SELECT distinct ?entityId ?entityLabel ?qualifier ?description  \
        WHERE {{ \
        ?entity model:accession ?entityId . \
        ?entity rdfs:label ?entityLabel . \
        ?entity model:description ?description . \
        ?entity rdf:type model:Disease . \
        ?entity model:tag ?tag . \
        ?tag skos:prefLabel ?qualifier . \
        VALUES ?tag {<http://purl.amypdb.org/thesaurus/Amyp_Amyloidosis> <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Related_Disease>  } \
        }} \
         ORDER BY ?entityLabel"
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = {'query': querystr}
    r = requests.get(endpoint, myparam, headers=headers)
    results = r.json()

    data = []
    for row in results["results"]["bindings"]:
        dict = {}
        for elt in results["head"]["vars"]:
            if elt in row:
                dict[elt] = row[elt]["value"]
            else:
                dict[elt] = ""
        data.append(dict)

    return {'resultList': data}


@view_config(route_name='gene_list', renderer='../templates/gene_list.jinja2')
def gene_list(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    endpoint = settings['repo_amyp_query']

    querystr = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
SELECT distinct ?entityId ?entityLabel ?description  (GROUP_CONCAT(DISTINCT ?proteinLabel; SEPARATOR=", ") AS ?proteinLabels) 
WHERE { 
?entity rdf:type model:Gene .
?entity model:accession ?entityId .
?entity rdfs:label ?entityLabel .
?entity model:description ?description . 
?entity model:encodes ?protein .
?protein model:tag ?tag .
?protein rdfs:label ?proteinLabel .
?protein model:organism <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606> .
VALUES ?tag { <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Like> 
<http://purl.amypdb.org/thesaurus/Amyp_Involved_In_Amyloidosis> <http://purl.amypdb.org/thesaurus/Amyp_Amylome_Interactor>  }
} GROUP BY ?entityId ?entityLabel ?description
   ORDER BY ?entityLabel """
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = {'query': querystr}
    r = requests.get(endpoint, myparam, headers=headers)
    results = r.json()

    data = []
    for row in results["results"]["bindings"]:
        dict = {}
        for elt in results["head"]["vars"]:
            if elt in row:
                dict[elt] = row[elt]["value"]
            else:
                dict[elt] = ""
        data.append(dict)

    return {'resultList': data}


@view_config(route_name='results_txt', renderer='../templates/results.jinja2')
def results_txt(request):
    resultGene = []
    resultProtein = []
    resultDisease = []

    settings = request.registry.settings
    endpoint = settings['repo_amyp_query']
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)

    if 'search_text' in request.params:
        search_item = request.params['search_text']
        try :
            search_param = request.params['specie_select']
        except KeyError:
            search_param ="id_search_send_back_here"
        # search_opt = request.params['entity_type']

        # search_type = " model:Gene model:Protein model:Disease "
        # search_type = 'model:Protein'
        # if search_opt == "proteins" :
        #	search_type = " model:Protein "
        # elif search_opt == "diseases" :
        #	search_type = " model:Disease "
        # elif search_opt == "genes" :
        #	search_type = " model:Gene "

        print("search by keyword " + search_item)
        print("espece" + search_param)
        try :
            search_opt = request.params['entity_type']
        except KeyError :
            search_opt="id_search_send_back_here"

        search_type = "model:Gene model:Protein model:Disease"
        search_tag = "?quali"
        if search_opt == "proteins":
            search_type = "model:Protein"
        elif search_opt == "amyloid":
            search_type = "model:Protein"
            search_tag = "<http://purl.amypdb.org/thesaurus/Amyp_Amyloid>"
        elif search_opt == "amylome_interactor":
            search_type = "model:Protein"
            search_tag = "<http://purl.amypdb.org/thesaurus/Amyp_Amylome_Interactor>"
        elif search_opt == "involved_amyloidosis":
            search_type = "model:Protein"
            search_tag = "<http://purl.amypdb.org/thesaurus/Amyp_Involved_In_Amyloidosis>"
        elif search_opt == "pathogenic":
            search_type = "model:Protein"
            search_tag = "<http://purl.amypdb.org/thesaurus/Amyp_Pathogenic>"
        elif search_opt == "diseases":
            search_type = "model:Disease"
        elif search_opt == "amyloidosis":
            search_type = "model:Disease"
            search_tag = "<http://purl.amypdb.org/thesaurus/Amyp_Amyloidosis>"
        elif search_opt == "amyloid_related_disease":
            search_type = "model:Disease"
            search_tag = "<http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Related_Disease>"
        elif search_opt == "genes":
            search_type = "model:Gene"

        print(search_opt)
        print(search_type)
        print(search_tag)

        if search_tag == "?quali":
            search_tag_line = "OPTIONAL{{?entity model:tag ?quali . ?quali skos:prefLabel ?qualifier .}}"
        else:
            search_tag_line = "?entity model:tag " + search_tag + " . " + search_tag + " skos:prefLabel ?qualifier ."

        if search_param == 'human':
            search_tag_line2 = " ?entity model:organism ?orga . FILTER (?orga = <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606>)"
        else:
            search_tag_line2 = " "

        querystr = "PREFIX search: <http://www.openrdf.org/contrib/lucenesail#>\
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
        SELECT distinct ?entityId ?entityLabel ?entityType ?qualifier ?description ?orgLabel \
        WHERE {{ \
        VALUES ?prop {{model:description rdfs:label model:accession }} \
        ?entity search:matches [\
        search:query '{searched}';\
        search:property ?prop;\
        search:snippet ?snippet ] .\
        ?entity model:accession ?entityId .\
        ?entity rdfs:label ?entityLabel .\
        ?entity model:description ?description .\
        ?entity rdf:type ?entityType .\
        OPTIONAL{{?entity model:organism ?org . ?org skos:prefLabel ?orgLabel .}} \
         {searchtagline} \
        {searchtagline2}\
        VALUES ?entityType {{ {searchtype} }}\
        }}  ".format(searched=search_item.strip(), searchtype=search_type, searchtagline=search_tag_line,
                     searchtagline2=search_tag_line2)
        headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
        myparam = {'query': querystr}
        r = requests.get(endpoint, myparam, headers=headers)
        results = r.json()

        data = []
        for row in results["results"]["bindings"]:
            dict = {}
            for elt in results["head"]["vars"]:
                if elt in row:
                    dict[elt] = row[elt]["value"]
                else:
                    dict[elt] = ""
            data.append(dict)

        if len(data) > 0:
            idList = []
            dataProtein = []
            dataGene = []
            dataDisease = []

            for item in data:
                if item["entityId"] not in idList:
                    # impossible to do a group concat on description when combining lucene search with "normal graph search so we must make sure there is no duplicate
                    idList.append(item["entityId"])
                    if "Protein" in item["entityType"]:
                        dataProtein.append(item)
                    elif "Gene" in item["entityType"]:
                        dataGene.append(item)
                    elif "Disease" in item["entityType"]:
                        dataDisease.append(item)

            resultGene = json.loads(json.dumps(dataGene))
            resultProtein = json.loads(json.dumps(dataProtein))
            resultDisease = json.loads(json.dumps(dataDisease))

            return {'search_item': search_item, 'resultGene': resultGene, 'resultProtein': resultProtein,
                    'resultDisease': resultDisease}
        else:
            print("search by kw 2 " + search_item)
            querySuggest = "PREFIX search: <http://www.openrdf.org/contrib/lucenesail#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
        SELECT distinct ?entityId ?entityLabel ?orgLabel \
        WHERE {{ \
        ?entity model:accession ?entityId . \
        ?entity rdf:type model:Protein . \
        ?entity rdfs:label ?entityLabel . \
        ?entity model:organism ?org . ?org skos:prefLabel ?orgLabel .\
         ?entity model:tag <{tag}> . \
         }} LIMIT 10 ".format(tag=STR_AMTHES + STR_AMNAME + "_Amyloid")
            headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
            myparam = {'query': querySuggest}
            r = requests.get(endpoint, myparam, headers=headers)
            search = r.json()
            # print("search results for suggestions")
            # print(search)
            suggestions = []
            for row in search["results"]["bindings"]:
                dict = {}
                for elt in search["head"]["vars"]:
                    if elt in row:
                        dict[elt] = row[elt]["value"]
                    else:
                        dict[elt] = ""
                suggestions.append(dict)
            resultSuggest = json.loads(json.dumps(suggestions))
            print(resultSuggest)

            return {'search_item': search_item, 'resultGene': resultGene, 'resultProtein': resultProtein,
                    'resultDisease': resultDisease, 'suggestions': resultSuggest}
    else:
        url = request.route_url(route_name='home')
        return HTTPFound(location=url)


@view_config(route_name='results_id', renderer='../templates/results.jinja2')
def results_id(request):
    search = None
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    endpoint = settings['repo_amyp_query']
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    if ('search_identifier' in request.params):
        search_item = request.params['search_identifier']

        print("search by id" + search_item)

        if ":" in search_item:
            search_item = search_item.replace(":", "_")
            print("search by id replace " + search_item)

        querystr1 = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
        SELECT distinct ?entity ?entityId  ?type \
        WHERE {{ \
        ?entity model:accession ?entityId . \
        VALUES ?entityId {{ '{searched}' }}  \
        ?entity rdf:type ?type . \
         }}".format(searched=search_item.strip())

        querystr2 = "PREFIX search: <http://www.openrdf.org/contrib/lucenesail#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
        SELECT distinct ?entity ?entityId  ?type  \
        WHERE {{ \
        VALUES ?prop {{ skos:notation }} \
        ?ref search:matches [ \
        search:query '{searched}'; \
        search:property ?prop ] . \
        ?entity model:thesRef ?ref . \
        ?entity model:accession ?entityId . \
        ?entity rdf:type ?type . \
         }}".format(searched=search_item.strip())

        r1 = requests.get(endpoint, params={"query": querystr1}, headers=headers)
        search = r1.json()
        itemType = None
        itemId = None
        print(search)
        if len(search["results"]["bindings"]) > 0:
            print("entityId")
            for item in search["results"]["bindings"]:
                itemId = item["entityId"]["value"]
                itemType = item["type"]["value"]
        else:
            print("second request")
            r2 = requests.get(endpoint, params={"query": querystr2}, headers=headers)
            search = r2.json()
            if len(search["results"]["bindings"]) > 0:
                for item in search["results"]["bindings"]:
                    itemId = item["entityId"]["value"]
                    itemType = item["type"]["value"]

        if itemType != None:
            if "Protein" in itemType:
                url = request.route_url(route_name='protein_amy', itemId=itemId)
                return HTTPFound(location=url)
            elif "Gene" in itemType:
                url = request.route_url(route_name='gene', itemId=itemId)
                return HTTPFound(location=url)
            elif "Concept" in itemType or "Disease" in itemType:
                url = request.route_url(route_name='disease', itemId=itemId)
                return HTTPFound(location=url)
        else:
            print("try search text " + request.params['search_identifier'])
            url = request.route_url(route_name='results_txt') + "?search_text=" + request.params['search_identifier']
            print(url)
            return HTTPFound(location=url)


@view_config(route_name='results_pheno', renderer='../templates/pheno.jinja2')
def results_pheno(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)

    endpoint = settings['repo_amyp_query']
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}

    # get the symptomes ids from the autocompletion menu
    search_items = []
    if "," in request.params['phenolist']: # if more than 1 symptom
        search_items = request.params['phenolist'].split(",")
    else:
        search_items.append(request.params['phenolist'])

    symptom = []
    for item in search_items: # get the names of symptomes with their id
        querystr2 = """PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
                                PREFIX model: <http://purl.amypdb.org/model/> \
                                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
                                SELECT DISTINCT  ?phenoLabel\
                                WHERE {{ <{phenouri}>  skos:prefLabel ?phenoLabel .}}
                                """.format(phenouri=item)
        r2 = requests.get(endpoint, params={"query": querystr2}, headers=headers)

        search2 = r2.json()
        symptom= anti_verbose_json(search2)
    symptom_names = json.loads(json.dumps(symptom))

    if len(search_items) != 0:
        data = []
        while len(search_items)>=1: # if no result found with all the symptoms try again without the last one
            to_search = "" # var to stock the formated symptoms for sparql request

            for i in search_items:
                to_search += " {{ ?maladie model:phenoRef <" + i + "> . }} UNION {{?maladie model:phenoRef ?phenoChildRef .  ?phenoChildRef skos:broader*  <" + i + "> }} "
            #print(to_search)

            querystr1 = """PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
                            PREFIX model: <http://purl.amypdb.org/model/>
                            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                            SELECT DISTINCT ?ID ?maladie ?label ?qualifier ?description  
                            WHERE {{ {searched} 
                            ?maladie model:accession ?ID .
                            ?maladie rdfs:label ?label .
                            ?maladie model:description ?description .
                            OPTIONAL{{ ?maladie model:tag ?quali .
                            ?quali skos:prefLabel ?qualifier .}} 
                            }}""".format(searched=to_search)

            r1 = requests.get(endpoint, params={"query": querystr1}, headers=headers) # do the request sparql

            search = r1.json() # to json format
            data = anti_verbose_json(search) # see in libAmyp/some_function.py

            # here if verification for the while loops (if no result and more than 1 symptom in the symptom list will pop the last
            if len(data) < 1 and len(search_items)>0:
                search_items.pop()
            else: # if no match for doing the while loops exit with break
                break

        global_unique={} # result sparql return different occurence for same disease for each prot linked and each tag this dict is to merge  all info about 1 disease
        # {identifiant_mondo : {id,label,prot ....}, identifiant_mondo2:{}}

        for item in data: # count of link prot-disease and fill of dict global (COULD BE PUT IN A FUNCTION)
            if item["ID"] not in global_unique:
                global_unique[item["ID"]] = item
            else:
                for key in item:
                    if  global_unique[item["ID"]][key] != item[key]:
                        if isinstance(global_unique[item["ID"]][key], list):
                            global_unique[item["ID"]][key].append(item[key])
                        else:
                            global_unique[item["ID"]][key] = [global_unique[item["ID"]][key], item[key]]


        # query to select prot linked to the diseases associated to the requested symptoms
        liste_maladie=""
        for i in global_unique.keys():
            liste_maladie +="\"" + i + "\" "

        querystr2 = """PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        PREFIX model: <http://purl.amypdb.org/model/>
        PREFIX kb: <http://purl.amypdb.org/kb/>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT DISTINCT ?ID ?id_prot ?name_prot ?tag_prot 
         WHERE {{ ?maladie model:accession ?ID .
         ?s  rdf:type model:Proda .
         ?s model:referringTo ?maladie.
         ?s model:referringTo ?id_prot.
         ?id_prot rdf:type <http://purl.amypdb.org/model/Protein>.
         ?id_prot model:accession ?name_prot.
          OPTIONAL{{ ?id_prot model:tag ?quali .
          ?quali skos:prefLabel ?tag_prot .}} 
         }}
         VALUES ?ID {{ {liste_maladie} }}""".format(liste_maladie=liste_maladie)
        #print(querystr2)

        r2 = requests.get(endpoint, params={"query": querystr2}, headers=headers)

        search2 = r2.json()
        data_prot = anti_verbose_json(search2)
        count_p={}

        results_prot = json.loads(json.dumps(data_prot))
        #print(results_prot)
        prot_tag={}
        for i in results_prot:
            if i["id_prot"] not in prot_tag and i["tag_prot"] != "" :
                print("new",i["id_prot"],i["tag_prot"])
                prot_tag[i["id_prot"]] = [i["tag_prot"]]
            elif i["id_prot"] in prot_tag  and i["tag_prot"] not in prot_tag[i["id_prot"]]:
                print("add ",i["id_prot"],i["tag_prot"])
                prot_tag[i["id_prot"]].append(i["tag_prot"])


        print(results_prot)

        for item in data_prot:
            if item["ID"] in count_p:
                count_p[item["ID"]]+=1
            else:
                count_p[item["ID"]]=1
            if item["id_prot"] in count_p:
                count_p[item["id_prot"]]+=1
            else:
                count_p[item["id_prot"]]=1
            if item["ID"] in global_unique: # to add the info relative to protein in the global dictionnary use for display the results
                for key in item:
                    #print('key', key)
                    if key not in global_unique[item["ID"]]: # id_g and name_g not present in query 1 are added for each item
                        global_unique[item["ID"]][key] =  item[key]

                    if  global_unique[item["ID"]][key] != item[key]:
                        if isinstance(global_unique[item["ID"]][key], list):
                            global_unique[item["ID"]][key].append(item[key])
                        else:
                            global_unique[item["ID"]][key] = [global_unique[item["ID"]][key], item[key]]

        #print(global_unique)
        # pour recuperer les tag des protéines crée des doublons au niveaux des nom des protéines cette fonction permet
        # de les éliminer pour l'affichage du tableau
        for item in global_unique:
                if type(global_unique[item]["name_prot"]) is list: # ne le fait pas sur les maladie associées a une seule prot
                    unique_items = list(dict.fromkeys(global_unique[item]["name_prot"]))
                    global_unique[item]["name_prot"]=unique_items

        # query to get the gene info related to diseases of interest  (ID = disease id)
        querystr3 = """PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
                    PREFIX model: <http://purl.amypdb.org/model/>
                    PREFIX kb: <http://purl.amypdb.org/kb/>
                    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                    SELECT DISTINCT ?ID ?id_gene ?name_gene
                    WHERE {{ 
                     ?maladie model:accession ?ID .
                    ?s  rdf:type model:Gda .
                    ?s model:referringTo ?maladie.
                    ?s model:referringTo ?id_gene.
                    ?id_gene rdf:type <http://purl.amypdb.org/model/Gene>.
                    ?id_gene model:accession ?name_gene.
                    }}
                    VALUES ?ID {{ {liste_maladie} }}""".format(liste_maladie=liste_maladie)

        r3 = requests.get(endpoint, params={"query": querystr3}, headers=headers)

        search3 = r3.json()
        data_gene = anti_verbose_json(search3) # see libamyp/some_function.py for function detail
        count = {}

        results_gene = json.loads(json.dumps(data_gene))
        #print(results_gene)

        for item in data_gene:
            if item["ID"] in count:
                count[item["ID"]] += 1
            else:
                count[item["ID"]] = 1
            if item["id_gene"] in count:
                count[item["id_gene"]] += 1
            else:
                count[item["id_gene"]] = 1
            if item["ID"] in global_unique:  # to add the info relative to protein in the global dictionnary use for display the results
                for key in item:
                    if key not in global_unique[
                        item["ID"]]:  # id_g and name_g not present in query 1 are added for each item
                        global_unique[item["ID"]][key] = item[key]

                    if global_unique[item["ID"]][key] != item[key]:
                        if isinstance(global_unique[item["ID"]][key], list):
                            global_unique[item["ID"]][key].append(item[key])
                        else:
                            global_unique[item["ID"]][key] = [global_unique[item["ID"]][key], item[key]]

        # permet de creer 2 var contenant ce qu est necessaire pour creer les noeud et les arcs du graphique diseases-prot

        nodeschart = {'nodes': [], 'edges': []}  # graph diseases-protein
        nodeschart_g = {'nodes': [], 'edges': []}  # graph diseases-gene
        list_tag_used=[] # needed for anychart module because you can't format colors of a non existing group

        #creation of json-like file for anychart. Here nodes a created only for interssting gene/prot/disease (more than
        # 1 links)
        # protein graph data
        for item in results_prot:
            if count_p[item['ID']] > 1 or count_p[item['id_prot']] > 1:
                nodeschart["nodes"].append({'id': global_unique[item['ID']]['maladie'], 'name': global_unique[item['ID']]['label'], 'group': "Disease", 'description':"Disease"})
                nodeschart["edges"].append({'from': global_unique[item['ID']]['maladie'], 'to': item["id_prot"]})

                # some protein are multi-tagged but chart need a unique color by protein so we assign groups to prot depending the most interesting tag.
                # This order is conceived to highlight interessting tag but will need adjustment for every new tag added.
                if  item['id_prot'] in prot_tag and "Variants only" in prot_tag[item['id_prot']] :
                    nodeschart["nodes"].append({'id': item['id_prot'], 'name': item['name_prot'], 'group': "Variants only" , 'description':prot_tag[item['id_prot']] })
                    if "Variants only" not in list_tag_used:
                        list_tag_used.append("Variants only")

                elif  item['id_prot'] in prot_tag and "Amyloid" in prot_tag[item['id_prot']] :
                    nodeschart["nodes"].append({'id': item['id_prot'], 'name': item['name_prot'], 'group': "Amyloid" , 'description':prot_tag[item['id_prot']] })
                    if "Amyloid" not in list_tag_used:
                        list_tag_used.append("Amyloid")

                elif item['id_prot'] in prot_tag and "Involved in amyloidosis" in prot_tag[item['id_prot']] :
                    nodeschart["nodes"].append({'id': item['id_prot'], 'name': item['name_prot'], 'group': "Involved in amyloidosis" ,
                                                'description': prot_tag[item['id_prot']]})
                    if "Involved in amyloidosis" not in list_tag_used:
                        list_tag_used.append("Involved in amyloidosis")

                elif item['id_prot'] in prot_tag and "Amylome Interactor" in prot_tag[item['id_prot']] :
                    nodeschart["nodes"].append({'id': item['id_prot'], 'name': item['name_prot'], 'group': "Amylome Interactor" ,
                                                'description': prot_tag[item['id_prot']]})
                    if "Amylome Interactor" not in list_tag_used:
                        list_tag_used.append("Amylome Interactor")

                elif item['id_prot'] in prot_tag and "Pathogenic" in prot_tag[item['id_prot']] :
                    nodeschart["nodes"].append({'id': item['id_prot'], 'name': item['name_prot'], 'group': "Pathogenic" ,
                                                'description': prot_tag[item['id_prot']]})
                    if "Pathogenic" not in list_tag_used:
                        list_tag_used.append("Pathogenic")

                else:
                    nodeschart["nodes"].append({'id': item['id_prot'], 'name': item['name_prot'], 'group': "Other",
                                                'description':prot_tag[item['id_prot']] })
                    if "Other" not in list_tag_used:
                        list_tag_used.append("Other")
        # gene graph data
        for item in results_gene:
            if count[item['ID']] > 1 or count[item['id_gene']] > 1:
                nodeschart_g["nodes"].append({'id': global_unique[item['ID']]['maladie'], 'name': global_unique[item['ID']]['label'], 'group': "Disease"})
                nodeschart_g["nodes"].append({'id': item['id_gene'], 'name': item['name_gene'], 'group': "Gene"})
                nodeschart_g["edges"].append({'from': global_unique[item['ID']]['maladie'], 'to': item["id_gene"]})

        return {'search_items': search_items, 'symptoms': symptom_names, 'nodeschart': nodeschart,
                'nodeschart_g': nodeschart_g,'global':global_unique, "list_tag_used":list_tag_used}
    else:
        url = request.route_url(route_name='home')
        return HTTPFound(location=url)
@view_config(route_name='result_variant', renderer='../templates/results.jinja2')
def result_variant(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    endpoint = settings['repo_amyp_query']
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}

    search_item = request.params['id_prot_var']
    print("identifiant : " + search_item)

    if ":" in search_item:
        search_item = search_item.replace(":", "_")
        print("search by id replace " + search_item)

    querystr1 = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
    SELECT distinct ?entity ?entityId  ?type \
    WHERE {{ \
    ?entity model:accession ?entityId . \
    VALUES ?entityId {{ '{searched}' }}  \
    ?entity rdf:type ?type . \
     }}".format(searched=search_item.strip())
    print(querystr1)

    r1 = requests.get(endpoint, params={"query": querystr1}, headers=headers)
    search = r1.json()
    itemType = None
    itemId = None
    print('search : ', search)
    if len(search["results"]["bindings"]) > 0:
        print("entityId")
        for item in search["results"]["bindings"]:
            itemId = item["entityId"]["value"]
            itemType = item["type"]["value"]
    print('itemtype ; ',itemType)
    if itemType != None and "Protein" in itemType:
            url = request.route_url(route_name='variant', itemId=itemId)
            return HTTPFound(location=url)
    else:
        print("try search text " + request.params['id_prot_var'])
        url = request.route_url(route_name='results_txt') + "?search_text=" + request.params['id_prot_var']
        print(url)
        return HTTPFound(location=url)
@view_config(route_name='variant', renderer='../templates/variant.jinja2')
def variant(request):
    settings = request.registry.settings
    endpoint = settings['repo_metamyl_query']

    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)


    itemId = request.matchdict['itemId']
    print("identifiant : " + itemId)

    isoDict = Protein(itemId).getIsoformInfo()
    isoDictSort = sorted(isoDict, key=itemgetter('isoAcc'))
    seqSt2 = ""
    for elt in isoDictSort:
        if "isoSeq" in elt:
            if "isoCano" in elt:
                if elt["isoCano"] == "True" or elt["isoCano"] == "true":
                    seqSt2 = elt["isoSeq"]

    if seqSt2 != "":
        dict_result = metamyl_info(seqSt2,endpoint)
        print(dict_result)
        return {'HS': dict_result['HS'], 'hexa_scores': dict_result['hexa_scores'], 'hsa': dict_result['hsa'], 'hst': dict_result['hst'], 'tab_HS': dict_result['tab_HS'], 'main_tab': dict_result['main_tab'],'ItemID':itemId}

@view_config(route_name='variant_search', renderer='../templates/variant_search.jinja2')
def var_search(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']
    print(itemId)
    var_info=Protein(itemId).getVariantInfo()
    if len(var_info)>0:
        print(var_info)
        return {'var_info': var_info,'ItemID':itemId}
    else:
        return {'var_info': None,'ItemID':itemId}

@view_config(route_name='variant_new', renderer='../templates/variant_new.jinja2')
def var_new(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']

    isoDict = Protein(itemId).getIsoformInfo()
    isoDictSort = sorted(isoDict, key=itemgetter('isoAcc'))

    seqSt=""
    for elt in isoDictSort:
        if "isoCano" in elt:
            if elt["isoCano"] == "True" or elt["isoCano"] == "true":
                seqSt = elt["isoSeq"]

    if len(seqSt)>0:
        print(type(seqSt))
    return {'seq': seqSt.lower(),'ItemID':itemId}

@view_config(route_name='variant_metamyl', renderer='../templates/variant_metamyl.jinja2')
def var_new_metamyl(request):
    print( 'new metamyl')
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    endpoint = settings['repo_metamyl_query']
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    itemId = request.matchdict['itemId']

    isoDict = Protein(itemId).getIsoformInfo()
    isoDictSort = sorted(isoDict, key=itemgetter('isoAcc'))
    seqSt2 = ""
    for elt in isoDictSort:
        if "isoSeq" in elt:
            if "isoCano" in elt:
                if elt["isoCano"] == "True" or elt["isoCano"] == "true":
                    seqSt2 = elt["isoSeq"]

    search_item = request.params.dict_of_lists()
    print(search_item)
    variant= seqSt2
    if 'del_pos_end' in search_item:
        print('deletion')
        for pos in range(len(search_item['del_pos_start'])):
            if search_item['del_pos_end'][pos] != "" and search_item['del_pos_start'][pos] != "":
                start = int(search_item['del_pos_start'][pos])-1
                end = int(search_item['del_pos_end'][pos])
                variant_list = list(variant)
                for i in range(end-1, start-1,-1):
                    variant_list.pop(i)
                variant = "".join(variant_list)
            else:
                variant = 'error_del'
                break

    elif 'in_pos_start' in search_item:
        for i in range(len(search_item['in_pos_start'])):
            if search_item['in_pos_start'][i] != "" and search_item['in_pos_end'][i] != "" and search_item['in_seq_in'][
                i] != "":
                start = int(search_item['in_pos_start'][i])
                end = int(search_item['in_pos_end'][i]) - 1
                seq = search_item['in_seq_in'][i].upper()
                variant = variant[:start] + seq + variant[end:]
            else:
                variant = 'error_in'
                break

    elif 'indel_pos_start_i' in search_item:
        for i in range(len(search_item['indel_pos_start_i'])):
            if search_item['indel_pos_start_d'][i] != "" and search_item['indel_pos_end_d'][i] != "" and\
                    search_item['indel_pos_start_i'][i] != "" and search_item['indel_pos_end_i'][i] != "" and \
                    search_item['indel_pos_seq'][i] != "": # check none of input were empty
                # deletion
                start_d = int(search_item['indel_pos_start_d'][i]) - 1
                end_d = int(search_item['indel_pos_end_d'][i])
                variant_list = list(variant)
                for i in range(end_d-1, start_d-1, -1):
                    variant_list.pop(i)
                variant = "".join(variant_list)
                # insertion
                start = int(search_item['indel_pos_start_i'][i])
                end = int(search_item['indel_pos_end_i'][i]) - 1
                seq = search_item['indel_pos_seq'][i].upper()
                variant = variant[:start] + seq + variant[end:]
            else:
                variant = 'error_indel'
                break
    else:
        for i in range(len(search_item['sub_AA_select_B'])):
            if search_item['sub_AA_pos'][i] != "":
                print('in select')
                AA_1 = search_item['sub_AA_select_B'][i]
                AA_2 = search_item['sub_AA_select_A'][i]
                start_1 = int(search_item['sub_AA_pos'][i])-1
                print(variant[start_1], AA_1, AA_2)
                if variant[start_1] == AA_1:
                    variant_list = list(variant)
                    variant_list[start_1] = AA_2
                    variant = "".join(variant_list)
                    print ('after : ',variant, ' AA : ', variant[start_1])
                else:
                    variant = 'error_sub'
                    break
            else:
                variant = 'error_sub'
                break
    print(variant)

    if variant.startswith('error') :
        return {'ItemID': itemId, 'variant': variant}
    else:
        dict_result=metamyl_info(variant,endpoint)
        return {'HS': dict_result['HS'], 'hexa_scores': dict_result['hexa_scores'] , 'hsa':dict_result['hsa'] ,
                'hst': dict_result['hst'] , 'tab_HS': dict_result['tab_HS'], 'ItemID':itemId,'variant':variant.lower(),
                "seq_origin":seqSt2.lower(),'main_tab':dict_result['main_tab'] }

@view_config(route_name='unavailable', renderer='../templates/unavailable.jinja2')
def unavailable(request):
    return {}
