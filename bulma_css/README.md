# Bulma for amypdb

Resources to generate the CSS stylesheets used in [Amypdb](https://gitlab.com/laureno/amyp_pyramid)

## how to

The source files are in the folder src : 
- to change the styles, edit mystyles.scss
- to specify the CSS output file, edit index.js
- to build the CSS file from the SCSS file, run the following command line : 

```text
npm run build
```

The generated CSS file can be found in the dist/css folder. 
The CSS files are then to be put in the static folder of the [Amypdb pyramid application](https://gitlab.com/laureno/amyp_pyramid)