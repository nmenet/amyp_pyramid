from rdflib import URIRef, Literal, Graph
from SPARQLWrapper import SPARQLWrapper, N3, JSON
import requests
from rdflib.plugins.stores import sparqlstore
from rdflib.namespace import SKOS, RDF, RDFS, OWL, DC, XSD
import json
from SPARQLWrapper import SPARQLWrapper, JSON

endpoint = "http://localhost:80/rdf4j-server/repositories/metamyl"

queryinit = 'SELECT ?s WHERE {?s ?p ?o} LIMIT 5 '
headers = {'content-type' : 'application/x-www-form-urlencoded'}
myparam = { 'query': queryinit }
try :
    r=requests.post(endpoint,myparam,headers=headers)
    print(r.status_code)
except :
    print("There was a problem trying to add the graph to the triple store")
